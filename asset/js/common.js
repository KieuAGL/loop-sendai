function breakCheck() {
        return "0px" != $("body").css("min-width") && ("840px" == $("body").css("min-width") ? "first" : "second")
    }! function(e, t) {
        "object" == typeof module && "object" == typeof module.exports ? module.exports = e.document ? t(e, !0) : function(e) {
            if (!e.document) throw new Error("jQuery requires a window with a document");
            return t(e)
        } : t(e)
    }("undefined" != typeof window ? window : this, function(e, t) {
        function n(e) {
            var t = e.length,
                n = oe.type(e);
            return "function" !== n && !oe.isWindow(e) && (!(1 !== e.nodeType || !t) || ("array" === n || 0 === t || "number" == typeof t && t > 0 && t - 1 in e))
        }

        function r(e, t, n) {
            if (oe.isFunction(t)) return oe.grep(e, function(e, r) {
                return !!t.call(e, r, e) !== n
            });
            if (t.nodeType) return oe.grep(e, function(e) {
                return e === t !== n
            });
            if ("string" == typeof t) {
                if (ce.test(t)) return oe.filter(t, e, n);
                t = oe.filter(t, e)
            }
            return oe.grep(e, function(e) {
                return oe.inArray(e, t) >= 0 !== n
            })
        }

        function i(e, t) {
            do {
                e = e[t]
            } while (e && 1 !== e.nodeType);
            return e
        }

        function o(e) {
            var t = ve[e] = {};
            return oe.each(e.match(me) || [], function(e, n) {
                t[n] = !0
            }), t
        }

        function a() {
            fe.addEventListener ? (fe.removeEventListener("DOMContentLoaded", s, !1), e.removeEventListener("load", s, !1)) : (fe.detachEvent("onreadystatechange", s), e.detachEvent("onload", s))
        }

        function s() {
            (fe.addEventListener || "load" === event.type || "complete" === fe.readyState) && (a(), oe.ready())
        }

        function l(e, t, n) {
            if (void 0 === n && 1 === e.nodeType) {
                var r = "data-" + t.replace(Ce, "-$1").toLowerCase();
                if ("string" == typeof(n = e.getAttribute(r))) {
                    try {
                        n = "true" === n || "false" !== n && ("null" === n ? null : +n + "" === n ? +n : we.test(n) ? oe.parseJSON(n) : n)
                    } catch (e) {}
                    oe.data(e, t, n)
                } else n = void 0
            }
            return n
        }

        function u(e) {
            var t;
            for (t in e)
                if (("data" !== t || !oe.isEmptyObject(e[t])) && "toJSON" !== t) return !1;
            return !0
        }

        function c(e, t, n, r) {
            if (oe.acceptData(e)) {
                var i, o, a = oe.expando,
                    s = e.nodeType,
                    l = s ? oe.cache : e,
                    u = s ? e[a] : e[a] && a;
                if (u && l[u] && (r || l[u].data) || void 0 !== n || "string" != typeof t) return u || (u = s ? e[a] = Q.pop() || oe.guid++ : a), l[u] || (l[u] = s ? {} : {
                    toJSON: oe.noop
                }), ("object" == typeof t || "function" == typeof t) && (r ? l[u] = oe.extend(l[u], t) : l[u].data = oe.extend(l[u].data, t)), o = l[u], r || (o.data || (o.data = {}), o = o.data), void 0 !== n && (o[oe.camelCase(t)] = n), "string" == typeof t ? null == (i = o[t]) && (i = o[oe.camelCase(t)]) : i = o, i
            }
        }

        function d(e, t, n) {
            if (oe.acceptData(e)) {
                var r, i, o = e.nodeType,
                    a = o ? oe.cache : e,
                    s = o ? e[oe.expando] : oe.expando;
                if (a[s]) {
                    if (t && (r = n ? a[s] : a[s].data)) {
                        oe.isArray(t) ? t = t.concat(oe.map(t, oe.camelCase)) : t in r ? t = [t] : (t = oe.camelCase(t), t = t in r ? [t] : t.split(" ")), i = t.length;
                        for (; i--;) delete r[t[i]];
                        if (n ? !u(r) : !oe.isEmptyObject(r)) return
                    }(n || (delete a[s].data, u(a[s]))) && (o ? oe.cleanData([e], !0) : re.deleteExpando || a != a.window ? delete a[s] : a[s] = null)
                }
            }
        }

        function f() {
            return !0
        }

        function p() {
            return !1
        }

        function h() {
            try {
                return fe.activeElement
            } catch (e) {}
        }

        function g(e) {
            var t = De.split("|"),
                n = e.createDocumentFragment();
            if (n.createElement)
                for (; t.length;) n.createElement(t.pop());
            return n
        }

        function m(e, t) {
            var n, r, i = 0,
                o = typeof e.getElementsByTagName !== xe ? e.getElementsByTagName(t || "*") : typeof e.querySelectorAll !== xe ? e.querySelectorAll(t || "*") : void 0;
            if (!o)
                for (o = [], n = e.childNodes || e; null != (r = n[i]); i++) !t || oe.nodeName(r, t) ? o.push(r) : oe.merge(o, m(r, t));
            return void 0 === t || t && oe.nodeName(e, t) ? oe.merge([e], o) : o
        }

        function v(e) {
            Ee.test(e.type) && (e.defaultChecked = e.checked)
        }

        function y(e, t) {
            return oe.nodeName(e, "table") && oe.nodeName(11 !== t.nodeType ? t : t.firstChild, "tr") ? e.getElementsByTagName("tbody")[0] || e.appendChild(e.ownerDocument.createElement("tbody")) : e
        }

        function b(e) {
            return e.type = (null !== oe.find.attr(e, "type")) + "/" + e.type, e
        }

        function x(e) {
            var t = ze.exec(e.type);
            return t ? e.type = t[1] : e.removeAttribute("type"), e
        }

        function w(e, t) {
            for (var n, r = 0; null != (n = e[r]); r++) oe._data(n, "globalEval", !t || oe._data(t[r], "globalEval"))
        }

        function C(e, t) {
            if (1 === t.nodeType && oe.hasData(e)) {
                var n, r, i, o = oe._data(e),
                    a = oe._data(t, o),
                    s = o.events;
                if (s) {
                    delete a.handle, a.events = {};
                    for (n in s)
                        for (r = 0, i = s[n].length; i > r; r++) oe.event.add(t, n, s[n][r])
                }
                a.data && (a.data = oe.extend({}, a.data))
            }
        }

        function T(e, t) {
            var n, r, i;
            if (1 === t.nodeType) {
                if (n = t.nodeName.toLowerCase(), !re.noCloneEvent && t[oe.expando]) {
                    i = oe._data(t);
                    for (r in i.events) oe.removeEvent(t, r, i.handle);
                    t.removeAttribute(oe.expando)
                }
                "script" === n && t.text !== e.text ? (b(t).text = e.text, x(t)) : "object" === n ? (t.parentNode && (t.outerHTML = e.outerHTML), re.html5Clone && e.innerHTML && !oe.trim(t.innerHTML) && (t.innerHTML = e.innerHTML)) : "input" === n && Ee.test(e.type) ? (t.defaultChecked = t.checked = e.checked, t.value !== e.value && (t.value = e.value)) : "option" === n ? t.defaultSelected = t.selected = e.defaultSelected : ("input" === n || "textarea" === n) && (t.defaultValue = e.defaultValue)
            }
        }

        function k(t, n) {
            var r = oe(n.createElement(t)).appendTo(n.body),
                i = e.getDefaultComputedStyle ? e.getDefaultComputedStyle(r[0]).display : oe.css(r[0], "display");
            return r.detach(), i
        }

        function S(e) {
            var t = fe,
                n = Qe[e];
            return n || (n = k(e, t), "none" !== n && n || (Ye = (Ye || oe("<iframe frameborder='0' width='0' height='0'/>")).appendTo(t.documentElement), t = (Ye[0].contentWindow || Ye[0].contentDocument).document, t.write(), t.close(), n = k(e, t), Ye.detach()), Qe[e] = n), n
        }

        function j(e, t) {
            return {
                get: function() {
                    var n = e();
                    if (null != n) return n ? void delete this.get : (this.get = t).apply(this, arguments)
                }
            }
        }

        function E(e, t) {
            if (t in e) return t;
            for (var n = t.charAt(0).toUpperCase() + t.slice(1), r = t, i = st.length; i--;)
                if ((t = st[i] + n) in e) return t;
            return r
        }

        function N(e, t) {
            for (var n, r, i, o = [], a = 0, s = e.length; s > a; a++) r = e[a], r.style && (o[a] = oe._data(r, "olddisplay"), n = r.style.display, t ? (o[a] || "none" !== n || (r.style.display = ""), "" === r.style.display && Se(r) && (o[a] = oe._data(r, "olddisplay", S(r.nodeName)))) : o[a] || (i = Se(r), (n && "none" !== n || !i) && oe._data(r, "olddisplay", i ? n : oe.css(r, "display"))));
            for (a = 0; s > a; a++) r = e[a], r.style && (t && "none" !== r.style.display && "" !== r.style.display || (r.style.display = t ? o[a] || "" : "none"));
            return e
        }

        function A(e, t, n) {
            var r = rt.exec(t);
            return r ? Math.max(0, r[1] - (n || 0)) + (r[2] || "px") : t
        }

        function P(e, t, n, r, i) {
            for (var o = n === (r ? "border" : "content") ? 4 : "width" === t ? 1 : 0, a = 0; 4 > o; o += 2) "margin" === n && (a += oe.css(e, n + ke[o], !0, i)), r ? ("content" === n && (a -= oe.css(e, "padding" + ke[o], !0, i)), "margin" !== n && (a -= oe.css(e, "border" + ke[o] + "Width", !0, i))) : (a += oe.css(e, "padding" + ke[o], !0, i), "padding" !== n && (a += oe.css(e, "border" + ke[o] + "Width", !0, i)));
            return a
        }

        function H(e, t, n) {
            var r = !0,
                i = "width" === t ? e.offsetWidth : e.offsetHeight,
                o = Ge(e),
                a = re.boxSizing() && "border-box" === oe.css(e, "boxSizing", !1, o);
            if (0 >= i || null == i) {
                if (i = Ue(e, t, o), (0 > i || null == i) && (i = e.style[t]), Ze.test(i)) return i;
                r = a && (re.boxSizingReliable() || i === e.style[t]), i = parseFloat(i) || 0
            }
            return i + P(e, t, n || (a ? "border" : "content"), r, o) + "px"
        }

        function L(e, t, n, r, i) {
            return new L.prototype.init(e, t, n, r, i)
        }

        function D() {
            return setTimeout(function() {
                lt = void 0
            }), lt = oe.now()
        }

        function M(e, t) {
            var n, r = {
                    height: e
                },
                i = 0;
            for (t = t ? 1 : 0; 4 > i; i += 2 - t) n = ke[i], r["margin" + n] = r["padding" + n] = e;
            return t && (r.opacity = r.width = e), r
        }

        function O(e, t, n) {
            for (var r, i = (ht[t] || []).concat(ht["*"]), o = 0, a = i.length; a > o; o++)
                if (r = i[o].call(n, t, e)) return r
        }

        function F(e, t, n) {
            var r, i, o, a, s, l, u, c, d = this,
                f = {},
                p = e.style,
                h = e.nodeType && Se(e),
                g = oe._data(e, "fxshow");
            n.queue || (s = oe._queueHooks(e, "fx"), null == s.unqueued && (s.unqueued = 0, l = s.empty.fire, s.empty.fire = function() {
                s.unqueued || l()
            }), s.unqueued++, d.always(function() {
                d.always(function() {
                    s.unqueued--, oe.queue(e, "fx").length || s.empty.fire()
                })
            })), 1 === e.nodeType && ("height" in t || "width" in t) && (n.overflow = [p.overflow, p.overflowX, p.overflowY], u = oe.css(e, "display"), c = S(e.nodeName), "none" === u && (u = c), "inline" === u && "none" === oe.css(e, "float") && (re.inlineBlockNeedsLayout && "inline" !== c ? p.zoom = 1 : p.display = "inline-block")), n.overflow && (p.overflow = "hidden", re.shrinkWrapBlocks() || d.always(function() {
                p.overflow = n.overflow[0], p.overflowX = n.overflow[1], p.overflowY = n.overflow[2]
            }));
            for (r in t)
                if (i = t[r], ct.exec(i)) {
                    if (delete t[r], o = o || "toggle" === i, i === (h ? "hide" : "show")) {
                        if ("show" !== i || !g || void 0 === g[r]) continue;
                        h = !0
                    }
                    f[r] = g && g[r] || oe.style(e, r)
                }
            if (!oe.isEmptyObject(f)) {
                g ? "hidden" in g && (h = g.hidden) : g = oe._data(e, "fxshow", {}), o && (g.hidden = !h), h ? oe(e).show() : d.done(function() {
                    oe(e).hide()
                }), d.done(function() {
                    var t;
                    oe._removeData(e, "fxshow");
                    for (t in f) oe.style(e, t, f[t])
                });
                for (r in f) a = O(h ? g[r] : 0, r, d), r in g || (g[r] = a.start, h && (a.end = a.start, a.start = "width" === r || "height" === r ? 1 : 0))
            }
        }

        function B(e, t) {
            var n, r, i, o, a;
            for (n in e)
                if (r = oe.camelCase(n), i = t[r], o = e[n], oe.isArray(o) && (i = o[1], o = e[n] = o[0]), n !== r && (e[r] = o, delete e[n]), (a = oe.cssHooks[r]) && "expand" in a) {
                    o = a.expand(o), delete e[r];
                    for (n in o) n in e || (e[n] = o[n], t[n] = i)
                } else t[r] = i
        }

        function $(e, t, n) {
            var r, i, o = 0,
                a = pt.length,
                s = oe.Deferred().always(function() {
                    delete l.elem
                }),
                l = function() {
                    if (i) return !1;
                    for (var t = lt || D(), n = Math.max(0, u.startTime + u.duration - t), r = n / u.duration || 0, o = 1 - r, a = 0, l = u.tweens.length; l > a; a++) u.tweens[a].run(o);
                    return s.notifyWith(e, [u, o, n]), 1 > o && l ? n : (s.resolveWith(e, [u]), !1)
                },
                u = s.promise({
                    elem: e,
                    props: oe.extend({}, t),
                    opts: oe.extend(!0, {
                        specialEasing: {}
                    }, n),
                    originalProperties: t,
                    originalOptions: n,
                    startTime: lt || D(),
                    duration: n.duration,
                    tweens: [],
                    createTween: function(t, n) {
                        var r = oe.Tween(e, u.opts, t, n, u.opts.specialEasing[t] || u.opts.easing);
                        return u.tweens.push(r), r
                    },
                    stop: function(t) {
                        var n = 0,
                            r = t ? u.tweens.length : 0;
                        if (i) return this;
                        for (i = !0; r > n; n++) u.tweens[n].run(1);
                        return t ? s.resolveWith(e, [u, t]) : s.rejectWith(e, [u, t]), this
                    }
                }),
                c = u.props;
            for (B(c, u.opts.specialEasing); a > o; o++)
                if (r = pt[o].call(u, e, c, u.opts)) return r;
            return oe.map(c, O, u), oe.isFunction(u.opts.start) && u.opts.start.call(e, u), oe.fx.timer(oe.extend(l, {
                elem: e,
                anim: u,
                queue: u.opts.queue
            })), u.progress(u.opts.progress).done(u.opts.done, u.opts.complete).fail(u.opts.fail).always(u.opts.always)
        }

        function V(e) {
            return function(t, n) {
                "string" != typeof t && (n = t, t = "*");
                var r, i = 0,
                    o = t.toLowerCase().match(me) || [];
                if (oe.isFunction(n))
                    for (; r = o[i++];) "+" === r.charAt(0) ? (r = r.slice(1) || "*", (e[r] = e[r] || []).unshift(n)) : (e[r] = e[r] || []).push(n)
            }
        }

        function q(e, t, n, r) {
            function i(s) {
                var l;
                return o[s] = !0, oe.each(e[s] || [], function(e, s) {
                    var u = s(t, n, r);
                    return "string" != typeof u || a || o[u] ? a ? !(l = u) : void 0 : (t.dataTypes.unshift(u), i(u), !1)
                }), l
            }
            var o = {},
                a = e === Mt;
            return i(t.dataTypes[0]) || !o["*"] && i("*")
        }

        function _(e, t) {
            var n, r, i = oe.ajaxSettings.flatOptions || {};
            for (r in t) void 0 !== t[r] && ((i[r] ? e : n || (n = {}))[r] = t[r]);
            return n && oe.extend(!0, e, n), e
        }

        function R(e, t, n) {
            for (var r, i, o, a, s = e.contents, l = e.dataTypes;
                "*" === l[0];) l.shift(), void 0 === i && (i = e.mimeType || t.getResponseHeader("Content-Type"));
            if (i)
                for (a in s)
                    if (s[a] && s[a].test(i)) {
                        l.unshift(a);
                        break
                    }
            if (l[0] in n) o = l[0];
            else {
                for (a in n) {
                    if (!l[0] || e.converters[a + " " + l[0]]) {
                        o = a;
                        break
                    }
                    r || (r = a)
                }
                o = o || r
            }
            return o ? (o !== l[0] && l.unshift(o), n[o]) : void 0
        }

        function z(e, t, n, r) {
            var i, o, a, s, l, u = {},
                c = e.dataTypes.slice();
            if (c[1])
                for (a in e.converters) u[a.toLowerCase()] = e.converters[a];
            for (o = c.shift(); o;)
                if (e.responseFields[o] && (n[e.responseFields[o]] = t), !l && r && e.dataFilter && (t = e.dataFilter(t, e.dataType)), l = o, o = c.shift())
                    if ("*" === o) o = l;
                    else if ("*" !== l && l !== o) {
                if (!(a = u[l + " " + o] || u["* " + o]))
                    for (i in u)
                        if (s = i.split(" "), s[1] === o && (a = u[l + " " + s[0]] || u["* " + s[0]])) {
                            a === !0 ? a = u[i] : u[i] !== !0 && (o = s[0], c.unshift(s[1]));
                            break
                        }
                if (a !== !0)
                    if (a && e.throws) t = a(t);
                    else try {
                        t = a(t)
                    } catch (e) {
                        return {
                            state: "parsererror",
                            error: a ? e : "No conversion from " + l + " to " + o
                        }
                    }
            }
            return {
                state: "success",
                data: t
            }
        }

        function I(e, t, n, r) {
            var i;
            if (oe.isArray(t)) oe.each(t, function(t, i) {
                n || Ft.test(e) ? r(e, i) : I(e + "[" + ("object" == typeof i ? t : "") + "]", i, n, r)
            });
            else if (n || "object" !== oe.type(t)) r(e, t);
            else
                for (i in t) I(e + "[" + i + "]", t[i], n, r)
        }

        function W() {
            try {
                return new e.XMLHttpRequest
            } catch (e) {}
        }

        function X() {
            try {
                return new e.ActiveXObject("Microsoft.XMLHTTP")
            } catch (e) {}
        }

        function Y(e) {
            return oe.isWindow(e) ? e : 9 === e.nodeType && (e.defaultView || e.parentWindow)
        }
        var Q = [],
            G = Q.slice,
            U = Q.concat,
            J = Q.push,
            Z = Q.indexOf,
            K = {},
            ee = K.toString,
            te = K.hasOwnProperty,
            ne = "".trim,
            re = {},
            ie = "1.11.0",
            oe = function(e, t) {
                return new oe.fn.init(e, t)
            },
            ae = function(e, t) {
                return t.toUpperCase()
            };
        oe.fn = oe.prototype = {
            jquery: ie,
            constructor: oe,
            selector: "",
            length: 0,
            toArray: function() {
                return G.call(this)
            },
            get: function(e) {
                return null != e ? 0 > e ? this[e + this.length] : this[e] : G.call(this)
            },
            pushStack: function(e) {
                var t = oe.merge(this.constructor(), e);
                return t.prevObject = this, t.context = this.context, t
            },
            each: function(e, t) {
                return oe.each(this, e, t)
            },
            map: function(e) {
                return this.pushStack(oe.map(this, function(t, n) {
                    return e.call(t, n, t)
                }))
            },
            slice: function() {
                return this.pushStack(G.apply(this, arguments))
            },
            first: function() {
                return this.eq(0)
            },
            last: function() {
                return this.eq(-1)
            },
            eq: function(e) {
                var t = this.length,
                    n = +e + (0 > e ? t : 0);
                return this.pushStack(n >= 0 && t > n ? [this[n]] : [])
            },
            end: function() {
                return this.prevObject || this.constructor(null)
            },
            push: J,
            sort: Q.sort,
            splice: Q.splice
        }, oe.extend = oe.fn.extend = function() {
            var e, t, n, r, i, o, a = arguments[0] || {},
                s = 1,
                l = arguments.length,
                u = !1;
            for ("boolean" == typeof a && (u = a, a = arguments[s] || {}, s++), "object" == typeof a || oe.isFunction(a) || (a = {}), s === l && (a = this, s--); l > s; s++)
                if (null != (i = arguments[s]))
                    for (r in i) e = a[r], n = i[r], a !== n && (u && n && (oe.isPlainObject(n) || (t = oe.isArray(n))) ? (t ? (t = !1, o = e && oe.isArray(e) ? e : []) : o = e && oe.isPlainObject(e) ? e : {}, a[r] = oe.extend(u, o, n)) : void 0 !== n && (a[r] = n));
            return a
        }, oe.extend({
            expando: "jQuery" + (ie + Math.random()).replace(/\D/g, ""),
            isReady: !0,
            error: function(e) {
                throw new Error(e)
            },
            noop: function() {},
            isFunction: function(e) {
                return "function" === oe.type(e)
            },
            isArray: Array.isArray || function(e) {
                return "array" === oe.type(e)
            },
            isWindow: function(e) {
                return null != e && e == e.window
            },
            isNumeric: function(e) {
                return e - parseFloat(e) >= 0
            },
            isEmptyObject: function(e) {
                var t;
                for (t in e) return !1;
                return !0
            },
            isPlainObject: function(e) {
                var t;
                if (!e || "object" !== oe.type(e) || e.nodeType || oe.isWindow(e)) return !1;
                try {
                    if (e.constructor && !te.call(e, "constructor") && !te.call(e.constructor.prototype, "isPrototypeOf")) return !1
                } catch (e) {
                    return !1
                }
                if (re.ownLast)
                    for (t in e) return te.call(e, t);
                for (t in e);
                return void 0 === t || te.call(e, t)
            },
            type: function(e) {
                return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? K[ee.call(e)] || "object" : typeof e
            },
            globalEval: function(t) {
                t && oe.trim(t) && (e.execScript || function(t) {
                    e.eval.call(e, t)
                })(t)
            },
            camelCase: function(e) {
                return e.replace(/^-ms-/, "ms-").replace(/-([\da-z])/gi, ae)
            },
            nodeName: function(e, t) {
                return e.nodeName && e.nodeName.toLowerCase() === t.toLowerCase()
            },
            each: function(e, t, r) {
                var i = 0,
                    o = e.length,
                    a = n(e);
                if (r) {
                    if (a)
                        for (; o > i && t.apply(e[i], r) !== !1; i++);
                    else
                        for (i in e)
                            if (t.apply(e[i], r) === !1) break
                } else if (a)
                    for (; o > i && t.call(e[i], i, e[i]) !== !1; i++);
                else
                    for (i in e)
                        if (t.call(e[i], i, e[i]) === !1) break; return e
            },
            trim: ne && !ne.call("\ufeff ") ? function(e) {
                return null == e ? "" : ne.call(e)
            } : function(e) {
                return null == e ? "" : (e + "").replace(/^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g, "")
            },
            makeArray: function(e, t) {
                var r = t || [];
                return null != e && (n(Object(e)) ? oe.merge(r, "string" == typeof e ? [e] : e) : J.call(r, e)), r
            },
            inArray: function(e, t, n) {
                var r;
                if (t) {
                    if (Z) return Z.call(t, e, n);
                    for (r = t.length, n = n ? 0 > n ? Math.max(0, r + n) : n : 0; r > n; n++)
                        if (n in t && t[n] === e) return n
                }
                return -1
            },
            merge: function(e, t) {
                for (var n = +t.length, r = 0, i = e.length; n > r;) e[i++] = t[r++];
                if (n !== n)
                    for (; void 0 !== t[r];) e[i++] = t[r++];
                return e.length = i, e
            },
            grep: function(e, t, n) {
                for (var r = [], i = 0, o = e.length, a = !n; o > i; i++) !t(e[i], i) !== a && r.push(e[i]);
                return r
            },
            map: function(e, t, r) {
                var i, o = 0,
                    a = e.length,
                    s = n(e),
                    l = [];
                if (s)
                    for (; a > o; o++) null != (i = t(e[o], o, r)) && l.push(i);
                else
                    for (o in e) null != (i = t(e[o], o, r)) && l.push(i);
                return U.apply([], l)
            },
            guid: 1,
            proxy: function(e, t) {
                var n, r, i;
                return "string" == typeof t && (i = e[t], t = e, e = i), oe.isFunction(e) ? (n = G.call(arguments, 2), r = function() {
                    return e.apply(t || this, n.concat(G.call(arguments)))
                }, r.guid = e.guid = e.guid || oe.guid++, r) : void 0
            },
            now: function() {
                return +new Date
            },
            support: re
        }), oe.each("Boolean Number String Function Array Date RegExp Object Error".split(" "), function(e, t) {
            K["[object " + t + "]"] = t.toLowerCase()
        });
        var se = function(e) {
            function t(e, t, n, r) {
                var i, o, a, s, u, f, p, h, g, m;
                if ((t ? t.ownerDocument || t : $) !== P && A(t), t = t || P, n = n || [], !e || "string" != typeof e) return n;
                if (1 !== (s = t.nodeType) && 9 !== s) return [];
                if (L && !r) {
                    if (i = me.exec(e))
                        if (a = i[1]) {
                            if (9 === s) {
                                if (!(o = t.getElementById(a)) || !o.parentNode) return n;
                                if (o.id === a) return n.push(o), n
                            } else if (t.ownerDocument && (o = t.ownerDocument.getElementById(a)) && F(t, o) && o.id === a) return n.push(o), n
                        } else {
                            if (i[2]) return J.apply(n, t.getElementsByTagName(e)), n;
                            if ((a = i[3]) && w.getElementsByClassName && t.getElementsByClassName) return J.apply(n, t.getElementsByClassName(a)), n
                        }
                    if (w.qsa && (!D || !D.test(e))) {
                        if (h = p = B, g = t, m = 9 === s && e, 1 === s && "object" !== t.nodeName.toLowerCase()) {
                            for (f = c(e), (p = t.getAttribute("id")) ? h = p.replace(ye, "\\$&") : t.setAttribute("id", h), h = "[id='" + h + "'] ", u = f.length; u--;) f[u] = h + d(f[u]);
                            g = ve.test(e) && l(t.parentNode) || t, m = f.join(",")
                        }
                        if (m) try {
                            return J.apply(n, g.querySelectorAll(m)), n
                        } catch (e) {} finally {
                            p || t.removeAttribute("id")
                        }
                    }
                }
                return b(e.replace(ae, "$1"), t, n, r)
            }

            function n() {
                function e(n, r) {
                    return t.push(n + " ") > C.cacheLength && delete e[t.shift()], e[n + " "] = r
                }
                var t = [];
                return e
            }

            function r(e) {
                return e[B] = !0, e
            }

            function i(e) {
                var t = P.createElement("div");
                try {
                    return !!e(t)
                } catch (e) {
                    return !1
                } finally {
                    t.parentNode && t.parentNode.removeChild(t), t = null
                }
            }

            function o(e, t) {
                for (var n = e.split("|"), r = e.length; r--;) C.attrHandle[n[r]] = t
            }

            function a(e, t) {
                var n = t && e,
                    r = n && 1 === e.nodeType && 1 === t.nodeType && (~t.sourceIndex || X) - (~e.sourceIndex || X);
                if (r) return r;
                if (n)
                    for (; n = n.nextSibling;)
                        if (n === t) return -1;
                return e ? 1 : -1
            }

            function s(e) {
                return r(function(t) {
                    return t = +t, r(function(n, r) {
                        for (var i, o = e([], n.length, t), a = o.length; a--;) n[i = o[a]] && (n[i] = !(r[i] = n[i]))
                    })
                })
            }

            function l(e) {
                return e && typeof e.getElementsByTagName !== W && e
            }

            function u() {}

            function c(e, n) {
                var r, i, o, a, s, l, u, c = R[e + " "];
                if (c) return n ? 0 : c.slice(0);
                for (s = e, l = [], u = C.preFilter; s;) {
                    (!r || (i = se.exec(s))) && (i && (s = s.slice(i[0].length) || s), l.push(o = [])), r = !1, (i = le.exec(s)) && (r = i.shift(), o.push({
                        value: r,
                        type: i[0].replace(ae, " ")
                    }), s = s.slice(r.length));
                    for (a in C.filter) !(i = fe[a].exec(s)) || u[a] && !(i = u[a](i)) || (r = i.shift(), o.push({
                        value: r,
                        type: a,
                        matches: i
                    }), s = s.slice(r.length));
                    if (!r) break
                }
                return n ? s.length : s ? t.error(e) : R(e, l).slice(0)
            }

            function d(e) {
                for (var t = 0, n = e.length, r = ""; n > t; t++) r += e[t].value;
                return r
            }

            function f(e, t, n) {
                var r = t.dir,
                    i = n && "parentNode" === r,
                    o = q++;
                return t.first ? function(t, n, o) {
                    for (; t = t[r];)
                        if (1 === t.nodeType || i) return e(t, n, o)
                } : function(t, n, a) {
                    var s, l, u = [V, o];
                    if (a) {
                        for (; t = t[r];)
                            if ((1 === t.nodeType || i) && e(t, n, a)) return !0
                    } else
                        for (; t = t[r];)
                            if (1 === t.nodeType || i) {
                                if (l = t[B] || (t[B] = {}), (s = l[r]) && s[0] === V && s[1] === o) return u[2] = s[2];
                                if (l[r] = u, u[2] = e(t, n, a)) return !0
                            }
                }
            }

            function p(e) {
                return e.length > 1 ? function(t, n, r) {
                    for (var i = e.length; i--;)
                        if (!e[i](t, n, r)) return !1;
                    return !0
                } : e[0]
            }

            function h(e, t, n, r, i) {
                for (var o, a = [], s = 0, l = e.length, u = null != t; l > s; s++)(o = e[s]) && (!n || n(o, r, i)) && (a.push(o), u && t.push(s));
                return a
            }

            function g(e, t, n, i, o, a) {
                return i && !i[B] && (i = g(i)), o && !o[B] && (o = g(o, a)), r(function(r, a, s, l) {
                    var u, c, d, f = [],
                        p = [],
                        g = a.length,
                        m = r || y(t || "*", s.nodeType ? [s] : s, []),
                        v = !e || !r && t ? m : h(m, f, e, s, l),
                        b = n ? o || (r ? e : g || i) ? [] : a : v;
                    if (n && n(v, b, s, l), i)
                        for (u = h(b, p), i(u, [], s, l), c = u.length; c--;)(d = u[c]) && (b[p[c]] = !(v[p[c]] = d));
                    if (r) {
                        if (o || e) {
                            if (o) {
                                for (u = [], c = b.length; c--;)(d = b[c]) && u.push(v[c] = d);
                                o(null, b = [], u, l)
                            }
                            for (c = b.length; c--;)(d = b[c]) && (u = o ? K.call(r, d) : f[c]) > -1 && (r[u] = !(a[u] = d))
                        }
                    } else b = h(b === a ? b.splice(g, b.length) : b), o ? o(null, a, b, l) : J.apply(a, b)
                })
            }

            function m(e) {
                for (var t, n, r, i = e.length, o = C.relative[e[0].type], a = o || C.relative[" "], s = o ? 1 : 0, l = f(function(e) {
                        return e === t
                    }, a, !0), u = f(function(e) {
                        return K.call(t, e) > -1
                    }, a, !0), c = [function(e, n, r) {
                        return !o && (r || n !== j) || ((t = n).nodeType ? l(e, n, r) : u(e, n, r))
                    }]; i > s; s++)
                    if (n = C.relative[e[s].type]) c = [f(p(c), n)];
                    else {
                        if (n = C.filter[e[s].type].apply(null, e[s].matches), n[B]) {
                            for (r = ++s; i > r && !C.relative[e[r].type]; r++);
                            return g(s > 1 && p(c), s > 1 && d(e.slice(0, s - 1).concat({
                                value: " " === e[s - 2].type ? "*" : ""
                            })).replace(ae, "$1"), n, r > s && m(e.slice(s, r)), i > r && m(e = e.slice(r)), i > r && d(e))
                        }
                        c.push(n)
                    }
                return p(c)
            }

            function v(e, n) {
                var i = n.length > 0,
                    o = e.length > 0,
                    a = function(r, a, s, l, u) {
                        var c, d, f, p = 0,
                            g = "0",
                            m = r && [],
                            v = [],
                            y = j,
                            b = r || o && C.find.TAG("*", u),
                            x = V += null == y ? 1 : Math.random() || .1,
                            w = b.length;
                        for (u && (j = a !== P && a); g !== w && null != (c = b[g]); g++) {
                            if (o && c) {
                                for (d = 0; f = e[d++];)
                                    if (f(c, a, s)) {
                                        l.push(c);
                                        break
                                    }
                                u && (V = x)
                            }
                            i && ((c = !f && c) && p--, r && m.push(c))
                        }
                        if (p += g, i && g !== p) {
                            for (d = 0; f = n[d++];) f(m, v, a, s);
                            if (r) {
                                if (p > 0)
                                    for (; g--;) m[g] || v[g] || (v[g] = G.call(l));
                                v = h(v)
                            }
                            J.apply(l, v), u && !r && v.length > 0 && p + n.length > 1 && t.uniqueSort(l)
                        }
                        return u && (V = x, j = y), m
                    };
                return i ? r(a) : a
            }

            function y(e, n, r) {
                for (var i = 0, o = n.length; o > i; i++) t(e, n[i], r);
                return r
            }

            function b(e, t, n, r) {
                var i, o, a, s, u, f = c(e);
                if (!r && 1 === f.length) {
                    if (o = f[0] = f[0].slice(0), o.length > 2 && "ID" === (a = o[0]).type && w.getById && 9 === t.nodeType && L && C.relative[o[1].type]) {
                        if (!(t = (C.find.ID(a.matches[0].replace(be, xe), t) || [])[0])) return n;
                        e = e.slice(o.shift().value.length)
                    }
                    for (i = fe.needsContext.test(e) ? 0 : o.length; i-- && (a = o[i], !C.relative[s = a.type]);)
                        if ((u = C.find[s]) && (r = u(a.matches[0].replace(be, xe), ve.test(o[0].type) && l(t.parentNode) || t))) {
                            if (o.splice(i, 1), !(e = r.length && d(o))) return J.apply(n, r), n;
                            break
                        }
                }
                return S(e, f)(r, t, !L, n, ve.test(e) && l(t.parentNode) || t), n
            }
            var x, w, C, T, k, S, j, E, N, A, P, H, L, D, M, O, F, B = "sizzle" + -new Date,
                $ = e.document,
                V = 0,
                q = 0,
                _ = n(),
                R = n(),
                z = n(),
                I = function(e, t) {
                    return e === t && (N = !0), 0
                },
                W = "undefined",
                X = 1 << 31,
                Y = {}.hasOwnProperty,
                Q = [],
                G = Q.pop,
                U = Q.push,
                J = Q.push,
                Z = Q.slice,
                K = Q.indexOf || function(e) {
                    for (var t = 0, n = this.length; n > t; t++)
                        if (this[t] === e) return t;
                    return -1
                },
                ee = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped",
                te = "[\\x20\\t\\r\\n\\f]",
                ne = "(?:\\\\.|[\\w-]|[^\\x00-\\xa0])+",
                re = ne.replace("w", "w#"),
                ie = "\\[" + te + "*(" + ne + ")" + te + "*(?:([*^$|!~]?=)" + te + "*(?:(['\"])((?:\\\\.|[^\\\\])*?)\\3|(" + re + ")|)|)" + te + "*\\]",
                oe = ":(" + ne + ")(?:\\(((['\"])((?:\\\\.|[^\\\\])*?)\\3|((?:\\\\.|[^\\\\()[\\]]|" + ie.replace(3, 8) + ")*)|.*)\\)|)",
                ae = new RegExp("^" + te + "+|((?:^|[^\\\\])(?:\\\\.)*)" + te + "+$", "g"),
                se = new RegExp("^" + te + "*," + te + "*"),
                le = new RegExp("^" + te + "*([>+~]|" + te + ")" + te + "*"),
                ue = new RegExp("=" + te + "*([^\\]'\"]*?)" + te + "*\\]", "g"),
                ce = new RegExp(oe),
                de = new RegExp("^" + re + "$"),
                fe = {
                    ID: new RegExp("^#(" + ne + ")"),
                    CLASS: new RegExp("^\\.(" + ne + ")"),
                    TAG: new RegExp("^(" + ne.replace("w", "w*") + ")"),
                    ATTR: new RegExp("^" + ie),
                    PSEUDO: new RegExp("^" + oe),
                    CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + te + "*(even|odd|(([+-]|)(\\d*)n|)" + te + "*(?:([+-]|)" + te + "*(\\d+)|))" + te + "*\\)|)", "i"),
                    bool: new RegExp("^(?:" + ee + ")$", "i"),
                    needsContext: new RegExp("^" + te + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + te + "*((?:-\\d)?\\d*)" + te + "*\\)|)(?=[^-]|$)", "i")
                },
                pe = /^(?:input|select|textarea|button)$/i,
                he = /^h\d$/i,
                ge = /^[^{]+\{\s*\[native \w/,
                me = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/,
                ve = /[+~]/,
                ye = /'|\\/g,
                be = new RegExp("\\\\([\\da-f]{1,6}" + te + "?|(" + te + ")|.)", "ig"),
                xe = function(e, t, n) {
                    var r = "0x" + t - 65536;
                    return r !== r || n ? t : 0 > r ? String.fromCharCode(r + 65536) : String.fromCharCode(r >> 10 | 55296, 1023 & r | 56320)
                };
            try {
                J.apply(Q = Z.call($.childNodes), $.childNodes), Q[$.childNodes.length].nodeType
            } catch (e) {
                J = {
                    apply: Q.length ? function(e, t) {
                        U.apply(e, Z.call(t))
                    } : function(e, t) {
                        for (var n = e.length, r = 0; e[n++] = t[r++];);
                        e.length = n - 1
                    }
                }
            }
            w = t.support = {}, k = t.isXML = function(e) {
                var t = e && (e.ownerDocument || e).documentElement;
                return !!t && "HTML" !== t.nodeName
            }, A = t.setDocument = function(e) {
                var t, n = e ? e.ownerDocument || e : $,
                    r = n.defaultView;
                return n !== P && 9 === n.nodeType && n.documentElement ? (P = n, H = n.documentElement, L = !k(n), r && r !== r.top && (r.addEventListener ? r.addEventListener("unload", function() {
                    A()
                }, !1) : r.attachEvent && r.attachEvent("onunload", function() {
                    A()
                })), w.attributes = i(function(e) {
                    return e.className = "i", !e.getAttribute("className")
                }), w.getElementsByTagName = i(function(e) {
                    return e.appendChild(n.createComment("")), !e.getElementsByTagName("*").length
                }), w.getElementsByClassName = ge.test(n.getElementsByClassName) && i(function(e) {
                    return e.innerHTML = "<div class='a'></div><div class='a i'></div>", e.firstChild.className = "i", 2 === e.getElementsByClassName("i").length
                }), w.getById = i(function(e) {
                    return H.appendChild(e).id = B, !n.getElementsByName || !n.getElementsByName(B).length
                }), w.getById ? (C.find.ID = function(e, t) {
                    if (typeof t.getElementById !== W && L) {
                        var n = t.getElementById(e);
                        return n && n.parentNode ? [n] : []
                    }
                }, C.filter.ID = function(e) {
                    var t = e.replace(be, xe);
                    return function(e) {
                        return e.getAttribute("id") === t
                    }
                }) : (delete C.find.ID, C.filter.ID = function(e) {
                    var t = e.replace(be, xe);
                    return function(e) {
                        var n = typeof e.getAttributeNode !== W && e.getAttributeNode("id");
                        return n && n.value === t
                    }
                }), C.find.TAG = w.getElementsByTagName ? function(e, t) {
                    return typeof t.getElementsByTagName !== W ? t.getElementsByTagName(e) : void 0
                } : function(e, t) {
                    var n, r = [],
                        i = 0,
                        o = t.getElementsByTagName(e);
                    if ("*" === e) {
                        for (; n = o[i++];) 1 === n.nodeType && r.push(n);
                        return r
                    }
                    return o
                }, C.find.CLASS = w.getElementsByClassName && function(e, t) {
                    return typeof t.getElementsByClassName !== W && L ? t.getElementsByClassName(e) : void 0
                }, M = [], D = [], (w.qsa = ge.test(n.querySelectorAll)) && (i(function(e) {
                    e.innerHTML = "<select t=''><option selected=''></option></select>", e.querySelectorAll("[t^='']").length && D.push("[*^$]=" + te + "*(?:''|\"\")"), e.querySelectorAll("[selected]").length || D.push("\\[" + te + "*(?:value|" + ee + ")"), e.querySelectorAll(":checked").length || D.push(":checked")
                }), i(function(e) {
                    var t = n.createElement("input");
                    t.setAttribute("type", "hidden"), e.appendChild(t).setAttribute("name", "D"), e.querySelectorAll("[name=d]").length && D.push("name" + te + "*[*^$|!~]?="), e.querySelectorAll(":enabled").length || D.push(":enabled", ":disabled"), e.querySelectorAll("*,:x"), D.push(",.*:")
                })), (w.matchesSelector = ge.test(O = H.webkitMatchesSelector || H.mozMatchesSelector || H.oMatchesSelector || H.msMatchesSelector)) && i(function(e) {
                    w.disconnectedMatch = O.call(e, "div"), O.call(e, "[s!='']:x"), M.push("!=", oe)
                }), D = D.length && new RegExp(D.join("|")), M = M.length && new RegExp(M.join("|")), t = ge.test(H.compareDocumentPosition), F = t || ge.test(H.contains) ? function(e, t) {
                    var n = 9 === e.nodeType ? e.documentElement : e,
                        r = t && t.parentNode;
                    return e === r || !(!r || 1 !== r.nodeType || !(n.contains ? n.contains(r) : e.compareDocumentPosition && 16 & e.compareDocumentPosition(r)))
                } : function(e, t) {
                    if (t)
                        for (; t = t.parentNode;)
                            if (t === e) return !0;
                    return !1
                }, I = t ? function(e, t) {
                    if (e === t) return N = !0, 0;
                    var r = !e.compareDocumentPosition - !t.compareDocumentPosition;
                    return r ? r : (r = (e.ownerDocument || e) === (t.ownerDocument || t) ? e.compareDocumentPosition(t) : 1, 1 & r || !w.sortDetached && t.compareDocumentPosition(e) === r ? e === n || e.ownerDocument === $ && F($, e) ? -1 : t === n || t.ownerDocument === $ && F($, t) ? 1 : E ? K.call(E, e) - K.call(E, t) : 0 : 4 & r ? -1 : 1)
                } : function(e, t) {
                    if (e === t) return N = !0, 0;
                    var r, i = 0,
                        o = e.parentNode,
                        s = t.parentNode,
                        l = [e],
                        u = [t];
                    if (!o || !s) return e === n ? -1 : t === n ? 1 : o ? -1 : s ? 1 : E ? K.call(E, e) - K.call(E, t) : 0;
                    if (o === s) return a(e, t);
                    for (r = e; r = r.parentNode;) l.unshift(r);
                    for (r = t; r = r.parentNode;) u.unshift(r);
                    for (; l[i] === u[i];) i++;
                    return i ? a(l[i], u[i]) : l[i] === $ ? -1 : u[i] === $ ? 1 : 0
                }, n) : P
            }, t.matches = function(e, n) {
                return t(e, null, null, n)
            }, t.matchesSelector = function(e, n) {
                if ((e.ownerDocument || e) !== P && A(e), n = n.replace(ue, "='$1']"), !(!w.matchesSelector || !L || M && M.test(n) || D && D.test(n))) try {
                    var r = O.call(e, n);
                    if (r || w.disconnectedMatch || e.document && 11 !== e.document.nodeType) return r
                } catch (e) {}
                return t(n, P, null, [e]).length > 0
            }, t.contains = function(e, t) {
                return (e.ownerDocument || e) !== P && A(e), F(e, t)
            }, t.attr = function(e, t) {
                (e.ownerDocument || e) !== P && A(e);
                var n = C.attrHandle[t.toLowerCase()],
                    r = n && Y.call(C.attrHandle, t.toLowerCase()) ? n(e, t, !L) : void 0;
                return void 0 !== r ? r : w.attributes || !L ? e.getAttribute(t) : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
            }, t.error = function(e) {
                throw new Error("Syntax error, unrecognized expression: " + e)
            }, t.uniqueSort = function(e) {
                var t, n = [],
                    r = 0,
                    i = 0;
                if (N = !w.detectDuplicates, E = !w.sortStable && e.slice(0), e.sort(I), N) {
                    for (; t = e[i++];) t === e[i] && (r = n.push(i));
                    for (; r--;) e.splice(n[r], 1)
                }
                return E = null, e
            }, T = t.getText = function(e) {
                var t, n = "",
                    r = 0,
                    i = e.nodeType;
                if (i) {
                    if (1 === i || 9 === i || 11 === i) {
                        if ("string" == typeof e.textContent) return e.textContent;
                        for (e = e.firstChild; e; e = e.nextSibling) n += T(e)
                    } else if (3 === i || 4 === i) return e.nodeValue
                } else
                    for (; t = e[r++];) n += T(t);
                return n
            }, C = t.selectors = {
                cacheLength: 50,
                createPseudo: r,
                match: fe,
                attrHandle: {},
                find: {},
                relative: {
                    ">": {
                        dir: "parentNode",
                        first: !0
                    },
                    " ": {
                        dir: "parentNode"
                    },
                    "+": {
                        dir: "previousSibling",
                        first: !0
                    },
                    "~": {
                        dir: "previousSibling"
                    }
                },
                preFilter: {
                    ATTR: function(e) {
                        return e[1] = e[1].replace(be, xe), e[3] = (e[4] || e[5] || "").replace(be, xe), "~=" === e[2] && (e[3] = " " + e[3] + " "), e.slice(0, 4)
                    },
                    CHILD: function(e) {
                        return e[1] = e[1].toLowerCase(), "nth" === e[1].slice(0, 3) ? (e[3] || t.error(e[0]), e[4] = +(e[4] ? e[5] + (e[6] || 1) : 2 * ("even" === e[3] || "odd" === e[3])), e[5] = +(e[7] + e[8] || "odd" === e[3])) : e[3] && t.error(e[0]), e
                    },
                    PSEUDO: function(e) {
                        var t, n = !e[5] && e[2];
                        return fe.CHILD.test(e[0]) ? null : (e[3] && void 0 !== e[4] ? e[2] = e[4] : n && ce.test(n) && (t = c(n, !0)) && (t = n.indexOf(")", n.length - t) - n.length) && (e[0] = e[0].slice(0, t), e[2] = n.slice(0, t)), e.slice(0, 3))
                    }
                },
                filter: {
                    TAG: function(e) {
                        var t = e.replace(be, xe).toLowerCase();
                        return "*" === e ? function() {
                            return !0
                        } : function(e) {
                            return e.nodeName && e.nodeName.toLowerCase() === t
                        }
                    },
                    CLASS: function(e) {
                        var t = _[e + " "];
                        return t || (t = new RegExp("(^|" + te + ")" + e + "(" + te + "|$)")) && _(e, function(e) {
                            return t.test("string" == typeof e.className && e.className || typeof e.getAttribute !== W && e.getAttribute("class") || "")
                        })
                    },
                    ATTR: function(e, n, r) {
                        return function(i) {
                            var o = t.attr(i, e);
                            return null == o ? "!=" === n : !n || (o += "", "=" === n ? o === r : "!=" === n ? o !== r : "^=" === n ? r && 0 === o.indexOf(r) : "*=" === n ? r && o.indexOf(r) > -1 : "$=" === n ? r && o.slice(-r.length) === r : "~=" === n ? (" " + o + " ").indexOf(r) > -1 : "|=" === n && (o === r || o.slice(0, r.length + 1) === r + "-"))
                        }
                    },
                    CHILD: function(e, t, n, r, i) {
                        var o = "nth" !== e.slice(0, 3),
                            a = "last" !== e.slice(-4),
                            s = "of-type" === t;
                        return 1 === r && 0 === i ? function(e) {
                            return !!e.parentNode
                        } : function(t, n, l) {
                            var u, c, d, f, p, h, g = o !== a ? "nextSibling" : "previousSibling",
                                m = t.parentNode,
                                v = s && t.nodeName.toLowerCase(),
                                y = !l && !s;
                            if (m) {
                                if (o) {
                                    for (; g;) {
                                        for (d = t; d = d[g];)
                                            if (s ? d.nodeName.toLowerCase() === v : 1 === d.nodeType) return !1;
                                        h = g = "only" === e && !h && "nextSibling"
                                    }
                                    return !0
                                }
                                if (h = [a ? m.firstChild : m.lastChild], a && y) {
                                    for (c = m[B] || (m[B] = {}), u = c[e] || [], p = u[0] === V && u[1], f = u[0] === V && u[2], d = p && m.childNodes[p]; d = ++p && d && d[g] || (f = p = 0) || h.pop();)
                                        if (1 === d.nodeType && ++f && d === t) {
                                            c[e] = [V, p, f];
                                            break
                                        }
                                } else if (y && (u = (t[B] || (t[B] = {}))[e]) && u[0] === V) f = u[1];
                                else
                                    for (;
                                        (d = ++p && d && d[g] || (f = p = 0) || h.pop()) && ((s ? d.nodeName.toLowerCase() !== v : 1 !== d.nodeType) || !++f || (y && ((d[B] || (d[B] = {}))[e] = [V, f]), d !== t)););
                                return (f -= i) === r || f % r == 0 && f / r >= 0
                            }
                        }
                    },
                    PSEUDO: function(e, n) {
                        var i, o = C.pseudos[e] || C.setFilters[e.toLowerCase()] || t.error("unsupported pseudo: " + e);
                        return o[B] ? o(n) : o.length > 1 ? (i = [e, e, "", n], C.setFilters.hasOwnProperty(e.toLowerCase()) ? r(function(e, t) {
                            for (var r, i = o(e, n), a = i.length; a--;) r = K.call(e, i[a]), e[r] = !(t[r] = i[a])
                        }) : function(e) {
                            return o(e, 0, i)
                        }) : o
                    }
                },
                pseudos: {
                    not: r(function(e) {
                        var t = [],
                            n = [],
                            i = S(e.replace(ae, "$1"));
                        return i[B] ? r(function(e, t, n, r) {
                            for (var o, a = i(e, null, r, []), s = e.length; s--;)(o = a[s]) && (e[s] = !(t[s] = o))
                        }) : function(e, r, o) {
                            return t[0] = e, i(t, null, o, n), !n.pop()
                        }
                    }),
                    has: r(function(e) {
                        return function(n) {
                            return t(e, n).length > 0
                        }
                    }),
                    contains: r(function(e) {
                        return function(t) {
                            return (t.textContent || t.innerText || T(t)).indexOf(e) > -1
                        }
                    }),
                    lang: r(function(e) {
                        return de.test(e || "") || t.error("unsupported lang: " + e), e = e.replace(be, xe).toLowerCase(),
                            function(t) {
                                var n;
                                do {
                                    if (n = L ? t.lang : t.getAttribute("xml:lang") || t.getAttribute("lang")) return (n = n.toLowerCase()) === e || 0 === n.indexOf(e + "-")
                                } while ((t = t.parentNode) && 1 === t.nodeType);
                                return !1
                            }
                    }),
                    target: function(t) {
                        var n = e.location && e.location.hash;
                        return n && n.slice(1) === t.id
                    },
                    root: function(e) {
                        return e === H
                    },
                    focus: function(e) {
                        return e === P.activeElement && (!P.hasFocus || P.hasFocus()) && !!(e.type || e.href || ~e.tabIndex)
                    },
                    enabled: function(e) {
                        return e.disabled === !1
                    },
                    disabled: function(e) {
                        return e.disabled === !0
                    },
                    checked: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && !!e.checked || "option" === t && !!e.selected
                    },
                    selected: function(e) {
                        return e.parentNode && e.parentNode.selectedIndex, e.selected === !0
                    },
                    empty: function(e) {
                        for (e = e.firstChild; e; e = e.nextSibling)
                            if (e.nodeType < 6) return !1;
                        return !0
                    },
                    parent: function(e) {
                        return !C.pseudos.empty(e)
                    },
                    header: function(e) {
                        return he.test(e.nodeName)
                    },
                    input: function(e) {
                        return pe.test(e.nodeName)
                    },
                    button: function(e) {
                        var t = e.nodeName.toLowerCase();
                        return "input" === t && "button" === e.type || "button" === t
                    },
                    text: function(e) {
                        var t;
                        return "input" === e.nodeName.toLowerCase() && "text" === e.type && (null == (t = e.getAttribute("type")) || "text" === t.toLowerCase())
                    },
                    first: s(function() {
                        return [0]
                    }),
                    last: s(function(e, t) {
                        return [t - 1]
                    }),
                    eq: s(function(e, t, n) {
                        return [0 > n ? n + t : n]
                    }),
                    even: s(function(e, t) {
                        for (var n = 0; t > n; n += 2) e.push(n);
                        return e
                    }),
                    odd: s(function(e, t) {
                        for (var n = 1; t > n; n += 2) e.push(n);
                        return e
                    }),
                    lt: s(function(e, t, n) {
                        for (var r = 0 > n ? n + t : n; --r >= 0;) e.push(r);
                        return e
                    }),
                    gt: s(function(e, t, n) {
                        for (var r = 0 > n ? n + t : n; ++r < t;) e.push(r);
                        return e
                    })
                }
            }, C.pseudos.nth = C.pseudos.eq;
            for (x in {
                    radio: !0,
                    checkbox: !0,
                    file: !0,
                    password: !0,
                    image: !0
                }) C.pseudos[x] = function(e) {
                return function(t) {
                    return "input" === t.nodeName.toLowerCase() && t.type === e
                }
            }(x);
            for (x in {
                    submit: !0,
                    reset: !0
                }) C.pseudos[x] = function(e) {
                return function(t) {
                    var n = t.nodeName.toLowerCase();
                    return ("input" === n || "button" === n) && t.type === e
                }
            }(x);
            return u.prototype = C.filters = C.pseudos, C.setFilters = new u, S = t.compile = function(e, t) {
                var n, r = [],
                    i = [],
                    o = z[e + " "];
                if (!o) {
                    for (t || (t = c(e)), n = t.length; n--;) o = m(t[n]), o[B] ? r.push(o) : i.push(o);
                    o = z(e, v(i, r))
                }
                return o
            }, w.sortStable = B.split("").sort(I).join("") === B, w.detectDuplicates = !!N, A(), w.sortDetached = i(function(e) {
                return 1 & e.compareDocumentPosition(P.createElement("div"))
            }), i(function(e) {
                return e.innerHTML = "<a href='#'></a>", "#" === e.firstChild.getAttribute("href")
            }) || o("type|href|height|width", function(e, t, n) {
                return n ? void 0 : e.getAttribute(t, "type" === t.toLowerCase() ? 1 : 2)
            }), w.attributes && i(function(e) {
                return e.innerHTML = "<input/>", e.firstChild.setAttribute("value", ""), "" === e.firstChild.getAttribute("value")
            }) || o("value", function(e, t, n) {
                return n || "input" !== e.nodeName.toLowerCase() ? void 0 : e.defaultValue
            }), i(function(e) {
                return null == e.getAttribute("disabled")
            }) || o(ee, function(e, t, n) {
                var r;
                return n ? void 0 : e[t] === !0 ? t.toLowerCase() : (r = e.getAttributeNode(t)) && r.specified ? r.value : null
            }), t
        }(e);
        oe.find = se, oe.expr = se.selectors, oe.expr[":"] = oe.expr.pseudos, oe.unique = se.uniqueSort, oe.text = se.getText, oe.isXMLDoc = se.isXML, oe.contains = se.contains;
        var le = oe.expr.match.needsContext,
            ue = /^<(\w+)\s*\/?>(?:<\/\1>|)$/,
            ce = /^.[^:#\[\.,]*$/;
        oe.filter = function(e, t, n) {
            var r = t[0];
            return n && (e = ":not(" + e + ")"), 1 === t.length && 1 === r.nodeType ? oe.find.matchesSelector(r, e) ? [r] : [] : oe.find.matches(e, oe.grep(t, function(e) {
                return 1 === e.nodeType
            }))
        }, oe.fn.extend({
            find: function(e) {
                var t, n = [],
                    r = this,
                    i = r.length;
                if ("string" != typeof e) return this.pushStack(oe(e).filter(function() {
                    for (t = 0; i > t; t++)
                        if (oe.contains(r[t], this)) return !0
                }));
                for (t = 0; i > t; t++) oe.find(e, r[t], n);
                return n = this.pushStack(i > 1 ? oe.unique(n) : n), n.selector = this.selector ? this.selector + " " + e : e, n
            },
            filter: function(e) {
                return this.pushStack(r(this, e || [], !1))
            },
            not: function(e) {
                return this.pushStack(r(this, e || [], !0))
            },
            is: function(e) {
                return !!r(this, "string" == typeof e && le.test(e) ? oe(e) : e || [], !1).length
            }
        });
        var de, fe = e.document,
            pe = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]*))$/;
        (oe.fn.init = function(e, t) {
            var n, r;
            if (!e) return this;
            if ("string" == typeof e) {
                if (!(n = "<" === e.charAt(0) && ">" === e.charAt(e.length - 1) && e.length >= 3 ? [null, e, null] : pe.exec(e)) || !n[1] && t) return !t || t.jquery ? (t || de).find(e) : this.constructor(t).find(e);
                if (n[1]) {
                    if (t = t instanceof oe ? t[0] : t, oe.merge(this, oe.parseHTML(n[1], t && t.nodeType ? t.ownerDocument || t : fe, !0)), ue.test(n[1]) && oe.isPlainObject(t))
                        for (n in t) oe.isFunction(this[n]) ? this[n](t[n]) : this.attr(n, t[n]);
                    return this
                }
                if ((r = fe.getElementById(n[2])) && r.parentNode) {
                    if (r.id !== n[2]) return de.find(e);
                    this.length = 1, this[0] = r
                }
                return this.context = fe, this.selector = e, this
            }
            return e.nodeType ? (this.context = this[0] = e, this.length = 1, this) : oe.isFunction(e) ? void 0 !== de.ready ? de.ready(e) : e(oe) : (void 0 !== e.selector && (this.selector = e.selector, this.context = e.context), oe.makeArray(e, this))
        }).prototype = oe.fn, de = oe(fe);
        var he = /^(?:parents|prev(?:Until|All))/,
            ge = {
                children: !0,
                contents: !0,
                next: !0,
                prev: !0
            };
        oe.extend({
            dir: function(e, t, n) {
                for (var r = [], i = e[t]; i && 9 !== i.nodeType && (void 0 === n || 1 !== i.nodeType || !oe(i).is(n));) 1 === i.nodeType && r.push(i), i = i[t];
                return r
            },
            sibling: function(e, t) {
                for (var n = []; e; e = e.nextSibling) 1 === e.nodeType && e !== t && n.push(e);
                return n
            }
        }), oe.fn.extend({
            has: function(e) {
                var t, n = oe(e, this),
                    r = n.length;
                return this.filter(function() {
                    for (t = 0; r > t; t++)
                        if (oe.contains(this, n[t])) return !0
                })
            },
            closest: function(e, t) {
                for (var n, r = 0, i = this.length, o = [], a = le.test(e) || "string" != typeof e ? oe(e, t || this.context) : 0; i > r; r++)
                    for (n = this[r]; n && n !== t; n = n.parentNode)
                        if (n.nodeType < 11 && (a ? a.index(n) > -1 : 1 === n.nodeType && oe.find.matchesSelector(n, e))) {
                            o.push(n);
                            break
                        }
                return this.pushStack(o.length > 1 ? oe.unique(o) : o)
            },
            index: function(e) {
                return e ? "string" == typeof e ? oe.inArray(this[0], oe(e)) : oe.inArray(e.jquery ? e[0] : e, this) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
            },
            add: function(e, t) {
                return this.pushStack(oe.unique(oe.merge(this.get(), oe(e, t))))
            },
            addBack: function(e) {
                return this.add(null == e ? this.prevObject : this.prevObject.filter(e))
            }
        }), oe.each({
            parent: function(e) {
                var t = e.parentNode;
                return t && 11 !== t.nodeType ? t : null
            },
            parents: function(e) {
                return oe.dir(e, "parentNode")
            },
            parentsUntil: function(e, t, n) {
                return oe.dir(e, "parentNode", n)
            },
            next: function(e) {
                return i(e, "nextSibling")
            },
            prev: function(e) {
                return i(e, "previousSibling")
            },
            nextAll: function(e) {
                return oe.dir(e, "nextSibling")
            },
            prevAll: function(e) {
                return oe.dir(e, "previousSibling")
            },
            nextUntil: function(e, t, n) {
                return oe.dir(e, "nextSibling", n)
            },
            prevUntil: function(e, t, n) {
                return oe.dir(e, "previousSibling", n)
            },
            siblings: function(e) {
                return oe.sibling((e.parentNode || {}).firstChild, e)
            },
            children: function(e) {
                return oe.sibling(e.firstChild)
            },
            contents: function(e) {
                return oe.nodeName(e, "iframe") ? e.contentDocument || e.contentWindow.document : oe.merge([], e.childNodes)
            }
        }, function(e, t) {
            oe.fn[e] = function(n, r) {
                var i = oe.map(this, t, n);
                return "Until" !== e.slice(-5) && (r = n), r && "string" == typeof r && (i = oe.filter(r, i)), this.length > 1 && (ge[e] || (i = oe.unique(i)), he.test(e) && (i = i.reverse())), this.pushStack(i)
            }
        });
        var me = /\S+/g,
            ve = {};
        oe.Callbacks = function(e) {
            e = "string" == typeof e ? ve[e] || o(e) : oe.extend({}, e);
            var t, n, r, i, a, s, l = [],
                u = !e.once && [],
                c = function(o) {
                    for (n = e.memory && o, r = !0, a = s || 0, s = 0, i = l.length, t = !0; l && i > a; a++)
                        if (l[a].apply(o[0], o[1]) === !1 && e.stopOnFalse) {
                            n = !1;
                            break
                        }
                    t = !1, l && (u ? u.length && c(u.shift()) : n ? l = [] : d.disable())
                },
                d = {
                    add: function() {
                        if (l) {
                            var r = l.length;
                            ! function t(n) {
                                oe.each(n, function(n, r) {
                                    var i = oe.type(r);
                                    "function" === i ? e.unique && d.has(r) || l.push(r) : r && r.length && "string" !== i && t(r)
                                })
                            }(arguments), t ? i = l.length : n && (s = r, c(n))
                        }
                        return this
                    },
                    remove: function() {
                        return l && oe.each(arguments, function(e, n) {
                            for (var r;
                                (r = oe.inArray(n, l, r)) > -1;) l.splice(r, 1), t && (i >= r && i--, a >= r && a--)
                        }), this
                    },
                    has: function(e) {
                        return e ? oe.inArray(e, l) > -1 : !(!l || !l.length)
                    },
                    empty: function() {
                        return l = [], i = 0, this
                    },
                    disable: function() {
                        return l = u = n = void 0, this
                    },
                    disabled: function() {
                        return !l
                    },
                    lock: function() {
                        return u = void 0, n || d.disable(), this
                    },
                    locked: function() {
                        return !u
                    },
                    fireWith: function(e, n) {
                        return !l || r && !u || (n = n || [], n = [e, n.slice ? n.slice() : n], t ? u.push(n) : c(n)), this
                    },
                    fire: function() {
                        return d.fireWith(this, arguments), this
                    },
                    fired: function() {
                        return !!r
                    }
                };
            return d
        }, oe.extend({
            Deferred: function(e) {
                var t = [
                        ["resolve", "done", oe.Callbacks("once memory"), "resolved"],
                        ["reject", "fail", oe.Callbacks("once memory"), "rejected"],
                        ["notify", "progress", oe.Callbacks("memory")]
                    ],
                    n = "pending",
                    r = {
                        state: function() {
                            return n
                        },
                        always: function() {
                            return i.done(arguments).fail(arguments), this
                        },
                        then: function() {
                            var e = arguments;
                            return oe.Deferred(function(n) {
                                oe.each(t, function(t, o) {
                                    var a = oe.isFunction(e[t]) && e[t];
                                    i[o[1]](function() {
                                        var e = a && a.apply(this, arguments);
                                        e && oe.isFunction(e.promise) ? e.promise().done(n.resolve).fail(n.reject).progress(n.notify) : n[o[0] + "With"](this === r ? n.promise() : this, a ? [e] : arguments)
                                    })
                                }), e = null
                            }).promise()
                        },
                        promise: function(e) {
                            return null != e ? oe.extend(e, r) : r
                        }
                    },
                    i = {};
                return r.pipe = r.then, oe.each(t, function(e, o) {
                    var a = o[2],
                        s = o[3];
                    r[o[1]] = a.add, s && a.add(function() {
                        n = s
                    }, t[1 ^ e][2].disable, t[2][2].lock), i[o[0]] = function() {
                        return i[o[0] + "With"](this === i ? r : this, arguments), this
                    }, i[o[0] + "With"] = a.fireWith
                }), r.promise(i), e && e.call(i, i), i
            },
            when: function(e) {
                var t, n, r, i = 0,
                    o = G.call(arguments),
                    a = o.length,
                    s = 1 !== a || e && oe.isFunction(e.promise) ? a : 0,
                    l = 1 === s ? e : oe.Deferred(),
                    u = function(e, n, r) {
                        return function(i) {
                            n[e] = this, r[e] = arguments.length > 1 ? G.call(arguments) : i, r === t ? l.notifyWith(n, r) : --s || l.resolveWith(n, r)
                        }
                    };
                if (a > 1)
                    for (t = new Array(a), n = new Array(a), r = new Array(a); a > i; i++) o[i] && oe.isFunction(o[i].promise) ? o[i].promise().done(u(i, r, o)).fail(l.reject).progress(u(i, n, t)) : --s;
                return s || l.resolveWith(r, o), l.promise()
            }
        });
        var ye;
        oe.fn.ready = function(e) {
            return oe.ready.promise().done(e), this
        }, oe.extend({
            isReady: !1,
            readyWait: 1,
            holdReady: function(e) {
                e ? oe.readyWait++ : oe.ready(!0)
            },
            ready: function(e) {
                if (e === !0 ? !--oe.readyWait : !oe.isReady) {
                    if (!fe.body) return setTimeout(oe.ready);
                    oe.isReady = !0, e !== !0 && --oe.readyWait > 0 || (ye.resolveWith(fe, [oe]), oe.fn.trigger && oe(fe).trigger("ready").off("ready"))
                }
            }
        }), oe.ready.promise = function(t) {
            if (!ye)
                if (ye = oe.Deferred(), "complete" === fe.readyState) setTimeout(oe.ready);
                else if (fe.addEventListener) fe.addEventListener("DOMContentLoaded", s, !1), e.addEventListener("load", s, !1);
            else {
                fe.attachEvent("onreadystatechange", s), e.attachEvent("onload", s);
                var n = !1;
                try {
                    n = null == e.frameElement && fe.documentElement
                } catch (e) {}
                n && n.doScroll && function e() {
                    if (!oe.isReady) {
                        try {
                            n.doScroll("left")
                        } catch (t) {
                            return setTimeout(e, 50)
                        }
                        a(), oe.ready()
                    }
                }()
            }
            return ye.promise(t)
        };
        var be, xe = "undefined";
        for (be in oe(re)) break;
        re.ownLast = "0" !== be, re.inlineBlockNeedsLayout = !1, oe(function() {
                var e, t, n = fe.getElementsByTagName("body")[0];
                n && (e = fe.createElement("div"), e.style.cssText = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px;margin-top:1px", t = fe.createElement("div"), n.appendChild(e).appendChild(t), typeof t.style.zoom !== xe && (t.style.cssText = "border:0;margin:0;width:1px;padding:1px;display:inline;zoom:1", (re.inlineBlockNeedsLayout = 3 === t.offsetWidth) && (n.style.zoom = 1)), n.removeChild(e), e = t = null)
            }),
            function() {
                var e = fe.createElement("div");
                if (null == re.deleteExpando) {
                    re.deleteExpando = !0;
                    try {
                        delete e.test
                    } catch (e) {
                        re.deleteExpando = !1
                    }
                }
                e = null
            }(), oe.acceptData = function(e) {
                var t = oe.noData[(e.nodeName + " ").toLowerCase()],
                    n = +e.nodeType || 1;
                return (1 === n || 9 === n) && (!t || t !== !0 && e.getAttribute("classid") === t)
            };
        var we = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/,
            Ce = /([A-Z])/g;
        oe.extend({
            cache: {},
            noData: {
                "applet ": !0,
                "embed ": !0,
                "object ": "clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"
            },
            hasData: function(e) {
                return !!(e = e.nodeType ? oe.cache[e[oe.expando]] : e[oe.expando]) && !u(e)
            },
            data: function(e, t, n) {
                return c(e, t, n)
            },
            removeData: function(e, t) {
                return d(e, t)
            },
            _data: function(e, t, n) {
                return c(e, t, n, !0)
            },
            _removeData: function(e, t) {
                return d(e, t, !0)
            }
        }), oe.fn.extend({
            data: function(e, t) {
                var n, r, i, o = this[0],
                    a = o && o.attributes;
                if (void 0 === e) {
                    if (this.length && (i = oe.data(o), 1 === o.nodeType && !oe._data(o, "parsedAttrs"))) {
                        for (n = a.length; n--;) r = a[n].name, 0 === r.indexOf("data-") && (r = oe.camelCase(r.slice(5)), l(o, r, i[r]));
                        oe._data(o, "parsedAttrs", !0)
                    }
                    return i
                }
                return "object" == typeof e ? this.each(function() {
                    oe.data(this, e)
                }) : arguments.length > 1 ? this.each(function() {
                    oe.data(this, e, t)
                }) : o ? l(o, e, oe.data(o, e)) : void 0
            },
            removeData: function(e) {
                return this.each(function() {
                    oe.removeData(this, e)
                })
            }
        }), oe.extend({
            queue: function(e, t, n) {
                var r;
                return e ? (t = (t || "fx") + "queue", r = oe._data(e, t), n && (!r || oe.isArray(n) ? r = oe._data(e, t, oe.makeArray(n)) : r.push(n)), r || []) : void 0
            },
            dequeue: function(e, t) {
                t = t || "fx";
                var n = oe.queue(e, t),
                    r = n.length,
                    i = n.shift(),
                    o = oe._queueHooks(e, t),
                    a = function() {
                        oe.dequeue(e, t)
                    };
                "inprogress" === i && (i = n.shift(), r--), i && ("fx" === t && n.unshift("inprogress"), delete o.stop, i.call(e, a, o)), !r && o && o.empty.fire()
            },
            _queueHooks: function(e, t) {
                var n = t + "queueHooks";
                return oe._data(e, n) || oe._data(e, n, {
                    empty: oe.Callbacks("once memory").add(function() {
                        oe._removeData(e, t + "queue"), oe._removeData(e, n)
                    })
                })
            }
        }), oe.fn.extend({
            queue: function(e, t) {
                var n = 2;
                return "string" != typeof e && (t = e, e = "fx", n--), arguments.length < n ? oe.queue(this[0], e) : void 0 === t ? this : this.each(function() {
                    var n = oe.queue(this, e, t);
                    oe._queueHooks(this, e), "fx" === e && "inprogress" !== n[0] && oe.dequeue(this, e)
                })
            },
            dequeue: function(e) {
                return this.each(function() {
                    oe.dequeue(this, e)
                })
            },
            clearQueue: function(e) {
                return this.queue(e || "fx", [])
            },
            promise: function(e, t) {
                var n, r = 1,
                    i = oe.Deferred(),
                    o = this,
                    a = this.length,
                    s = function() {
                        --r || i.resolveWith(o, [o])
                    };
                for ("string" != typeof e && (t = e, e = void 0), e = e || "fx"; a--;)(n = oe._data(o[a], e + "queueHooks")) && n.empty && (r++, n.empty.add(s));
                return s(), i.promise(t)
            }
        });
        var Te = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source,
            ke = ["Top", "Right", "Bottom", "Left"],
            Se = function(e, t) {
                return e = t || e, "none" === oe.css(e, "display") || !oe.contains(e.ownerDocument, e)
            },
            je = oe.access = function(e, t, n, r, i, o, a) {
                var s = 0,
                    l = e.length,
                    u = null == n;
                if ("object" === oe.type(n)) {
                    i = !0;
                    for (s in n) oe.access(e, t, s, n[s], !0, o, a)
                } else if (void 0 !== r && (i = !0, oe.isFunction(r) || (a = !0), u && (a ? (t.call(e, r), t = null) : (u = t, t = function(e, t, n) {
                        return u.call(oe(e), n)
                    })), t))
                    for (; l > s; s++) t(e[s], n, a ? r : r.call(e[s], s, t(e[s], n)));
                return i ? e : u ? t.call(e) : l ? t(e[0], n) : o
            },
            Ee = /^(?:checkbox|radio)$/i;
        ! function() {
            var e = fe.createDocumentFragment(),
                t = fe.createElement("div"),
                n = fe.createElement("input");
            if (t.setAttribute("className", "t"), t.innerHTML = "  <link/><table></table><a href='/a'>a</a>", re.leadingWhitespace = 3 === t.firstChild.nodeType, re.tbody = !t.getElementsByTagName("tbody").length, re.htmlSerialize = !!t.getElementsByTagName("link").length, re.html5Clone = "<:nav></:nav>" !== fe.createElement("nav").cloneNode(!0).outerHTML, n.type = "checkbox", n.checked = !0, e.appendChild(n), re.appendChecked = n.checked, t.innerHTML = "<textarea>x</textarea>", re.noCloneChecked = !!t.cloneNode(!0).lastChild.defaultValue, e.appendChild(t), t.innerHTML = "<input type='radio' checked='checked' name='t'/>", re.checkClone = t.cloneNode(!0).cloneNode(!0).lastChild.checked, re.noCloneEvent = !0, t.attachEvent && (t.attachEvent("onclick", function() {
                    re.noCloneEvent = !1
                }), t.cloneNode(!0).click()), null == re.deleteExpando) {
                re.deleteExpando = !0;
                try {
                    delete t.test
                } catch (e) {
                    re.deleteExpando = !1
                }
            }
            e = t = n = null
        }(),
        function() {
            var t, n, r = fe.createElement("div");
            for (t in {
                    submit: !0,
                    change: !0,
                    focusin: !0
                }) n = "on" + t, (re[t + "Bubbles"] = n in e) || (r.setAttribute(n, "t"), re[t + "Bubbles"] = r.attributes[n].expando === !1);
            r = null
        }();
        var Ne = /^(?:input|select|textarea)$/i,
            Ae = /^key/,
            Pe = /^(?:mouse|contextmenu)|click/,
            He = /^(?:focusinfocus|focusoutblur)$/,
            Le = /^([^.]*)(?:\.(.+)|)$/;
        oe.event = {
            global: {},
            add: function(e, t, n, r, i) {
                var o, a, s, l, u, c, d, f, p, h, g, m = oe._data(e);
                if (m) {
                    for (n.handler && (l = n, n = l.handler, i = l.selector), n.guid || (n.guid = oe.guid++), (a = m.events) || (a = m.events = {}), (c = m.handle) || (c = m.handle = function(e) {
                            return typeof oe === xe || e && oe.event.triggered === e.type ? void 0 : oe.event.dispatch.apply(c.elem, arguments)
                        }, c.elem = e), t = (t || "").match(me) || [""], s = t.length; s--;) o = Le.exec(t[s]) || [], p = g = o[1], h = (o[2] || "").split(".").sort(), p && (u = oe.event.special[p] || {}, p = (i ? u.delegateType : u.bindType) || p, u = oe.event.special[p] || {}, d = oe.extend({
                        type: p,
                        origType: g,
                        data: r,
                        handler: n,
                        guid: n.guid,
                        selector: i,
                        needsContext: i && oe.expr.match.needsContext.test(i),
                        namespace: h.join(".")
                    }, l), (f = a[p]) || (f = a[p] = [], f.delegateCount = 0, u.setup && u.setup.call(e, r, h, c) !== !1 || (e.addEventListener ? e.addEventListener(p, c, !1) : e.attachEvent && e.attachEvent("on" + p, c))), u.add && (u.add.call(e, d), d.handler.guid || (d.handler.guid = n.guid)), i ? f.splice(f.delegateCount++, 0, d) : f.push(d), oe.event.global[p] = !0);
                    e = null
                }
            },
            remove: function(e, t, n, r, i) {
                var o, a, s, l, u, c, d, f, p, h, g, m = oe.hasData(e) && oe._data(e);
                if (m && (c = m.events)) {
                    for (t = (t || "").match(me) || [""], u = t.length; u--;)
                        if (s = Le.exec(t[u]) || [], p = g = s[1], h = (s[2] || "").split(".").sort(), p) {
                            for (d = oe.event.special[p] || {}, p = (r ? d.delegateType : d.bindType) || p, f = c[p] || [], s = s[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"), l = o = f.length; o--;) a = f[o], !i && g !== a.origType || n && n.guid !== a.guid || s && !s.test(a.namespace) || r && r !== a.selector && ("**" !== r || !a.selector) || (f.splice(o, 1), a.selector && f.delegateCount--, d.remove && d.remove.call(e, a));
                            l && !f.length && (d.teardown && d.teardown.call(e, h, m.handle) !== !1 || oe.removeEvent(e, p, m.handle), delete c[p])
                        } else
                            for (p in c) oe.event.remove(e, p + t[u], n, r, !0);
                    oe.isEmptyObject(c) && (delete m.handle, oe._removeData(e, "events"))
                }
            },
            trigger: function(t, n, r, i) {
                var o, a, s, l, u, c, d, f = [r || fe],
                    p = te.call(t, "type") ? t.type : t,
                    h = te.call(t, "namespace") ? t.namespace.split(".") : [];
                if (s = c = r = r || fe, 3 !== r.nodeType && 8 !== r.nodeType && !He.test(p + oe.event.triggered) && (p.indexOf(".") >= 0 && (h = p.split("."), p = h.shift(), h.sort()), a = p.indexOf(":") < 0 && "on" + p, t = t[oe.expando] ? t : new oe.Event(p, "object" == typeof t && t), t.isTrigger = i ? 2 : 3, t.namespace = h.join("."), t.namespace_re = t.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null, t.result = void 0, t.target || (t.target = r), n = null == n ? [t] : oe.makeArray(n, [t]), u = oe.event.special[p] || {}, i || !u.trigger || u.trigger.apply(r, n) !== !1)) {
                    if (!i && !u.noBubble && !oe.isWindow(r)) {
                        for (l = u.delegateType || p, He.test(l + p) || (s = s.parentNode); s; s = s.parentNode) f.push(s), c = s;
                        c === (r.ownerDocument || fe) && f.push(c.defaultView || c.parentWindow || e)
                    }
                    for (d = 0;
                        (s = f[d++]) && !t.isPropagationStopped();) t.type = d > 1 ? l : u.bindType || p, o = (oe._data(s, "events") || {})[t.type] && oe._data(s, "handle"), o && o.apply(s, n), (o = a && s[a]) && o.apply && oe.acceptData(s) && (t.result = o.apply(s, n), t.result === !1 && t.preventDefault());
                    if (t.type = p, !i && !t.isDefaultPrevented() && (!u._default || u._default.apply(f.pop(), n) === !1) && oe.acceptData(r) && a && r[p] && !oe.isWindow(r)) {
                        c = r[a], c && (r[a] = null), oe.event.triggered = p;
                        try {
                            r[p]()
                        } catch (e) {}
                        oe.event.triggered = void 0, c && (r[a] = c)
                    }
                    return t.result
                }
            },
            dispatch: function(e) {
                e = oe.event.fix(e);
                var t, n, r, i, o, a = [],
                    s = G.call(arguments),
                    l = (oe._data(this, "events") || {})[e.type] || [],
                    u = oe.event.special[e.type] || {};
                if (s[0] = e, e.delegateTarget = this, !u.preDispatch || u.preDispatch.call(this, e) !== !1) {
                    for (a = oe.event.handlers.call(this, e, l), t = 0;
                        (i = a[t++]) && !e.isPropagationStopped();)
                        for (e.currentTarget = i.elem, o = 0;
                            (r = i.handlers[o++]) && !e.isImmediatePropagationStopped();)(!e.namespace_re || e.namespace_re.test(r.namespace)) && (e.handleObj = r, e.data = r.data, void 0 !== (n = ((oe.event.special[r.origType] || {}).handle || r.handler).apply(i.elem, s)) && (e.result = n) === !1 && (e.preventDefault(), e.stopPropagation()));
                    return u.postDispatch && u.postDispatch.call(this, e), e.result
                }
            },
            handlers: function(e, t) {
                var n, r, i, o, a = [],
                    s = t.delegateCount,
                    l = e.target;
                if (s && l.nodeType && (!e.button || "click" !== e.type))
                    for (; l != this; l = l.parentNode || this)
                        if (1 === l.nodeType && (l.disabled !== !0 || "click" !== e.type)) {
                            for (i = [], o = 0; s > o; o++) r = t[o], n = r.selector + " ", void 0 === i[n] && (i[n] = r.needsContext ? oe(n, this).index(l) >= 0 : oe.find(n, this, null, [l]).length), i[n] && i.push(r);
                            i.length && a.push({
                                elem: l,
                                handlers: i
                            })
                        }
                return s < t.length && a.push({
                    elem: this,
                    handlers: t.slice(s)
                }), a
            },
            fix: function(e) {
                if (e[oe.expando]) return e;
                var t, n, r, i = e.type,
                    o = e,
                    a = this.fixHooks[i];
                for (a || (this.fixHooks[i] = a = Pe.test(i) ? this.mouseHooks : Ae.test(i) ? this.keyHooks : {}), r = a.props ? this.props.concat(a.props) : this.props, e = new oe.Event(o), t = r.length; t--;) n = r[t], e[n] = o[n];
                return e.target || (e.target = o.srcElement || fe), 3 === e.target.nodeType && (e.target = e.target.parentNode), e.metaKey = !!e.metaKey, a.filter ? a.filter(e, o) : e
            },
            props: "altKey bubbles cancelable ctrlKey currentTarget eventPhase metaKey relatedTarget shiftKey target timeStamp view which".split(" "),
            fixHooks: {},
            keyHooks: {
                props: "char charCode key keyCode".split(" "),
                filter: function(e, t) {
                    return null == e.which && (e.which = null != t.charCode ? t.charCode : t.keyCode), e
                }
            },
            mouseHooks: {
                props: "button buttons clientX clientY fromElement offsetX offsetY pageX pageY screenX screenY toElement".split(" "),
                filter: function(e, t) {
                    var n, r, i, o = t.button,
                        a = t.fromElement;
                    return null == e.pageX && null != t.clientX && (r = e.target.ownerDocument || fe, i = r.documentElement, n = r.body, e.pageX = t.clientX + (i && i.scrollLeft || n && n.scrollLeft || 0) - (i && i.clientLeft || n && n.clientLeft || 0), e.pageY = t.clientY + (i && i.scrollTop || n && n.scrollTop || 0) - (i && i.clientTop || n && n.clientTop || 0)), !e.relatedTarget && a && (e.relatedTarget = a === e.target ? t.toElement : a), e.which || void 0 === o || (e.which = 1 & o ? 1 : 2 & o ? 3 : 4 & o ? 2 : 0), e
                }
            },
            special: {
                load: {
                    noBubble: !0
                },
                focus: {
                    trigger: function() {
                        if (this !== h() && this.focus) try {
                            return this.focus(), !1
                        } catch (e) {}
                    },
                    delegateType: "focusin"
                },
                blur: {
                    trigger: function() {
                        return this === h() && this.blur ? (this.blur(), !1) : void 0
                    },
                    delegateType: "focusout"
                },
                click: {
                    trigger: function() {
                        return oe.nodeName(this, "input") && "checkbox" === this.type && this.click ? (this.click(), !1) : void 0
                    },
                    _default: function(e) {
                        return oe.nodeName(e.target, "a")
                    }
                },
                beforeunload: {
                    postDispatch: function(e) {
                        void 0 !== e.result && (e.originalEvent.returnValue = e.result)
                    }
                }
            },
            simulate: function(e, t, n, r) {
                var i = oe.extend(new oe.Event, n, {
                    type: e,
                    isSimulated: !0,
                    originalEvent: {}
                });
                r ? oe.event.trigger(i, null, t) : oe.event.dispatch.call(t, i), i.isDefaultPrevented() && n.preventDefault()
            }
        }, oe.removeEvent = fe.removeEventListener ? function(e, t, n) {
            e.removeEventListener && e.removeEventListener(t, n, !1)
        } : function(e, t, n) {
            var r = "on" + t;
            e.detachEvent && (typeof e[r] === xe && (e[r] = null), e.detachEvent(r, n))
        }, oe.Event = function(e, t) {
            return this instanceof oe.Event ? (e && e.type ? (this.originalEvent = e, this.type = e.type, this.isDefaultPrevented = e.defaultPrevented || void 0 === e.defaultPrevented && (e.returnValue === !1 || e.getPreventDefault && e.getPreventDefault()) ? f : p) : this.type = e, t && oe.extend(this, t), this.timeStamp = e && e.timeStamp || oe.now(), void(this[oe.expando] = !0)) : new oe.Event(e, t)
        }, oe.Event.prototype = {
            isDefaultPrevented: p,
            isPropagationStopped: p,
            isImmediatePropagationStopped: p,
            preventDefault: function() {
                var e = this.originalEvent;
                this.isDefaultPrevented = f, e && (e.preventDefault ? e.preventDefault() : e.returnValue = !1)
            },
            stopPropagation: function() {
                var e = this.originalEvent;
                this.isPropagationStopped = f, e && (e.stopPropagation && e.stopPropagation(), e.cancelBubble = !0)
            },
            stopImmediatePropagation: function() {
                this.isImmediatePropagationStopped = f, this.stopPropagation()
            }
        }, oe.each({
            mouseenter: "mouseover",
            mouseleave: "mouseout"
        }, function(e, t) {
            oe.event.special[e] = {
                delegateType: t,
                bindType: t,
                handle: function(e) {
                    var n, r = this,
                        i = e.relatedTarget,
                        o = e.handleObj;
                    return (!i || i !== r && !oe.contains(r, i)) && (e.type = o.origType, n = o.handler.apply(this, arguments), e.type = t), n
                }
            }
        }), re.submitBubbles || (oe.event.special.submit = {
            setup: function() {
                return !oe.nodeName(this, "form") && void oe.event.add(this, "click._submit keypress._submit", function(e) {
                    var t = e.target,
                        n = oe.nodeName(t, "input") || oe.nodeName(t, "button") ? t.form : void 0;
                    n && !oe._data(n, "submitBubbles") && (oe.event.add(n, "submit._submit", function(e) {
                        e._submit_bubble = !0
                    }), oe._data(n, "submitBubbles", !0))
                })
            },
            postDispatch: function(e) {
                e._submit_bubble && (delete e._submit_bubble, this.parentNode && !e.isTrigger && oe.event.simulate("submit", this.parentNode, e, !0))
            },
            teardown: function() {
                return !oe.nodeName(this, "form") && void oe.event.remove(this, "._submit")
            }
        }), re.changeBubbles || (oe.event.special.change = {
            setup: function() {
                return Ne.test(this.nodeName) ? (("checkbox" === this.type || "radio" === this.type) && (oe.event.add(this, "propertychange._change", function(e) {
                    "checked" === e.originalEvent.propertyName && (this._just_changed = !0)
                }), oe.event.add(this, "click._change", function(e) {
                    this._just_changed && !e.isTrigger && (this._just_changed = !1), oe.event.simulate("change", this, e, !0)
                })), !1) : void oe.event.add(this, "beforeactivate._change", function(e) {
                    var t = e.target;
                    Ne.test(t.nodeName) && !oe._data(t, "changeBubbles") && (oe.event.add(t, "change._change", function(e) {
                        !this.parentNode || e.isSimulated || e.isTrigger || oe.event.simulate("change", this.parentNode, e, !0)
                    }), oe._data(t, "changeBubbles", !0))
                })
            },
            handle: function(e) {
                var t = e.target;
                return this !== t || e.isSimulated || e.isTrigger || "radio" !== t.type && "checkbox" !== t.type ? e.handleObj.handler.apply(this, arguments) : void 0
            },
            teardown: function() {
                return oe.event.remove(this, "._change"), !Ne.test(this.nodeName)
            }
        }), re.focusinBubbles || oe.each({
            focus: "focusin",
            blur: "focusout"
        }, function(e, t) {
            var n = function(e) {
                oe.event.simulate(t, e.target, oe.event.fix(e), !0)
            };
            oe.event.special[t] = {
                setup: function() {
                    var r = this.ownerDocument || this,
                        i = oe._data(r, t);
                    i || r.addEventListener(e, n, !0), oe._data(r, t, (i || 0) + 1)
                },
                teardown: function() {
                    var r = this.ownerDocument || this,
                        i = oe._data(r, t) - 1;
                    i ? oe._data(r, t, i) : (r.removeEventListener(e, n, !0), oe._removeData(r, t))
                }
            }
        }), oe.fn.extend({
            on: function(e, t, n, r, i) {
                var o, a;
                if ("object" == typeof e) {
                    "string" != typeof t && (n = n || t, t = void 0);
                    for (o in e) this.on(o, t, n, e[o], i);
                    return this
                }
                if (null == n && null == r ? (r = t, n = t = void 0) : null == r && ("string" == typeof t ? (r = n, n = void 0) : (r = n, n = t, t = void 0)), r === !1) r = p;
                else if (!r) return this;
                return 1 === i && (a = r, r = function(e) {
                    return oe().off(e), a.apply(this, arguments)
                }, r.guid = a.guid || (a.guid = oe.guid++)), this.each(function() {
                    oe.event.add(this, e, r, n, t)
                })
            },
            one: function(e, t, n, r) {
                return this.on(e, t, n, r, 1)
            },
            off: function(e, t, n) {
                var r, i;
                if (e && e.preventDefault && e.handleObj) return r = e.handleObj, oe(e.delegateTarget).off(r.namespace ? r.origType + "." + r.namespace : r.origType, r.selector, r.handler), this;
                if ("object" == typeof e) {
                    for (i in e) this.off(i, t, e[i]);
                    return this
                }
                return (t === !1 || "function" == typeof t) && (n = t, t = void 0), n === !1 && (n = p), this.each(function() {
                    oe.event.remove(this, e, n, t)
                })
            },
            trigger: function(e, t) {
                return this.each(function() {
                    oe.event.trigger(e, t, this)
                })
            },
            triggerHandler: function(e, t) {
                var n = this[0];
                return n ? oe.event.trigger(e, t, n, !0) : void 0
            }
        });
        var De = "abbr|article|aside|audio|bdi|canvas|data|datalist|details|figcaption|figure|footer|header|hgroup|mark|meter|nav|output|progress|section|summary|time|video",
            Me = new RegExp("<(?:" + De + ")[\\s/>]", "i"),
            Oe = /^\s+/,
            Fe = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([\w:]+)[^>]*)\/>/gi,
            Be = /<([\w:]+)/,
            $e = /<tbody/i,
            Ve = /<|&#?\w+;/,
            qe = /<(?:script|style|link)/i,
            _e = /checked\s*(?:[^=]|=\s*.checked.)/i,
            Re = /^$|\/(?:java|ecma)script/i,
            ze = /^true\/(.*)/,
            Ie = {
                option: [1, "<select multiple='multiple'>", "</select>"],
                legend: [1, "<fieldset>", "</fieldset>"],
                area: [1, "<map>", "</map>"],
                param: [1, "<object>", "</object>"],
                thead: [1, "<table>", "</table>"],
                tr: [2, "<table><tbody>", "</tbody></table>"],
                col: [2, "<table><tbody></tbody><colgroup>", "</colgroup></table>"],
                td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
                _default: re.htmlSerialize ? [0, "", ""] : [1, "X<div>", "</div>"]
            },
            We = g(fe),
            Xe = We.appendChild(fe.createElement("div"));
        Ie.optgroup = Ie.option, Ie.tbody = Ie.tfoot = Ie.colgroup = Ie.caption = Ie.thead, Ie.th = Ie.td, oe.extend({
            clone: function(e, t, n) {
                var r, i, o, a, s, l = oe.contains(e.ownerDocument, e);
                if (re.html5Clone || oe.isXMLDoc(e) || !Me.test("<" + e.nodeName + ">") ? o = e.cloneNode(!0) : (Xe.innerHTML = e.outerHTML, Xe.removeChild(o = Xe.firstChild)), !(re.noCloneEvent && re.noCloneChecked || 1 !== e.nodeType && 11 !== e.nodeType || oe.isXMLDoc(e)))
                    for (r = m(o), s = m(e), a = 0; null != (i = s[a]); ++a) r[a] && T(i, r[a]);
                if (t)
                    if (n)
                        for (s = s || m(e), r = r || m(o), a = 0; null != (i = s[a]); a++) C(i, r[a]);
                    else C(e, o);
                return r = m(o, "script"), r.length > 0 && w(r, !l && m(e, "script")), r = s = i = null, o
            },
            buildFragment: function(e, t, n, r) {
                for (var i, o, a, s, l, u, c, d = e.length, f = g(t), p = [], h = 0; d > h; h++)
                    if ((o = e[h]) || 0 === o)
                        if ("object" === oe.type(o)) oe.merge(p, o.nodeType ? [o] : o);
                        else if (Ve.test(o)) {
                    for (s = s || f.appendChild(t.createElement("div")), l = (Be.exec(o) || ["", ""])[1].toLowerCase(), c = Ie[l] || Ie._default, s.innerHTML = c[1] + o.replace(Fe, "<$1></$2>") + c[2], i = c[0]; i--;) s = s.lastChild;
                    if (!re.leadingWhitespace && Oe.test(o) && p.push(t.createTextNode(Oe.exec(o)[0])), !re.tbody)
                        for (o = "table" !== l || $e.test(o) ? "<table>" !== c[1] || $e.test(o) ? 0 : s : s.firstChild, i = o && o.childNodes.length; i--;) oe.nodeName(u = o.childNodes[i], "tbody") && !u.childNodes.length && o.removeChild(u);
                    for (oe.merge(p, s.childNodes), s.textContent = ""; s.firstChild;) s.removeChild(s.firstChild);
                    s = f.lastChild
                } else p.push(t.createTextNode(o));
                for (s && f.removeChild(s), re.appendChecked || oe.grep(m(p, "input"), v), h = 0; o = p[h++];)
                    if ((!r || -1 === oe.inArray(o, r)) && (a = oe.contains(o.ownerDocument, o), s = m(f.appendChild(o), "script"), a && w(s), n))
                        for (i = 0; o = s[i++];) Re.test(o.type || "") && n.push(o);
                return s = null, f
            },
            cleanData: function(e, t) {
                for (var n, r, i, o, a = 0, s = oe.expando, l = oe.cache, u = re.deleteExpando, c = oe.event.special; null != (n = e[a]); a++)
                    if ((t || oe.acceptData(n)) && (i = n[s], o = i && l[i])) {
                        if (o.events)
                            for (r in o.events) c[r] ? oe.event.remove(n, r) : oe.removeEvent(n, r, o.handle);
                        l[i] && (delete l[i], u ? delete n[s] : typeof n.removeAttribute !== xe ? n.removeAttribute(s) : n[s] = null, Q.push(i))
                    }
            }
        }), oe.fn.extend({
            text: function(e) {
                return je(this, function(e) {
                    return void 0 === e ? oe.text(this) : this.empty().append((this[0] && this[0].ownerDocument || fe).createTextNode(e))
                }, null, e, arguments.length)
            },
            append: function() {
                return this.domManip(arguments, function(e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        y(this, e).appendChild(e)
                    }
                })
            },
            prepend: function() {
                return this.domManip(arguments, function(e) {
                    if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                        var t = y(this, e);
                        t.insertBefore(e, t.firstChild)
                    }
                })
            },
            before: function() {
                return this.domManip(arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this)
                })
            },
            after: function() {
                return this.domManip(arguments, function(e) {
                    this.parentNode && this.parentNode.insertBefore(e, this.nextSibling)
                })
            },
            remove: function(e, t) {
                for (var n, r = e ? oe.filter(e, this) : this, i = 0; null != (n = r[i]); i++) t || 1 !== n.nodeType || oe.cleanData(m(n)), n.parentNode && (t && oe.contains(n.ownerDocument, n) && w(m(n, "script")), n.parentNode.removeChild(n));
                return this
            },
            empty: function() {
                for (var e, t = 0; null != (e = this[t]); t++) {
                    for (1 === e.nodeType && oe.cleanData(m(e, !1)); e.firstChild;) e.removeChild(e.firstChild);
                    e.options && oe.nodeName(e, "select") && (e.options.length = 0)
                }
                return this
            },
            clone: function(e, t) {
                return e = null != e && e, t = null == t ? e : t, this.map(function() {
                    return oe.clone(this, e, t)
                })
            },
            html: function(e) {
                return je(this, function(e) {
                    var t = this[0] || {},
                        n = 0,
                        r = this.length;
                    if (void 0 === e) return 1 === t.nodeType ? t.innerHTML.replace(/ jQuery\d+="(?:null|\d+)"/g, "") : void 0;
                    if (!("string" != typeof e || qe.test(e) || !re.htmlSerialize && Me.test(e) || !re.leadingWhitespace && Oe.test(e) || Ie[(Be.exec(e) || ["", ""])[1].toLowerCase()])) {
                        e = e.replace(Fe, "<$1></$2>");
                        try {
                            for (; r > n; n++) t = this[n] || {}, 1 === t.nodeType && (oe.cleanData(m(t, !1)), t.innerHTML = e);
                            t = 0
                        } catch (e) {}
                    }
                    t && this.empty().append(e)
                }, null, e, arguments.length)
            },
            replaceWith: function() {
                var e = arguments[0];
                return this.domManip(arguments, function(t) {
                    e = this.parentNode, oe.cleanData(m(this)), e && e.replaceChild(t, this)
                }), e && (e.length || e.nodeType) ? this : this.remove()
            },
            detach: function(e) {
                return this.remove(e, !0)
            },
            domManip: function(e, t) {
                e = U.apply([], e);
                var n, r, i, o, a, s, l = 0,
                    u = this.length,
                    c = this,
                    d = u - 1,
                    f = e[0],
                    p = oe.isFunction(f);
                if (p || u > 1 && "string" == typeof f && !re.checkClone && _e.test(f)) return this.each(function(n) {
                    var r = c.eq(n);
                    p && (e[0] = f.call(this, n, r.html())), r.domManip(e, t)
                });
                if (u && (s = oe.buildFragment(e, this[0].ownerDocument, !1, this), n = s.firstChild, 1 === s.childNodes.length && (s = n), n)) {
                    for (o = oe.map(m(s, "script"), b), i = o.length; u > l; l++) r = s, l !== d && (r = oe.clone(r, !0, !0), i && oe.merge(o, m(r, "script"))), t.call(this[l], r, l);
                    if (i)
                        for (a = o[o.length - 1].ownerDocument, oe.map(o, x), l = 0; i > l; l++) r = o[l], Re.test(r.type || "") && !oe._data(r, "globalEval") && oe.contains(a, r) && (r.src ? oe._evalUrl && oe._evalUrl(r.src) : oe.globalEval((r.text || r.textContent || r.innerHTML || "").replace(/^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g, "")));
                    s = n = null
                }
                return this
            }
        }), oe.each({
            appendTo: "append",
            prependTo: "prepend",
            insertBefore: "before",
            insertAfter: "after",
            replaceAll: "replaceWith"
        }, function(e, t) {
            oe.fn[e] = function(e) {
                for (var n, r = 0, i = [], o = oe(e), a = o.length - 1; a >= r; r++) n = r === a ? this : this.clone(!0), oe(o[r])[t](n), J.apply(i, n.get());
                return this.pushStack(i)
            }
        });
        var Ye, Qe = {};
        ! function() {
            var e, t, n = fe.createElement("div");
            n.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", e = n.getElementsByTagName("a")[0], e.style.cssText = "float:left;opacity:.5", re.opacity = /^0.5/.test(e.style.opacity), re.cssFloat = !!e.style.cssFloat, n.style.backgroundClip = "content-box", n.cloneNode(!0).style.backgroundClip = "", re.clearCloneStyle = "content-box" === n.style.backgroundClip, e = n = null, re.shrinkWrapBlocks = function() {
                var e, n, r;
                if (null == t) {
                    if (!(e = fe.getElementsByTagName("body")[0])) return;
                    "border:0;width:0;height:0;position:absolute;top:0;left:-9999px", n = fe.createElement("div"), r = fe.createElement("div"), e.appendChild(n).appendChild(r), t = !1, typeof r.style.zoom !== xe && (r.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0;width:1px;padding:1px;zoom:1", r.innerHTML = "<div></div>", r.firstChild.style.width = "5px", t = 3 !== r.offsetWidth), e.removeChild(n), e = n = r = null
                }
                return t
            }
        }();
        var Ge, Ue, Je = /^margin/,
            Ze = new RegExp("^(" + Te + ")(?!px)[a-z%]+$", "i"),
            Ke = /^(top|right|bottom|left)$/;
        e.getComputedStyle ? (Ge = function(e) {
            return e.ownerDocument.defaultView.getComputedStyle(e, null)
        }, Ue = function(e, t, n) {
            var r, i, o, a, s = e.style;
            return n = n || Ge(e), a = n ? n.getPropertyValue(t) || n[t] : void 0, n && ("" !== a || oe.contains(e.ownerDocument, e) || (a = oe.style(e, t)), Ze.test(a) && Je.test(t) && (r = s.width, i = s.minWidth, o = s.maxWidth,
                s.minWidth = s.maxWidth = s.width = a, a = n.width, s.width = r, s.minWidth = i, s.maxWidth = o)), void 0 === a ? a : a + ""
        }) : fe.documentElement.currentStyle && (Ge = function(e) {
            return e.currentStyle
        }, Ue = function(e, t, n) {
            var r, i, o, a, s = e.style;
            return n = n || Ge(e), a = n ? n[t] : void 0, null == a && s && s[t] && (a = s[t]), Ze.test(a) && !Ke.test(t) && (r = s.left, i = e.runtimeStyle, o = i && i.left, o && (i.left = e.currentStyle.left), s.left = "fontSize" === t ? "1em" : a, a = s.pixelLeft + "px", s.left = r, o && (i.left = o)), void 0 === a ? a : a + "" || "auto"
        }), ! function() {
            function t() {
                var t, n, r = fe.getElementsByTagName("body")[0];
                r && (t = fe.createElement("div"), n = fe.createElement("div"), t.style.cssText = u, r.appendChild(t).appendChild(n), n.style.cssText = "-webkit-box-sizing:border-box;-moz-box-sizing:border-box;box-sizing:border-box;position:absolute;display:block;padding:1px;border:1px;width:4px;margin-top:1%;top:1%", oe.swap(r, null != r.style.zoom ? {
                    zoom: 1
                } : {}, function() {
                    i = 4 === n.offsetWidth
                }), o = !0, a = !1, s = !0, e.getComputedStyle && (a = "1%" !== (e.getComputedStyle(n, null) || {}).top, o = "4px" === (e.getComputedStyle(n, null) || {
                    width: "4px"
                }).width), r.removeChild(t), n = r = null)
            }
            var n, r, i, o, a, s, l = fe.createElement("div"),
                u = "border:0;width:0;height:0;position:absolute;top:0;left:-9999px";
            l.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", n = l.getElementsByTagName("a")[0], n.style.cssText = "float:left;opacity:.5", re.opacity = /^0.5/.test(n.style.opacity), re.cssFloat = !!n.style.cssFloat, l.style.backgroundClip = "content-box", l.cloneNode(!0).style.backgroundClip = "", re.clearCloneStyle = "content-box" === l.style.backgroundClip, n = l = null, oe.extend(re, {
                reliableHiddenOffsets: function() {
                    if (null != r) return r;
                    var e, t, n, i = fe.createElement("div"),
                        o = fe.getElementsByTagName("body")[0];
                    return o ? (i.setAttribute("className", "t"), i.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", e = fe.createElement("div"), e.style.cssText = u, o.appendChild(e).appendChild(i), i.innerHTML = "<table><tr><td></td><td>t</td></tr></table>", t = i.getElementsByTagName("td"), t[0].style.cssText = "padding:0;margin:0;border:0;display:none", n = 0 === t[0].offsetHeight, t[0].style.display = "", t[1].style.display = "none", r = n && 0 === t[0].offsetHeight, o.removeChild(e), i = o = null, r) : void 0
                },
                boxSizing: function() {
                    return null == i && t(), i
                },
                boxSizingReliable: function() {
                    return null == o && t(), o
                },
                pixelPosition: function() {
                    return null == a && t(), a
                },
                reliableMarginRight: function() {
                    var t, n, r, i;
                    if (null == s && e.getComputedStyle) {
                        if (!(t = fe.getElementsByTagName("body")[0])) return;
                        n = fe.createElement("div"), r = fe.createElement("div"), n.style.cssText = u, t.appendChild(n).appendChild(r), i = r.appendChild(fe.createElement("div")), i.style.cssText = r.style.cssText = "-webkit-box-sizing:content-box;-moz-box-sizing:content-box;box-sizing:content-box;display:block;padding:0;margin:0;border:0", i.style.marginRight = i.style.width = "0", r.style.width = "1px", s = !parseFloat((e.getComputedStyle(i, null) || {}).marginRight), t.removeChild(n)
                    }
                    return s
                }
            })
        }(), oe.swap = function(e, t, n, r) {
            var i, o, a = {};
            for (o in t) a[o] = e.style[o], e.style[o] = t[o];
            i = n.apply(e, r || []);
            for (o in t) e.style[o] = a[o];
            return i
        };
        var et = /alpha\([^)]*\)/i,
            tt = /opacity\s*=\s*([^)]*)/,
            nt = /^(none|table(?!-c[ea]).+)/,
            rt = new RegExp("^(" + Te + ")(.*)$", "i"),
            it = new RegExp("^([+-])=(" + Te + ")", "i"),
            ot = {
                position: "absolute",
                visibility: "hidden",
                display: "block"
            },
            at = {
                letterSpacing: 0,
                fontWeight: 400
            },
            st = ["Webkit", "O", "Moz", "ms"];
        oe.extend({
            cssHooks: {
                opacity: {
                    get: function(e, t) {
                        if (t) {
                            var n = Ue(e, "opacity");
                            return "" === n ? "1" : n
                        }
                    }
                }
            },
            cssNumber: {
                columnCount: !0,
                fillOpacity: !0,
                fontWeight: !0,
                lineHeight: !0,
                opacity: !0,
                order: !0,
                orphans: !0,
                widows: !0,
                zIndex: !0,
                zoom: !0
            },
            cssProps: {
                float: re.cssFloat ? "cssFloat" : "styleFloat"
            },
            style: function(e, t, n, r) {
                if (e && 3 !== e.nodeType && 8 !== e.nodeType && e.style) {
                    var i, o, a, s = oe.camelCase(t),
                        l = e.style;
                    if (t = oe.cssProps[s] || (oe.cssProps[s] = E(l, s)), a = oe.cssHooks[t] || oe.cssHooks[s], void 0 === n) return a && "get" in a && void 0 !== (i = a.get(e, !1, r)) ? i : l[t];
                    if (o = typeof n, "string" === o && (i = it.exec(n)) && (n = (i[1] + 1) * i[2] + parseFloat(oe.css(e, t)), o = "number"), null != n && n === n && ("number" !== o || oe.cssNumber[s] || (n += "px"), re.clearCloneStyle || "" !== n || 0 !== t.indexOf("background") || (l[t] = "inherit"), !(a && "set" in a && void 0 === (n = a.set(e, n, r))))) try {
                        l[t] = "", l[t] = n
                    } catch (e) {}
                }
            },
            css: function(e, t, n, r) {
                var i, o, a, s = oe.camelCase(t);
                return t = oe.cssProps[s] || (oe.cssProps[s] = E(e.style, s)), a = oe.cssHooks[t] || oe.cssHooks[s], a && "get" in a && (o = a.get(e, !0, n)), void 0 === o && (o = Ue(e, t, r)), "normal" === o && t in at && (o = at[t]), "" === n || n ? (i = parseFloat(o), n === !0 || oe.isNumeric(i) ? i || 0 : o) : o
            }
        }), oe.each(["height", "width"], function(e, t) {
            oe.cssHooks[t] = {
                get: function(e, n, r) {
                    return n ? 0 === e.offsetWidth && nt.test(oe.css(e, "display")) ? oe.swap(e, ot, function() {
                        return H(e, t, r)
                    }) : H(e, t, r) : void 0
                },
                set: function(e, n, r) {
                    var i = r && Ge(e);
                    return A(e, n, r ? P(e, t, r, re.boxSizing() && "border-box" === oe.css(e, "boxSizing", !1, i), i) : 0)
                }
            }
        }), re.opacity || (oe.cssHooks.opacity = {
            get: function(e, t) {
                return tt.test((t && e.currentStyle ? e.currentStyle.filter : e.style.filter) || "") ? .01 * parseFloat(RegExp.$1) + "" : t ? "1" : ""
            },
            set: function(e, t) {
                var n = e.style,
                    r = e.currentStyle,
                    i = oe.isNumeric(t) ? "alpha(opacity=" + 100 * t + ")" : "",
                    o = r && r.filter || n.filter || "";
                n.zoom = 1, (t >= 1 || "" === t) && "" === oe.trim(o.replace(et, "")) && n.removeAttribute && (n.removeAttribute("filter"), "" === t || r && !r.filter) || (n.filter = et.test(o) ? o.replace(et, i) : o + " " + i)
            }
        }), oe.cssHooks.marginRight = j(re.reliableMarginRight, function(e, t) {
            return t ? oe.swap(e, {
                display: "inline-block"
            }, Ue, [e, "marginRight"]) : void 0
        }), oe.each({
            margin: "",
            padding: "",
            border: "Width"
        }, function(e, t) {
            oe.cssHooks[e + t] = {
                expand: function(n) {
                    for (var r = 0, i = {}, o = "string" == typeof n ? n.split(" ") : [n]; 4 > r; r++) i[e + ke[r] + t] = o[r] || o[r - 2] || o[0];
                    return i
                }
            }, Je.test(e) || (oe.cssHooks[e + t].set = A)
        }), oe.fn.extend({
            css: function(e, t) {
                return je(this, function(e, t, n) {
                    var r, i, o = {},
                        a = 0;
                    if (oe.isArray(t)) {
                        for (r = Ge(e), i = t.length; i > a; a++) o[t[a]] = oe.css(e, t[a], !1, r);
                        return o
                    }
                    return void 0 !== n ? oe.style(e, t, n) : oe.css(e, t)
                }, e, t, arguments.length > 1)
            },
            show: function() {
                return N(this, !0)
            },
            hide: function() {
                return N(this)
            },
            toggle: function(e) {
                return "boolean" == typeof e ? e ? this.show() : this.hide() : this.each(function() {
                    Se(this) ? oe(this).show() : oe(this).hide()
                })
            }
        }), oe.Tween = L, L.prototype = {
            constructor: L,
            init: function(e, t, n, r, i, o) {
                this.elem = e, this.prop = n, this.easing = i || "swing", this.options = t, this.start = this.now = this.cur(), this.end = r, this.unit = o || (oe.cssNumber[n] ? "" : "px")
            },
            cur: function() {
                var e = L.propHooks[this.prop];
                return e && e.get ? e.get(this) : L.propHooks._default.get(this)
            },
            run: function(e) {
                var t, n = L.propHooks[this.prop];
                return this.pos = t = this.options.duration ? oe.easing[this.easing](e, this.options.duration * e, 0, 1, this.options.duration) : e, this.now = (this.end - this.start) * t + this.start, this.options.step && this.options.step.call(this.elem, this.now, this), n && n.set ? n.set(this) : L.propHooks._default.set(this), this
            }
        }, L.prototype.init.prototype = L.prototype, L.propHooks = {
            _default: {
                get: function(e) {
                    var t;
                    return null == e.elem[e.prop] || e.elem.style && null != e.elem.style[e.prop] ? (t = oe.css(e.elem, e.prop, ""), t && "auto" !== t ? t : 0) : e.elem[e.prop]
                },
                set: function(e) {
                    oe.fx.step[e.prop] ? oe.fx.step[e.prop](e) : e.elem.style && (null != e.elem.style[oe.cssProps[e.prop]] || oe.cssHooks[e.prop]) ? oe.style(e.elem, e.prop, e.now + e.unit) : e.elem[e.prop] = e.now
                }
            }
        }, L.propHooks.scrollTop = L.propHooks.scrollLeft = {
            set: function(e) {
                e.elem.nodeType && e.elem.parentNode && (e.elem[e.prop] = e.now)
            }
        }, oe.easing = {
            linear: function(e) {
                return e
            },
            swing: function(e) {
                return .5 - Math.cos(e * Math.PI) / 2
            }
        }, oe.fx = L.prototype.init, oe.fx.step = {};
        var lt, ut, ct = /^(?:toggle|show|hide)$/,
            dt = new RegExp("^(?:([+-])=|)(" + Te + ")([a-z%]*)$", "i"),
            ft = /queueHooks$/,
            pt = [F],
            ht = {
                "*": [function(e, t) {
                    var n = this.createTween(e, t),
                        r = n.cur(),
                        i = dt.exec(t),
                        o = i && i[3] || (oe.cssNumber[e] ? "" : "px"),
                        a = (oe.cssNumber[e] || "px" !== o && +r) && dt.exec(oe.css(n.elem, e)),
                        s = 1,
                        l = 20;
                    if (a && a[3] !== o) {
                        o = o || a[3], i = i || [], a = +r || 1;
                        do {
                            s = s || ".5", a /= s, oe.style(n.elem, e, a + o)
                        } while (s !== (s = n.cur() / r) && 1 !== s && --l)
                    }
                    return i && (a = n.start = +a || +r || 0, n.unit = o, n.end = i[1] ? a + (i[1] + 1) * i[2] : +i[2]), n
                }]
            };
        oe.Animation = oe.extend($, {
                tweener: function(e, t) {
                    oe.isFunction(e) ? (t = e, e = ["*"]) : e = e.split(" ");
                    for (var n, r = 0, i = e.length; i > r; r++) n = e[r], ht[n] = ht[n] || [], ht[n].unshift(t)
                },
                prefilter: function(e, t) {
                    t ? pt.unshift(e) : pt.push(e)
                }
            }), oe.speed = function(e, t, n) {
                var r = e && "object" == typeof e ? oe.extend({}, e) : {
                    complete: n || !n && t || oe.isFunction(e) && e,
                    duration: e,
                    easing: n && t || t && !oe.isFunction(t) && t
                };
                return r.duration = oe.fx.off ? 0 : "number" == typeof r.duration ? r.duration : r.duration in oe.fx.speeds ? oe.fx.speeds[r.duration] : oe.fx.speeds._default, (null == r.queue || r.queue === !0) && (r.queue = "fx"), r.old = r.complete, r.complete = function() {
                    oe.isFunction(r.old) && r.old.call(this), r.queue && oe.dequeue(this, r.queue)
                }, r
            }, oe.fn.extend({
                fadeTo: function(e, t, n, r) {
                    return this.filter(Se).css("opacity", 0).show().end().animate({
                        opacity: t
                    }, e, n, r)
                },
                animate: function(e, t, n, r) {
                    var i = oe.isEmptyObject(e),
                        o = oe.speed(t, n, r),
                        a = function() {
                            var t = $(this, oe.extend({}, e), o);
                            (i || oe._data(this, "finish")) && t.stop(!0)
                        };
                    return a.finish = a, i || o.queue === !1 ? this.each(a) : this.queue(o.queue, a)
                },
                stop: function(e, t, n) {
                    var r = function(e) {
                        var t = e.stop;
                        delete e.stop, t(n)
                    };
                    return "string" != typeof e && (n = t, t = e, e = void 0), t && e !== !1 && this.queue(e || "fx", []), this.each(function() {
                        var t = !0,
                            i = null != e && e + "queueHooks",
                            o = oe.timers,
                            a = oe._data(this);
                        if (i) a[i] && a[i].stop && r(a[i]);
                        else
                            for (i in a) a[i] && a[i].stop && ft.test(i) && r(a[i]);
                        for (i = o.length; i--;) o[i].elem !== this || null != e && o[i].queue !== e || (o[i].anim.stop(n), t = !1, o.splice(i, 1));
                        (t || !n) && oe.dequeue(this, e)
                    })
                },
                finish: function(e) {
                    return e !== !1 && (e = e || "fx"), this.each(function() {
                        var t, n = oe._data(this),
                            r = n[e + "queue"],
                            i = n[e + "queueHooks"],
                            o = oe.timers,
                            a = r ? r.length : 0;
                        for (n.finish = !0, oe.queue(this, e, []), i && i.stop && i.stop.call(this, !0), t = o.length; t--;) o[t].elem === this && o[t].queue === e && (o[t].anim.stop(!0), o.splice(t, 1));
                        for (t = 0; a > t; t++) r[t] && r[t].finish && r[t].finish.call(this);
                        delete n.finish
                    })
                }
            }), oe.each(["toggle", "show", "hide"], function(e, t) {
                var n = oe.fn[t];
                oe.fn[t] = function(e, r, i) {
                    return null == e || "boolean" == typeof e ? n.apply(this, arguments) : this.animate(M(t, !0), e, r, i)
                }
            }), oe.each({
                slideDown: M("show"),
                slideUp: M("hide"),
                slideToggle: M("toggle"),
                fadeIn: {
                    opacity: "show"
                },
                fadeOut: {
                    opacity: "hide"
                },
                fadeToggle: {
                    opacity: "toggle"
                }
            }, function(e, t) {
                oe.fn[e] = function(e, n, r) {
                    return this.animate(t, e, n, r)
                }
            }), oe.timers = [], oe.fx.tick = function() {
                var e, t = oe.timers,
                    n = 0;
                for (lt = oe.now(); n < t.length; n++)(e = t[n])() || t[n] !== e || t.splice(n--, 1);
                t.length || oe.fx.stop(), lt = void 0
            }, oe.fx.timer = function(e) {
                oe.timers.push(e), e() ? oe.fx.start() : oe.timers.pop()
            }, oe.fx.interval = 13, oe.fx.start = function() {
                ut || (ut = setInterval(oe.fx.tick, oe.fx.interval))
            }, oe.fx.stop = function() {
                clearInterval(ut), ut = null
            }, oe.fx.speeds = {
                slow: 600,
                fast: 200,
                _default: 400
            }, oe.fn.delay = function(e, t) {
                return e = oe.fx ? oe.fx.speeds[e] || e : e, t = t || "fx", this.queue(t, function(t, n) {
                    var r = setTimeout(t, e);
                    n.stop = function() {
                        clearTimeout(r)
                    }
                })
            },
            function() {
                var e, t, n, r, i = fe.createElement("div");
                i.setAttribute("className", "t"), i.innerHTML = "  <link/><table></table><a href='/a'>a</a><input type='checkbox'/>", e = i.getElementsByTagName("a")[0], n = fe.createElement("select"), r = n.appendChild(fe.createElement("option")), t = i.getElementsByTagName("input")[0], e.style.cssText = "top:1px", re.getSetAttribute = "t" !== i.className, re.style = /top/.test(e.getAttribute("style")), re.hrefNormalized = "/a" === e.getAttribute("href"), re.checkOn = !!t.value, re.optSelected = r.selected, re.enctype = !!fe.createElement("form").enctype, n.disabled = !0, re.optDisabled = !r.disabled, t = fe.createElement("input"), t.setAttribute("value", ""), re.input = "" === t.getAttribute("value"), t.value = "t", t.setAttribute("type", "radio"), re.radioValue = "t" === t.value, e = t = n = r = i = null
            }();
        oe.fn.extend({
            val: function(e) {
                var t, n, r, i = this[0];
                return arguments.length ? (r = oe.isFunction(e), this.each(function(n) {
                    var i;
                    1 === this.nodeType && (i = r ? e.call(this, n, oe(this).val()) : e, null == i ? i = "" : "number" == typeof i ? i += "" : oe.isArray(i) && (i = oe.map(i, function(e) {
                        return null == e ? "" : e + ""
                    })), (t = oe.valHooks[this.type] || oe.valHooks[this.nodeName.toLowerCase()]) && "set" in t && void 0 !== t.set(this, i, "value") || (this.value = i))
                })) : i ? (t = oe.valHooks[i.type] || oe.valHooks[i.nodeName.toLowerCase()], t && "get" in t && void 0 !== (n = t.get(i, "value")) ? n : (n = i.value, "string" == typeof n ? n.replace(/\r/g, "") : null == n ? "" : n)) : void 0
            }
        }), oe.extend({
            valHooks: {
                option: {
                    get: function(e) {
                        var t = oe.find.attr(e, "value");
                        return null != t ? t : oe.text(e)
                    }
                },
                select: {
                    get: function(e) {
                        for (var t, n, r = e.options, i = e.selectedIndex, o = "select-one" === e.type || 0 > i, a = o ? null : [], s = o ? i + 1 : r.length, l = 0 > i ? s : o ? i : 0; s > l; l++)
                            if (n = r[l], !(!n.selected && l !== i || (re.optDisabled ? n.disabled : null !== n.getAttribute("disabled")) || n.parentNode.disabled && oe.nodeName(n.parentNode, "optgroup"))) {
                                if (t = oe(n).val(), o) return t;
                                a.push(t)
                            }
                        return a
                    },
                    set: function(e, t) {
                        for (var n, r, i = e.options, o = oe.makeArray(t), a = i.length; a--;)
                            if (r = i[a], oe.inArray(oe.valHooks.option.get(r), o) >= 0) try {
                                r.selected = n = !0
                            } catch (e) {
                                r.scrollHeight
                            } else r.selected = !1;
                        return n || (e.selectedIndex = -1), i
                    }
                }
            }
        }), oe.each(["radio", "checkbox"], function() {
            oe.valHooks[this] = {
                set: function(e, t) {
                    return oe.isArray(t) ? e.checked = oe.inArray(oe(e).val(), t) >= 0 : void 0
                }
            }, re.checkOn || (oe.valHooks[this].get = function(e) {
                return null === e.getAttribute("value") ? "on" : e.value
            })
        });
        var gt, mt, vt = oe.expr.attrHandle,
            yt = /^(?:checked|selected)$/i,
            bt = re.getSetAttribute,
            xt = re.input;
        oe.fn.extend({
            attr: function(e, t) {
                return je(this, oe.attr, e, t, arguments.length > 1)
            },
            removeAttr: function(e) {
                return this.each(function() {
                    oe.removeAttr(this, e)
                })
            }
        }), oe.extend({
            attr: function(e, t, n) {
                var r, i, o = e.nodeType;
                if (e && 3 !== o && 8 !== o && 2 !== o) return typeof e.getAttribute === xe ? oe.prop(e, t, n) : (1 === o && oe.isXMLDoc(e) || (t = t.toLowerCase(), r = oe.attrHooks[t] || (oe.expr.match.bool.test(t) ? mt : gt)), void 0 === n ? r && "get" in r && null !== (i = r.get(e, t)) ? i : (i = oe.find.attr(e, t), null == i ? void 0 : i) : null !== n ? r && "set" in r && void 0 !== (i = r.set(e, n, t)) ? i : (e.setAttribute(t, n + ""), n) : void oe.removeAttr(e, t))
            },
            removeAttr: function(e, t) {
                var n, r, i = 0,
                    o = t && t.match(me);
                if (o && 1 === e.nodeType)
                    for (; n = o[i++];) r = oe.propFix[n] || n, oe.expr.match.bool.test(n) ? xt && bt || !yt.test(n) ? e[r] = !1 : e[oe.camelCase("default-" + n)] = e[r] = !1 : oe.attr(e, n, ""), e.removeAttribute(bt ? n : r)
            },
            attrHooks: {
                type: {
                    set: function(e, t) {
                        if (!re.radioValue && "radio" === t && oe.nodeName(e, "input")) {
                            var n = e.value;
                            return e.setAttribute("type", t), n && (e.value = n), t
                        }
                    }
                }
            }
        }), mt = {
            set: function(e, t, n) {
                return t === !1 ? oe.removeAttr(e, n) : xt && bt || !yt.test(n) ? e.setAttribute(!bt && oe.propFix[n] || n, n) : e[oe.camelCase("default-" + n)] = e[n] = !0, n
            }
        }, oe.each(oe.expr.match.bool.source.match(/\w+/g), function(e, t) {
            var n = vt[t] || oe.find.attr;
            vt[t] = xt && bt || !yt.test(t) ? function(e, t, r) {
                var i, o;
                return r || (o = vt[t], vt[t] = i, i = null != n(e, t, r) ? t.toLowerCase() : null, vt[t] = o), i
            } : function(e, t, n) {
                return n ? void 0 : e[oe.camelCase("default-" + t)] ? t.toLowerCase() : null
            }
        }), xt && bt || (oe.attrHooks.value = {
            set: function(e, t, n) {
                return oe.nodeName(e, "input") ? void(e.defaultValue = t) : gt && gt.set(e, t, n)
            }
        }), bt || (gt = {
            set: function(e, t, n) {
                var r = e.getAttributeNode(n);
                return r || e.setAttributeNode(r = e.ownerDocument.createAttribute(n)), r.value = t += "", "value" === n || t === e.getAttribute(n) ? t : void 0
            }
        }, vt.id = vt.name = vt.coords = function(e, t, n) {
            var r;
            return n ? void 0 : (r = e.getAttributeNode(t)) && "" !== r.value ? r.value : null
        }, oe.valHooks.button = {
            get: function(e, t) {
                var n = e.getAttributeNode(t);
                return n && n.specified ? n.value : void 0
            },
            set: gt.set
        }, oe.attrHooks.contenteditable = {
            set: function(e, t, n) {
                gt.set(e, "" !== t && t, n)
            }
        }, oe.each(["width", "height"], function(e, t) {
            oe.attrHooks[t] = {
                set: function(e, n) {
                    return "" === n ? (e.setAttribute(t, "auto"), n) : void 0
                }
            }
        })), re.style || (oe.attrHooks.style = {
            get: function(e) {
                return e.style.cssText || void 0
            },
            set: function(e, t) {
                return e.style.cssText = t + ""
            }
        });
        var wt = /^(?:input|select|textarea|button|object)$/i,
            Ct = /^(?:a|area)$/i;
        oe.fn.extend({
            prop: function(e, t) {
                return je(this, oe.prop, e, t, arguments.length > 1)
            },
            removeProp: function(e) {
                return e = oe.propFix[e] || e, this.each(function() {
                    try {
                        this[e] = void 0, delete this[e]
                    } catch (e) {}
                })
            }
        }), oe.extend({
            propFix: {
                for: "htmlFor",
                class: "className"
            },
            prop: function(e, t, n) {
                var r, i, o, a = e.nodeType;
                if (e && 3 !== a && 8 !== a && 2 !== a) return o = 1 !== a || !oe.isXMLDoc(e), o && (t = oe.propFix[t] || t, i = oe.propHooks[t]), void 0 !== n ? i && "set" in i && void 0 !== (r = i.set(e, n, t)) ? r : e[t] = n : i && "get" in i && null !== (r = i.get(e, t)) ? r : e[t]
            },
            propHooks: {
                tabIndex: {
                    get: function(e) {
                        var t = oe.find.attr(e, "tabindex");
                        return t ? parseInt(t, 10) : wt.test(e.nodeName) || Ct.test(e.nodeName) && e.href ? 0 : -1
                    }
                }
            }
        }), re.hrefNormalized || oe.each(["href", "src"], function(e, t) {
            oe.propHooks[t] = {
                get: function(e) {
                    return e.getAttribute(t, 4)
                }
            }
        }), re.optSelected || (oe.propHooks.selected = {
            get: function(e) {
                var t = e.parentNode;
                return t && (t.selectedIndex, t.parentNode && t.parentNode.selectedIndex), null
            }
        }), oe.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
            oe.propFix[this.toLowerCase()] = this
        }), re.enctype || (oe.propFix.enctype = "encoding");
        var Tt = /[\t\r\n\f]/g;
        oe.fn.extend({
            addClass: function(e) {
                var t, n, r, i, o, a, s = 0,
                    l = this.length,
                    u = "string" == typeof e && e;
                if (oe.isFunction(e)) return this.each(function(t) {
                    oe(this).addClass(e.call(this, t, this.className))
                });
                if (u)
                    for (t = (e || "").match(me) || []; l > s; s++)
                        if (n = this[s], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Tt, " ") : " ")) {
                            for (o = 0; i = t[o++];) r.indexOf(" " + i + " ") < 0 && (r += i + " ");
                            a = oe.trim(r), n.className !== a && (n.className = a)
                        }
                return this
            },
            removeClass: function(e) {
                var t, n, r, i, o, a, s = 0,
                    l = this.length,
                    u = 0 === arguments.length || "string" == typeof e && e;
                if (oe.isFunction(e)) return this.each(function(t) {
                    oe(this).removeClass(e.call(this, t, this.className))
                });
                if (u)
                    for (t = (e || "").match(me) || []; l > s; s++)
                        if (n = this[s], r = 1 === n.nodeType && (n.className ? (" " + n.className + " ").replace(Tt, " ") : "")) {
                            for (o = 0; i = t[o++];)
                                for (; r.indexOf(" " + i + " ") >= 0;) r = r.replace(" " + i + " ", " ");
                            a = e ? oe.trim(r) : "", n.className !== a && (n.className = a)
                        }
                return this
            },
            toggleClass: function(e, t) {
                var n = typeof e;
                return "boolean" == typeof t && "string" === n ? t ? this.addClass(e) : this.removeClass(e) : this.each(oe.isFunction(e) ? function(n) {
                    oe(this).toggleClass(e.call(this, n, this.className, t), t)
                } : function() {
                    if ("string" === n)
                        for (var t, r = 0, i = oe(this), o = e.match(me) || []; t = o[r++];) i.hasClass(t) ? i.removeClass(t) : i.addClass(t);
                    else(n === xe || "boolean" === n) && (this.className && oe._data(this, "__className__", this.className), this.className = this.className || e === !1 ? "" : oe._data(this, "__className__") || "")
                })
            },
            hasClass: function(e) {
                for (var t = " " + e + " ", n = 0, r = this.length; r > n; n++)
                    if (1 === this[n].nodeType && (" " + this[n].className + " ").replace(Tt, " ").indexOf(t) >= 0) return !0;
                return !1
            }
        }), oe.each("blur focus focusin focusout load resize scroll unload click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup error contextmenu".split(" "), function(e, t) {
            oe.fn[t] = function(e, n) {
                return arguments.length > 0 ? this.on(t, null, e, n) : this.trigger(t)
            }
        }), oe.fn.extend({
            hover: function(e, t) {
                return this.mouseenter(e).mouseleave(t || e)
            },
            bind: function(e, t, n) {
                return this.on(e, null, t, n)
            },
            unbind: function(e, t) {
                return this.off(e, null, t)
            },
            delegate: function(e, t, n, r) {
                return this.on(t, e, n, r)
            },
            undelegate: function(e, t, n) {
                return 1 === arguments.length ? this.off(e, "**") : this.off(t, e || "**", n)
            }
        });
        var kt = oe.now(),
            St = /\?/;
        oe.parseJSON = function(t) {
            if (e.JSON && e.JSON.parse) return e.JSON.parse(t + "");
            var n, r = null,
                i = oe.trim(t + "");
            return i && !oe.trim(i.replace(/(,)|(\[|{)|(}|])|"(?:[^"\\\r\n]|\\["\\\/bfnrt]|\\u[\da-fA-F]{4})*"\s*:?|true|false|null|-?(?!0\d)\d+(?:\.\d+|)(?:[eE][+-]?\d+|)/g, function(e, t, i, o) {
                return n && t && (r = 0), 0 === r ? e : (n = i || t, r += !o - !i, "")
            })) ? Function("return " + i)() : oe.error("Invalid JSON: " + t)
        }, oe.parseXML = function(t) {
            var n, r;
            if (!t || "string" != typeof t) return null;
            try {
                e.DOMParser ? (r = new DOMParser, n = r.parseFromString(t, "text/xml")) : (n = new ActiveXObject("Microsoft.XMLDOM"), n.async = "false", n.loadXML(t))
            } catch (e) {
                n = void 0
            }
            return n && n.documentElement && !n.getElementsByTagName("parsererror").length || oe.error("Invalid XML: " + t), n
        };
        var jt, Et, Nt = /([?&])_=[^&]*/,
            At = /^(.*?):[ \t]*([^\r\n]*)\r?$/gm,
            Pt = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/,
            Ht = /^(?:GET|HEAD)$/,
            Lt = /^([\w.+-]+:)(?:\/\/(?:[^\/?#]*@|)([^\/?#:]*)(?::(\d+)|)|)/,
            Dt = {},
            Mt = {},
            Ot = "*/".concat("*");
        try {
            Et = location.href
        } catch (e) {
            Et = fe.createElement("a"), Et.href = "", Et = Et.href
        }
        jt = Lt.exec(Et.toLowerCase()) || [], oe.extend({
            active: 0,
            lastModified: {},
            etag: {},
            ajaxSettings: {
                url: Et,
                type: "GET",
                isLocal: Pt.test(jt[1]),
                global: !0,
                processData: !0,
                async: !0,
                contentType: "application/x-www-form-urlencoded; charset=UTF-8",
                accepts: {
                    "*": Ot,
                    text: "text/plain",
                    html: "text/html",
                    xml: "application/xml, text/xml",
                    json: "application/json, text/javascript"
                },
                contents: {
                    xml: /xml/,
                    html: /html/,
                    json: /json/
                },
                responseFields: {
                    xml: "responseXML",
                    text: "responseText",
                    json: "responseJSON"
                },
                converters: {
                    "* text": String,
                    "text html": !0,
                    "text json": oe.parseJSON,
                    "text xml": oe.parseXML
                },
                flatOptions: {
                    url: !0,
                    context: !0
                }
            },
            ajaxSetup: function(e, t) {
                return t ? _(_(e, oe.ajaxSettings), t) : _(oe.ajaxSettings, e)
            },
            ajaxPrefilter: V(Dt),
            ajaxTransport: V(Mt),
            ajax: function(e, t) {
                function n(e, t, n, r) {
                    var i, c, v, y, x, C = t;
                    2 !== b && (b = 2, s && clearTimeout(s), u = void 0, a = r || "", w.readyState = e > 0 ? 4 : 0, i = e >= 200 && 300 > e || 304 === e, n && (y = R(d, w, n)), y = z(d, y, w, i), i ? (d.ifModified && (x = w.getResponseHeader("Last-Modified"), x && (oe.lastModified[o] = x), (x = w.getResponseHeader("etag")) && (oe.etag[o] = x)), 204 === e || "HEAD" === d.type ? C = "nocontent" : 304 === e ? C = "notmodified" : (C = y.state, c = y.data, v = y.error, i = !v)) : (v = C, (e || !C) && (C = "error", 0 > e && (e = 0))), w.status = e, w.statusText = (t || C) + "", i ? h.resolveWith(f, [c, C, w]) : h.rejectWith(f, [w, C, v]), w.statusCode(m), m = void 0, l && p.trigger(i ? "ajaxSuccess" : "ajaxError", [w, d, i ? c : v]), g.fireWith(f, [w, C]), l && (p.trigger("ajaxComplete", [w, d]), --oe.active || oe.event.trigger("ajaxStop")))
                }
                "object" == typeof e && (t = e, e = void 0), t = t || {};
                var r, i, o, a, s, l, u, c, d = oe.ajaxSetup({}, t),
                    f = d.context || d,
                    p = d.context && (f.nodeType || f.jquery) ? oe(f) : oe.event,
                    h = oe.Deferred(),
                    g = oe.Callbacks("once memory"),
                    m = d.statusCode || {},
                    v = {},
                    y = {},
                    b = 0,
                    x = "canceled",
                    w = {
                        readyState: 0,
                        getResponseHeader: function(e) {
                            var t;
                            if (2 === b) {
                                if (!c)
                                    for (c = {}; t = At.exec(a);) c[t[1].toLowerCase()] = t[2];
                                t = c[e.toLowerCase()]
                            }
                            return null == t ? null : t
                        },
                        getAllResponseHeaders: function() {
                            return 2 === b ? a : null
                        },
                        setRequestHeader: function(e, t) {
                            var n = e.toLowerCase();
                            return b || (e = y[n] = y[n] || e, v[e] = t), this
                        },
                        overrideMimeType: function(e) {
                            return b || (d.mimeType = e), this
                        },
                        statusCode: function(e) {
                            var t;
                            if (e)
                                if (2 > b)
                                    for (t in e) m[t] = [m[t], e[t]];
                                else w.always(e[w.status]);
                            return this
                        },
                        abort: function(e) {
                            var t = e || x;
                            return u && u.abort(t), n(0, t), this
                        }
                    };
                if (h.promise(w).complete = g.add, w.success = w.done, w.error = w.fail, d.url = ((e || d.url || Et) + "").replace(/#.*$/, "").replace(/^\/\//, jt[1] + "//"), d.type = t.method || t.type || d.method || d.type, d.dataTypes = oe.trim(d.dataType || "*").toLowerCase().match(me) || [""], null == d.crossDomain && (r = Lt.exec(d.url.toLowerCase()), d.crossDomain = !(!r || r[1] === jt[1] && r[2] === jt[2] && (r[3] || ("http:" === r[1] ? "80" : "443")) === (jt[3] || ("http:" === jt[1] ? "80" : "443")))), d.data && d.processData && "string" != typeof d.data && (d.data = oe.param(d.data, d.traditional)), q(Dt, d, t, w), 2 === b) return w;
                l = d.global, l && 0 == oe.active++ && oe.event.trigger("ajaxStart"), d.type = d.type.toUpperCase(), d.hasContent = !Ht.test(d.type), o = d.url, d.hasContent || (d.data && (o = d.url += (St.test(o) ? "&" : "?") + d.data, delete d.data), d.cache === !1 && (d.url = Nt.test(o) ? o.replace(Nt, "$1_=" + kt++) : o + (St.test(o) ? "&" : "?") + "_=" + kt++)), d.ifModified && (oe.lastModified[o] && w.setRequestHeader("If-Modified-Since", oe.lastModified[o]), oe.etag[o] && w.setRequestHeader("If-None-Match", oe.etag[o])), (d.data && d.hasContent && d.contentType !== !1 || t.contentType) && w.setRequestHeader("Content-Type", d.contentType), w.setRequestHeader("Accept", d.dataTypes[0] && d.accepts[d.dataTypes[0]] ? d.accepts[d.dataTypes[0]] + ("*" !== d.dataTypes[0] ? ", " + Ot + "; q=0.01" : "") : d.accepts["*"]);
                for (i in d.headers) w.setRequestHeader(i, d.headers[i]);
                if (d.beforeSend && (d.beforeSend.call(f, w, d) === !1 || 2 === b)) return w.abort();
                x = "abort";
                for (i in {
                        success: 1,
                        error: 1,
                        complete: 1
                    }) w[i](d[i]);
                if (u = q(Mt, d, t, w)) {
                    w.readyState = 1, l && p.trigger("ajaxSend", [w, d]), d.async && d.timeout > 0 && (s = setTimeout(function() {
                        w.abort("timeout")
                    }, d.timeout));
                    try {
                        b = 1, u.send(v, n)
                    } catch (e) {
                        if (!(2 > b)) throw e;
                        n(-1, e)
                    }
                } else n(-1, "No Transport");
                return w
            },
            getJSON: function(e, t, n) {
                return oe.get(e, t, n, "json")
            },
            getScript: function(e, t) {
                return oe.get(e, void 0, t, "script")
            }
        }), oe.each(["get", "post"], function(e, t) {
            oe[t] = function(e, n, r, i) {
                return oe.isFunction(n) && (i = i || r, r = n, n = void 0), oe.ajax({
                    url: e,
                    type: t,
                    dataType: i,
                    data: n,
                    success: r
                })
            }
        }), oe.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(e, t) {
            oe.fn[t] = function(e) {
                return this.on(t, e)
            }
        }), oe._evalUrl = function(e) {
            return oe.ajax({
                url: e,
                type: "GET",
                dataType: "script",
                async: !1,
                global: !1,
                throws: !0
            })
        }, oe.fn.extend({
            wrapAll: function(e) {
                if (oe.isFunction(e)) return this.each(function(t) {
                    oe(this).wrapAll(e.call(this, t))
                });
                if (this[0]) {
                    var t = oe(e, this[0].ownerDocument).eq(0).clone(!0);
                    this[0].parentNode && t.insertBefore(this[0]), t.map(function() {
                        for (var e = this; e.firstChild && 1 === e.firstChild.nodeType;) e = e.firstChild;
                        return e
                    }).append(this)
                }
                return this
            },
            wrapInner: function(e) {
                return this.each(oe.isFunction(e) ? function(t) {
                    oe(this).wrapInner(e.call(this, t))
                } : function() {
                    var t = oe(this),
                        n = t.contents();
                    n.length ? n.wrapAll(e) : t.append(e)
                })
            },
            wrap: function(e) {
                var t = oe.isFunction(e);
                return this.each(function(n) {
                    oe(this).wrapAll(t ? e.call(this, n) : e)
                })
            },
            unwrap: function() {
                return this.parent().each(function() {
                    oe.nodeName(this, "body") || oe(this).replaceWith(this.childNodes)
                }).end()
            }
        }), oe.expr.filters.hidden = function(e) {
            return e.offsetWidth <= 0 && e.offsetHeight <= 0 || !re.reliableHiddenOffsets() && "none" === (e.style && e.style.display || oe.css(e, "display"))
        }, oe.expr.filters.visible = function(e) {
            return !oe.expr.filters.hidden(e)
        };
        var Ft = /\[\]$/,
            Bt = /^(?:submit|button|image|reset|file)$/i,
            $t = /^(?:input|select|textarea|keygen)/i;
        oe.param = function(e, t) {
            var n, r = [],
                i = function(e, t) {
                    t = oe.isFunction(t) ? t() : null == t ? "" : t, r[r.length] = encodeURIComponent(e) + "=" + encodeURIComponent(t)
                };
            if (void 0 === t && (t = oe.ajaxSettings && oe.ajaxSettings.traditional), oe.isArray(e) || e.jquery && !oe.isPlainObject(e)) oe.each(e, function() {
                i(this.name, this.value)
            });
            else
                for (n in e) I(n, e[n], t, i);
            return r.join("&").replace(/%20/g, "+")
        }, oe.fn.extend({
            serialize: function() {
                return oe.param(this.serializeArray())
            },
            serializeArray: function() {
                return this.map(function() {
                    var e = oe.prop(this, "elements");
                    return e ? oe.makeArray(e) : this
                }).filter(function() {
                    var e = this.type;
                    return this.name && !oe(this).is(":disabled") && $t.test(this.nodeName) && !Bt.test(e) && (this.checked || !Ee.test(e))
                }).map(function(e, t) {
                    var n = oe(this).val();
                    return null == n ? null : oe.isArray(n) ? oe.map(n, function(e) {
                        return {
                            name: t.name,
                            value: e.replace(/\r?\n/g, "\r\n")
                        }
                    }) : {
                        name: t.name,
                        value: n.replace(/\r?\n/g, "\r\n")
                    }
                }).get()
            }
        }), oe.ajaxSettings.xhr = void 0 !== e.ActiveXObject ? function() {
            return !this.isLocal && /^(get|post|head|put|delete|options)$/i.test(this.type) && W() || X()
        } : W;
        var Vt = 0,
            qt = {},
            _t = oe.ajaxSettings.xhr();
        e.ActiveXObject && oe(e).on("unload", function() {
            for (var e in qt) qt[e](void 0, !0)
        }), re.cors = !!_t && "withCredentials" in _t, (_t = re.ajax = !!_t) && oe.ajaxTransport(function(e) {
            if (!e.crossDomain || re.cors) {
                var t;
                return {
                    send: function(n, r) {
                        var i, o = e.xhr(),
                            a = ++Vt;
                        if (o.open(e.type, e.url, e.async, e.username, e.password), e.xhrFields)
                            for (i in e.xhrFields) o[i] = e.xhrFields[i];
                        e.mimeType && o.overrideMimeType && o.overrideMimeType(e.mimeType), e.crossDomain || n["X-Requested-With"] || (n["X-Requested-With"] = "XMLHttpRequest");
                        for (i in n) void 0 !== n[i] && o.setRequestHeader(i, n[i] + "");
                        o.send(e.hasContent && e.data || null), t = function(n, i) {
                            var s, l, u;
                            if (t && (i || 4 === o.readyState))
                                if (delete qt[a], t = void 0, o.onreadystatechange = oe.noop, i) 4 !== o.readyState && o.abort();
                                else {
                                    u = {}, s = o.status, "string" == typeof o.responseText && (u.text = o.responseText);
                                    try {
                                        l = o.statusText
                                    } catch (e) {
                                        l = ""
                                    }
                                    s || !e.isLocal || e.crossDomain ? 1223 === s && (s = 204) : s = u.text ? 200 : 404
                                }
                            u && r(s, l, u, o.getAllResponseHeaders())
                        }, e.async ? 4 === o.readyState ? setTimeout(t) : o.onreadystatechange = qt[a] = t : t()
                    },
                    abort: function() {
                        t && t(void 0, !0)
                    }
                }
            }
        }), oe.ajaxSetup({
            accepts: {
                script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
            },
            contents: {
                script: /(?:java|ecma)script/
            },
            converters: {
                "text script": function(e) {
                    return oe.globalEval(e), e
                }
            }
        }), oe.ajaxPrefilter("script", function(e) {
            void 0 === e.cache && (e.cache = !1), e.crossDomain && (e.type = "GET", e.global = !1)
        }), oe.ajaxTransport("script", function(e) {
            if (e.crossDomain) {
                var t, n = fe.head || oe("head")[0] || fe.documentElement;
                return {
                    send: function(r, i) {
                        t = fe.createElement("script"), t.async = !0, e.scriptCharset && (t.charset = e.scriptCharset), t.src = e.url, t.onload = t.onreadystatechange = function(e, n) {
                            (n || !t.readyState || /loaded|complete/.test(t.readyState)) && (t.onload = t.onreadystatechange = null, t.parentNode && t.parentNode.removeChild(t), t = null, n || i(200, "success"))
                        }, n.insertBefore(t, n.firstChild)
                    },
                    abort: function() {
                        t && t.onload(void 0, !0)
                    }
                }
            }
        });
        var Rt = [],
            zt = /(=)\?(?=&|$)|\?\?/;
        oe.ajaxSetup({
            jsonp: "callback",
            jsonpCallback: function() {
                var e = Rt.pop() || oe.expando + "_" + kt++;
                return this[e] = !0, e
            }
        }), oe.ajaxPrefilter("json jsonp", function(t, n, r) {
            var i, o, a, s = t.jsonp !== !1 && (zt.test(t.url) ? "url" : "string" == typeof t.data && !(t.contentType || "").indexOf("application/x-www-form-urlencoded") && zt.test(t.data) && "data");
            return s || "jsonp" === t.dataTypes[0] ? (i = t.jsonpCallback = oe.isFunction(t.jsonpCallback) ? t.jsonpCallback() : t.jsonpCallback, s ? t[s] = t[s].replace(zt, "$1" + i) : t.jsonp !== !1 && (t.url += (St.test(t.url) ? "&" : "?") + t.jsonp + "=" + i), t.converters["script json"] = function() {
                return a || oe.error(i + " was not called"), a[0]
            }, t.dataTypes[0] = "json", o = e[i], e[i] = function() {
                a = arguments
            }, r.always(function() {
                e[i] = o, t[i] && (t.jsonpCallback = n.jsonpCallback, Rt.push(i)), a && oe.isFunction(o) && o(a[0]), a = o = void 0
            }), "script") : void 0
        }), oe.parseHTML = function(e, t, n) {
            if (!e || "string" != typeof e) return null;
            "boolean" == typeof t && (n = t, t = !1), t = t || fe;
            var r = ue.exec(e),
                i = !n && [];
            return r ? [t.createElement(r[1])] : (r = oe.buildFragment([e], t, i), i && i.length && oe(i).remove(), oe.merge([], r.childNodes))
        };
        var It = oe.fn.load;
        oe.fn.load = function(e, t, n) {
            if ("string" != typeof e && It) return It.apply(this, arguments);
            var r, i, o, a = this,
                s = e.indexOf(" ");
            return s >= 0 && (r = e.slice(s, e.length), e = e.slice(0, s)), oe.isFunction(t) ? (n = t, t = void 0) : t && "object" == typeof t && (o = "POST"), a.length > 0 && oe.ajax({
                url: e,
                type: o,
                dataType: "html",
                data: t
            }).done(function(e) {
                i = arguments, a.html(r ? oe("<div>").append(oe.parseHTML(e)).find(r) : e)
            }).complete(n && function(e, t) {
                a.each(n, i || [e.responseText, t, e])
            }), this
        }, oe.expr.filters.animated = function(e) {
            return oe.grep(oe.timers, function(t) {
                return e === t.elem
            }).length
        };
        var Wt = e.document.documentElement;
        oe.offset = {
            setOffset: function(e, t, n) {
                var r, i, o, a, s, l, u, c = oe.css(e, "position"),
                    d = oe(e),
                    f = {};
                "static" === c && (e.style.position = "relative"), s = d.offset(), o = oe.css(e, "top"), l = oe.css(e, "left"), u = ("absolute" === c || "fixed" === c) && oe.inArray("auto", [o, l]) > -1, u ? (r = d.position(), a = r.top, i = r.left) : (a = parseFloat(o) || 0, i = parseFloat(l) || 0), oe.isFunction(t) && (t = t.call(e, n, s)), null != t.top && (f.top = t.top - s.top + a), null != t.left && (f.left = t.left - s.left + i), "using" in t ? t.using.call(e, f) : d.css(f)
            }
        }, oe.fn.extend({
            offset: function(e) {
                if (arguments.length) return void 0 === e ? this : this.each(function(t) {
                    oe.offset.setOffset(this, e, t)
                });
                var t, n, r = {
                        top: 0,
                        left: 0
                    },
                    i = this[0],
                    o = i && i.ownerDocument;
                return o ? (t = o.documentElement, oe.contains(t, i) ? (typeof i.getBoundingClientRect !== xe && (r = i.getBoundingClientRect()), n = Y(o), {
                    top: r.top + (n.pageYOffset || t.scrollTop) - (t.clientTop || 0),
                    left: r.left + (n.pageXOffset || t.scrollLeft) - (t.clientLeft || 0)
                }) : r) : void 0
            },
            position: function() {
                if (this[0]) {
                    var e, t, n = {
                            top: 0,
                            left: 0
                        },
                        r = this[0];
                    return "fixed" === oe.css(r, "position") ? t = r.getBoundingClientRect() : (e = this.offsetParent(), t = this.offset(), oe.nodeName(e[0], "html") || (n = e.offset()), n.top += oe.css(e[0], "borderTopWidth", !0), n.left += oe.css(e[0], "borderLeftWidth", !0)), {
                        top: t.top - n.top - oe.css(r, "marginTop", !0),
                        left: t.left - n.left - oe.css(r, "marginLeft", !0)
                    }
                }
            },
            offsetParent: function() {
                return this.map(function() {
                    for (var e = this.offsetParent || Wt; e && !oe.nodeName(e, "html") && "static" === oe.css(e, "position");) e = e.offsetParent;
                    return e || Wt
                })
            }
        }), oe.each({
            scrollLeft: "pageXOffset",
            scrollTop: "pageYOffset"
        }, function(e, t) {
            var n = /Y/.test(t);
            oe.fn[e] = function(r) {
                return je(this, function(e, r, i) {
                    var o = Y(e);
                    return void 0 === i ? o ? t in o ? o[t] : o.document.documentElement[r] : e[r] : void(o ? o.scrollTo(n ? oe(o).scrollLeft() : i, n ? i : oe(o).scrollTop()) : e[r] = i)
                }, e, r, arguments.length, null)
            }
        }), oe.each(["top", "left"], function(e, t) {
            oe.cssHooks[t] = j(re.pixelPosition, function(e, n) {
                return n ? (n = Ue(e, t), Ze.test(n) ? oe(e).position()[t] + "px" : n) : void 0
            })
        }), oe.each({
            Height: "height",
            Width: "width"
        }, function(e, t) {
            oe.each({
                padding: "inner" + e,
                content: t,
                "": "outer" + e
            }, function(n, r) {
                oe.fn[r] = function(r, i) {
                    var o = arguments.length && (n || "boolean" != typeof r),
                        a = n || (r === !0 || i === !0 ? "margin" : "border");
                    return je(this, function(t, n, r) {
                        var i;
                        return oe.isWindow(t) ? t.document.documentElement["client" + e] : 9 === t.nodeType ? (i = t.documentElement, Math.max(t.body["scroll" + e], i["scroll" + e], t.body["offset" + e], i["offset" + e], i["client" + e])) : void 0 === r ? oe.css(t, n, a) : oe.style(t, n, r, a)
                    }, t, o ? r : void 0, o, null)
                }
            })
        }), oe.fn.size = function() {
            return this.length
        }, oe.fn.andSelf = oe.fn.addBack, "function" == typeof define && define.amd && define("jquery", [], function() {
            return oe
        });
        var Xt = e.jQuery,
            Yt = e.$;
        return oe.noConflict = function(t) {
            return e.$ === oe && (e.$ = Yt), t && e.jQuery === oe && (e.jQuery = Xt), oe
        }, typeof t === xe && (e.jQuery = e.$ = oe), oe
    }),
    function(e) {
        e.fn.SmoothScroll = function(t) {
            var n = {
                duration: 1e3,
                easing: "easeOutQuint"
            };
            t = e.extend(n, t);
            var r, i = {
                scrollStop: function() {
                    r.stop(!0)
                },
                getTargetBody: function() {
                    return e("html").scrollTop() > 0 ? r = e("html") : e("body").scrollTop() > 0 && (r = e("body")), r
                }
            };
            return this.each(function(n, o) {
                e(o).on("click", function() {
                    var n = this.hash,
                        o = e(n).eq(0).offset();
                    if (n && null !== o) {
                        var a = e(window).scrollTop();
                        if (0 === a && e(window).scrollTop(a + 1), void 0 !== (r = i.getTargetBody())) return r.animate({
                            scrollTop: o.top
                        }, t.duration, t.easing, function() {}), window.addEventListener && window.addEventListener("DOMMouseScroll", i.scrollStop, !1), window.onmousewheel = document.onmousewheel = i.scrollStop, !1
                    }
                })
            })
        }
    }(jQuery), jQuery(function(e) {
        e(window).on("load", function() {
            e('a[href^="#"]').SmoothScroll()
        }), jQuery.effects || function(e, t) {
            var n = e.uiBackCompat !== !1,
                r = "ui-effects-";
            e.effects = {
                    effect: {}
                },
                function(t, n) {
                    function r(e, t, n) {
                        var r = f[t.type] || {};
                        return null == e ? n || !t.def ? null : t.def : (e = r.floor ? ~~e : parseFloat(e), isNaN(e) ? t.def : r.mod ? (e + r.mod) % r.mod : 0 > e ? 0 : r.max < e ? r.max : e)
                    }

                    function i(e) {
                        var n = c(),
                            r = n._rgba = [];
                        return e = e.toLowerCase(), g(u, function(t, i) {
                            var o, a = i.re.exec(e),
                                s = a && i.parse(a),
                                l = i.space || "rgba";
                            if (s) return o = n[l](s), n[d[l].cache] = o[d[l].cache], r = n._rgba = o._rgba, !1
                        }), r.length ? ("0,0,0,0" === r.join() && t.extend(r, a.transparent), n) : a[e]
                    }

                    function o(e, t, n) {
                        return n = (n + 1) % 1, 6 * n < 1 ? e + (t - e) * n * 6 : 2 * n < 1 ? t : 3 * n < 2 ? e + (t - e) * (2 / 3 - n) * 6 : e
                    }
                    var a, s = "backgroundColor borderBottomColor borderLeftColor borderRightColor borderTopColor color columnRuleColor outlineColor textDecorationColor textEmphasisColor".split(" "),
                        l = /^([\-+])=\s*(\d+\.?\d*)/,
                        u = [{
                            re: /rgba?\(\s*(\d{1,3})\s*,\s*(\d{1,3})\s*,\s*(\d{1,3})\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
                            parse: function(e) {
                                return [e[1], e[2], e[3], e[4]]
                            }
                        }, {
                            re: /rgba?\(\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
                            parse: function(e) {
                                return [2.55 * e[1], 2.55 * e[2], 2.55 * e[3], e[4]]
                            }
                        }, {
                            re: /#([a-f0-9]{2})([a-f0-9]{2})([a-f0-9]{2})/,
                            parse: function(e) {
                                return [parseInt(e[1], 16), parseInt(e[2], 16), parseInt(e[3], 16)]
                            }
                        }, {
                            re: /#([a-f0-9])([a-f0-9])([a-f0-9])/,
                            parse: function(e) {
                                return [parseInt(e[1] + e[1], 16), parseInt(e[2] + e[2], 16), parseInt(e[3] + e[3], 16)]
                            }
                        }, {
                            re: /hsla?\(\s*(\d+(?:\.\d+)?)\s*,\s*(\d+(?:\.\d+)?)\%\s*,\s*(\d+(?:\.\d+)?)\%\s*(?:,\s*(\d+(?:\.\d+)?)\s*)?\)/,
                            space: "hsla",
                            parse: function(e) {
                                return [e[1], e[2] / 100, e[3] / 100, e[4]]
                            }
                        }],
                        c = t.Color = function(e, n, r, i) {
                            return new t.Color.fn.parse(e, n, r, i)
                        },
                        d = {
                            rgba: {
                                props: {
                                    red: {
                                        idx: 0,
                                        type: "byte"
                                    },
                                    green: {
                                        idx: 1,
                                        type: "byte"
                                    },
                                    blue: {
                                        idx: 2,
                                        type: "byte"
                                    }
                                }
                            },
                            hsla: {
                                props: {
                                    hue: {
                                        idx: 0,
                                        type: "degrees"
                                    },
                                    saturation: {
                                        idx: 1,
                                        type: "percent"
                                    },
                                    lightness: {
                                        idx: 2,
                                        type: "percent"
                                    }
                                }
                            }
                        },
                        f = {
                            byte: {
                                floor: !0,
                                max: 255
                            },
                            percent: {
                                max: 1
                            },
                            degrees: {
                                mod: 360,
                                floor: !0
                            }
                        },
                        p = c.support = {},
                        h = t("<p>")[0],
                        g = t.each;
                    h.style.cssText = "background-color:rgba(1,1,1,.5)", p.rgba = h.style.backgroundColor.indexOf("rgba") > -1, g(d, function(e, t) {
                        t.cache = "_" + e, t.props.alpha = {
                            idx: 3,
                            type: "percent",
                            def: 1
                        }
                    }), c.fn = t.extend(c.prototype, {
                        parse: function(o, s, l, u) {
                            if (o === n) return this._rgba = [null, null, null, null], this;
                            (o.jquery || o.nodeType) && (o = t(o).css(s), s = n);
                            var f = this,
                                p = t.type(o),
                                h = this._rgba = [];
                            return s !== n && (o = [o, s, l, u], p = "array"), "string" === p ? this.parse(i(o) || a._default) : "array" === p ? (g(d.rgba.props, function(e, t) {
                                h[t.idx] = r(o[t.idx], t)
                            }), this) : "object" === p ? (o instanceof c ? g(d, function(e, t) {
                                o[t.cache] && (f[t.cache] = o[t.cache].slice())
                            }) : g(d, function(t, n) {
                                var i = n.cache;
                                g(n.props, function(e, t) {
                                    if (!f[i] && n.to) {
                                        if ("alpha" === e || null == o[e]) return;
                                        f[i] = n.to(f._rgba)
                                    }
                                    f[i][t.idx] = r(o[e], t, !0)
                                }), f[i] && e.inArray(null, f[i].slice(0, 3)) < 0 && (f[i][3] = 1, n.from && (f._rgba = n.from(f[i])))
                            }), this) : void 0
                        },
                        is: function(e) {
                            var t = c(e),
                                n = !0,
                                r = this;
                            return g(d, function(e, i) {
                                var o, a = t[i.cache];
                                return a && (o = r[i.cache] || i.to && i.to(r._rgba) || [], g(i.props, function(e, t) {
                                    if (null != a[t.idx]) return n = a[t.idx] === o[t.idx]
                                })), n
                            }), n
                        },
                        _space: function() {
                            var e = [],
                                t = this;
                            return g(d, function(n, r) {
                                t[r.cache] && e.push(n)
                            }), e.pop()
                        },
                        transition: function(e, t) {
                            var n = c(e),
                                i = n._space(),
                                o = d[i],
                                a = 0 === this.alpha() ? c("transparent") : this,
                                s = a[o.cache] || o.to(a._rgba),
                                l = s.slice();
                            return n = n[o.cache], g(o.props, function(e, i) {
                                var o = i.idx,
                                    a = s[o],
                                    u = n[o],
                                    c = f[i.type] || {};
                                null !== u && (null === a ? l[o] = u : (c.mod && (u - a > c.mod / 2 ? a += c.mod : a - u > c.mod / 2 && (a -= c.mod)), l[o] = r((u - a) * t + a, i)))
                            }), this[i](l)
                        },
                        blend: function(e) {
                            if (1 === this._rgba[3]) return this;
                            var n = this._rgba.slice(),
                                r = n.pop(),
                                i = c(e)._rgba;
                            return c(t.map(n, function(e, t) {
                                return (1 - r) * i[t] + r * e
                            }))
                        },
                        toRgbaString: function() {
                            var e = "rgba(",
                                n = t.map(this._rgba, function(e, t) {
                                    return null == e ? t > 2 ? 1 : 0 : e
                                });
                            return 1 === n[3] && (n.pop(), e = "rgb("), e + n.join() + ")"
                        },
                        toHslaString: function() {
                            var e = "hsla(",
                                n = t.map(this.hsla(), function(e, t) {
                                    return null == e && (e = t > 2 ? 1 : 0), t && t < 3 && (e = Math.round(100 * e) + "%"), e
                                });
                            return 1 === n[3] && (n.pop(), e = "hsl("), e + n.join() + ")"
                        },
                        toHexString: function(e) {
                            var n = this._rgba.slice(),
                                r = n.pop();
                            return e && n.push(~~(255 * r)), "#" + t.map(n, function(e) {
                                return e = (e || 0).toString(16), 1 === e.length ? "0" + e : e
                            }).join("")
                        },
                        toString: function() {
                            return 0 === this._rgba[3] ? "transparent" : this.toRgbaString()
                        }
                    }), c.fn.parse.prototype = c.fn, d.hsla.to = function(e) {
                        if (null == e[0] || null == e[1] || null == e[2]) return [null, null, null, e[3]];
                        var t, n, r = e[0] / 255,
                            i = e[1] / 255,
                            o = e[2] / 255,
                            a = e[3],
                            s = Math.max(r, i, o),
                            l = Math.min(r, i, o),
                            u = s - l,
                            c = s + l,
                            d = .5 * c;
                        return t = l === s ? 0 : r === s ? 60 * (i - o) / u + 360 : i === s ? 60 * (o - r) / u + 120 : 60 * (r - i) / u + 240, n = 0 === d || 1 === d ? d : d <= .5 ? u / c : u / (2 - c), [Math.round(t) % 360, n, d, null == a ? 1 : a]
                    }, d.hsla.from = function(e) {
                        if (null == e[0] || null == e[1] || null == e[2]) return [null, null, null, e[3]];
                        var t = e[0] / 360,
                            n = e[1],
                            r = e[2],
                            i = e[3],
                            a = r <= .5 ? r * (1 + n) : r + n - r * n,
                            s = 2 * r - a;
                        return [Math.round(255 * o(s, a, t + 1 / 3)), Math.round(255 * o(s, a, t)), Math.round(255 * o(s, a, t - 1 / 3)), i]
                    }, g(d, function(e, i) {
                        var o = i.props,
                            a = i.cache,
                            s = i.to,
                            u = i.from;
                        c.fn[e] = function(e) {
                            if (s && !this[a] && (this[a] = s(this._rgba)), e === n) return this[a].slice();
                            var i, l = t.type(e),
                                d = "array" === l || "object" === l ? e : arguments,
                                f = this[a].slice();
                            return g(o, function(e, t) {
                                var n = d["object" === l ? e : t.idx];
                                null == n && (n = f[t.idx]), f[t.idx] = r(n, t)
                            }), u ? (i = c(u(f)), i[a] = f, i) : c(f)
                        }, g(o, function(n, r) {
                            c.fn[n] || (c.fn[n] = function(i) {
                                var o, a = t.type(i),
                                    s = "alpha" === n ? this._hsla ? "hsla" : "rgba" : e,
                                    u = this[s](),
                                    c = u[r.idx];
                                return "undefined" === a ? c : ("function" === a && (i = i.call(this, c), a = t.type(i)), null == i && r.empty ? this : ("string" === a && (o = l.exec(i)) && (i = c + parseFloat(o[2]) * ("+" === o[1] ? 1 : -1)), u[r.idx] = i, this[s](u)))
                            })
                        })
                    }), g(s, function(e, n) {
                        t.cssHooks[n] = {
                            set: function(e, r) {
                                var o, a, s = "";
                                if ("string" !== t.type(r) || (o = i(r))) {
                                    if (r = c(o || r), !p.rgba && 1 !== r._rgba[3]) {
                                        for (a = "backgroundColor" === n ? e.parentNode : e;
                                            ("" === s || "transparent" === s) && a && a.style;) try {
                                            s = t.css(a, "backgroundColor"), a = a.parentNode
                                        } catch (e) {}
                                        r = r.blend(s && "transparent" !== s ? s : "_default")
                                    }
                                    r = r.toRgbaString()
                                }
                                try {
                                    e.style[n] = r
                                } catch (e) {}
                            }
                        }, t.fx.step[n] = function(e) {
                            e.colorInit || (e.start = c(e.elem, n), e.end = c(e.end), e.colorInit = !0), t.cssHooks[n].set(e.elem, e.start.transition(e.end, e.pos))
                        }
                    }), t.cssHooks.borderColor = {
                        expand: function(e) {
                            var t = {};
                            return g(["Top", "Right", "Bottom", "Left"], function(n, r) {
                                t["border" + r + "Color"] = e
                            }), t
                        }
                    }, a = t.Color.names = {
                        aqua: "#00ffff",
                        black: "#000000",
                        blue: "#0000ff",
                        fuchsia: "#ff00ff",
                        gray: "#808080",
                        green: "#008000",
                        lime: "#00ff00",
                        maroon: "#800000",
                        navy: "#000080",
                        olive: "#808000",
                        purple: "#800080",
                        red: "#ff0000",
                        silver: "#c0c0c0",
                        teal: "#008080",
                        white: "#ffffff",
                        yellow: "#ffff00",
                        transparent: [null, null, null, 0],
                        _default: "#ffffff"
                    }
                }(jQuery),
                function() {
                    function n() {
                        var t, n, r = this.ownerDocument.defaultView ? this.ownerDocument.defaultView.getComputedStyle(this, null) : this.currentStyle,
                            i = {};
                        if (r && r.length && r[0] && r[r[0]])
                            for (n = r.length; n--;) t = r[n], "string" == typeof r[t] && (i[e.camelCase(t)] = r[t]);
                        else
                            for (t in r) "string" == typeof r[t] && (i[t] = r[t]);
                        return i
                    }

                    function r(t, n) {
                        var r, i, a = {};
                        for (r in n) i = n[r], t[r] !== i && !o[r] && (e.fx.step[r] || !isNaN(parseFloat(i))) && (a[r] = i);
                        return a
                    }
                    var i = ["add", "remove", "toggle"],
                        o = {
                            border: 1,
                            borderBottom: 1,
                            borderColor: 1,
                            borderLeft: 1,
                            borderRight: 1,
                            borderTop: 1,
                            borderWidth: 1,
                            margin: 1,
                            padding: 1
                        };
                    e.each(["borderLeftStyle", "borderRightStyle", "borderBottomStyle", "borderTopStyle"], function(t, n) {
                        e.fx.step[n] = function(e) {
                            ("none" !== e.end && !e.setAttr || 1 === e.pos && !e.setAttr) && (jQuery.style(e.elem, n, e.end), e.setAttr = !0)
                        }
                    }), e.effects.animateClass = function(t, o, a, s) {
                        var l = e.speed(o, a, s);
                        return this.queue(function() {
                            var o, a = e(this),
                                s = a.attr("class") || "",
                                u = l.children ? a.find("*").andSelf() : a;
                            u = u.map(function() {
                                return {
                                    el: e(this),
                                    start: n.call(this)
                                }
                            }), o = function() {
                                e.each(i, function(e, n) {
                                    t[n] && a[n + "Class"](t[n])
                                })
                            }, o(), u = u.map(function() {
                                return this.end = n.call(this.el[0]), this.diff = r(this.start, this.end), this
                            }), a.attr("class", s), u = u.map(function() {
                                var t = this,
                                    n = e.Deferred(),
                                    r = jQuery.extend({}, l, {
                                        queue: !1,
                                        complete: function() {
                                            n.resolve(t)
                                        }
                                    });
                                return this.el.animate(this.diff, r), n.promise()
                            }), e.when.apply(e, u.get()).done(function() {
                                o(), e.each(arguments, function() {
                                    var t = this.el;
                                    e.each(this.diff, function(e) {
                                        t.css(e, "")
                                    })
                                }), l.complete.call(a[0])
                            })
                        })
                    }, e.fn.extend({
                        _addClass: e.fn.addClass,
                        addClass: function(t, n, r, i) {
                            return n ? e.effects.animateClass.call(this, {
                                add: t
                            }, n, r, i) : this._addClass(t)
                        },
                        _removeClass: e.fn.removeClass,
                        removeClass: function(t, n, r, i) {
                            return n ? e.effects.animateClass.call(this, {
                                remove: t
                            }, n, r, i) : this._removeClass(t)
                        },
                        _toggleClass: e.fn.toggleClass,
                        toggleClass: function(n, r, i, o, a) {
                            return "boolean" == typeof r || r === t ? i ? e.effects.animateClass.call(this, r ? {
                                add: n
                            } : {
                                remove: n
                            }, i, o, a) : this._toggleClass(n, r) : e.effects.animateClass.call(this, {
                                toggle: n
                            }, r, i, o)
                        },
                        switchClass: function(t, n, r, i, o) {
                            return e.effects.animateClass.call(this, {
                                add: n,
                                remove: t
                            }, r, i, o)
                        }
                    })
                }(),
                function() {
                    function i(t, n, r, i) {
                        return e.isPlainObject(t) && (n = t, t = t.effect), t = {
                            effect: t
                        }, null == n && (n = {}), e.isFunction(n) && (i = n, r = null, n = {}), ("number" == typeof n || e.fx.speeds[n]) && (i = r, r = n, n = {}), e.isFunction(r) && (i = r, r = null), n && e.extend(t, n), r = r || n.duration, t.duration = e.fx.off ? 0 : "number" == typeof r ? r : r in e.fx.speeds ? e.fx.speeds[r] : e.fx.speeds._default, t.complete = i || n.complete, t
                    }

                    function o(t) {
                        return !(t && "number" != typeof t && !e.fx.speeds[t]) || "string" == typeof t && !e.effects.effect[t] && (!n || !e.effects[t])
                    }
                    e.extend(e.effects, {
                        version: "1.9.2",
                        save: function(e, t) {
                            for (var n = 0; n < t.length; n++) null !== t[n] && e.data(r + t[n], e[0].style[t[n]])
                        },
                        restore: function(e, n) {
                            var i, o;
                            for (o = 0; o < n.length; o++) null !== n[o] && (i = e.data(r + n[o]), i === t && (i = ""), e.css(n[o], i))
                        },
                        setMode: function(e, t) {
                            return "toggle" === t && (t = e.is(":hidden") ? "show" : "hide"), t
                        },
                        getBaseline: function(e, t) {
                            var n, r;
                            switch (e[0]) {
                                case "top":
                                    n = 0;
                                    break;
                                case "middle":
                                    n = .5;
                                    break;
                                case "bottom":
                                    n = 1;
                                    break;
                                default:
                                    n = e[0] / t.height
                            }
                            switch (e[1]) {
                                case "left":
                                    r = 0;
                                    break;
                                case "center":
                                    r = .5;
                                    break;
                                case "right":
                                    r = 1;
                                    break;
                                default:
                                    r = e[1] / t.width
                            }
                            return {
                                x: r,
                                y: n
                            }
                        },
                        createWrapper: function(t) {
                            if (t.parent().is(".ui-effects-wrapper")) return t.parent();
                            var n = {
                                    width: t.outerWidth(!0),
                                    height: t.outerHeight(!0),
                                    float: t.css("float")
                                },
                                r = e("<div></div>").addClass("ui-effects-wrapper").css({
                                    fontSize: "100%",
                                    background: "transparent",
                                    border: "none",
                                    margin: 0,
                                    padding: 0
                                }),
                                i = {
                                    width: t.width(),
                                    height: t.height()
                                },
                                o = document.activeElement;
                            try {
                                o.id
                            } catch (e) {
                                o = document.body
                            }
                            return t.wrap(r), (t[0] === o || e.contains(t[0], o)) && e(o).focus(), r = t.parent(), "static" === t.css("position") ? (r.css({
                                position: "relative"
                            }), t.css({
                                position: "relative"
                            })) : (e.extend(n, {
                                position: t.css("position"),
                                zIndex: t.css("z-index")
                            }), e.each(["top", "left", "bottom", "right"], function(e, r) {
                                n[r] = t.css(r), isNaN(parseInt(n[r], 10)) && (n[r] = "auto")
                            }), t.css({
                                position: "relative",
                                top: 0,
                                left: 0,
                                right: "auto",
                                bottom: "auto"
                            })), t.css(i), r.css(n).show()
                        },
                        removeWrapper: function(t) {
                            var n = document.activeElement;
                            return t.parent().is(".ui-effects-wrapper") && (t.parent().replaceWith(t), (t[0] === n || e.contains(t[0], n)) && e(n).focus()), t
                        },
                        setTransition: function(t, n, r, i) {
                            return i = i || {}, e.each(n, function(e, n) {
                                var o = t.cssUnit(n);
                                o[0] > 0 && (i[n] = o[0] * r + o[1])
                            }), i
                        }
                    }), e.fn.extend({
                        effect: function() {
                            function t(t) {
                                function n() {
                                    e.isFunction(o) && o.call(i[0]), e.isFunction(t) && t()
                                }
                                var i = e(this),
                                    o = r.complete,
                                    a = r.mode;
                                (i.is(":hidden") ? "hide" === a : "show" === a) ? n(): s.call(i[0], r, n)
                            }
                            var r = i.apply(this, arguments),
                                o = r.mode,
                                a = r.queue,
                                s = e.effects.effect[r.effect],
                                l = !s && n && e.effects[r.effect];
                            return e.fx.off || !s && !l ? o ? this[o](r.duration, r.complete) : this.each(function() {
                                r.complete && r.complete.call(this)
                            }) : s ? a === !1 ? this.each(t) : this.queue(a || "fx", t) : l.call(this, {
                                options: r,
                                duration: r.duration,
                                callback: r.complete,
                                mode: r.mode
                            })
                        },
                        _show: e.fn.show,
                        show: function(e) {
                            if (o(e)) return this._show.apply(this, arguments);
                            var t = i.apply(this, arguments);
                            return t.mode = "show", this.effect.call(this, t)
                        },
                        _hide: e.fn.hide,
                        hide: function(e) {
                            if (o(e)) return this._hide.apply(this, arguments);
                            var t = i.apply(this, arguments);
                            return t.mode = "hide", this.effect.call(this, t)
                        },
                        __toggle: e.fn.toggle,
                        toggle: function(t) {
                            if (o(t) || "boolean" == typeof t || e.isFunction(t)) return this.__toggle.apply(this, arguments);
                            var n = i.apply(this, arguments);
                            return n.mode = "toggle", this.effect.call(this, n)
                        },
                        cssUnit: function(t) {
                            var n = this.css(t),
                                r = [];
                            return e.each(["em", "px", "%", "pt"], function(e, t) {
                                n.indexOf(t) > 0 && (r = [parseFloat(n), t])
                            }), r
                        }
                    })
                }(),
                function() {
                    var t = {};
                    e.each(["Quad", "Cubic", "Quart", "Quint", "Expo"], function(e, n) {
                        t[n] = function(t) {
                            return Math.pow(t, e + 2)
                        }
                    }), e.extend(t, {
                        Sine: function(e) {
                            return 1 - Math.cos(e * Math.PI / 2)
                        },
                        Circ: function(e) {
                            return 1 - Math.sqrt(1 - e * e)
                        },
                        Elastic: function(e) {
                            return 0 === e || 1 === e ? e : -Math.pow(2, 8 * (e - 1)) * Math.sin((80 * (e - 1) - 7.5) * Math.PI / 15)
                        },
                        Back: function(e) {
                            return e * e * (3 * e - 2)
                        },
                        Bounce: function(e) {
                            for (var t, n = 4; e < ((t = Math.pow(2, --n)) - 1) / 11;);
                            return 1 / Math.pow(4, 3 - n) - 7.5625 * Math.pow((3 * t - 2) / 22 - e, 2)
                        }
                    }), e.each(t, function(t, n) {
                        e.easing["easeIn" + t] = n, e.easing["easeOut" + t] = function(e) {
                            return 1 - n(1 - e)
                        }, e.easing["easeInOut" + t] = function(e) {
                            return e < .5 ? n(2 * e) / 2 : 1 - n(e * -2 + 2) / 2
                        }
                    })
                }()
        }(jQuery)
    }), ! function(e) {
        function t(e) {
            var t = e.length,
                r = n.type(e);
            return "function" !== r && !n.isWindow(e) && (!(1 !== e.nodeType || !t) || ("array" === r || 0 === t || "number" == typeof t && t > 0 && t - 1 in e))
        }
        if (!e.jQuery) {
            var n = function(e, t) {
                return new n.fn.init(e, t)
            };
            n.isWindow = function(e) {
                return null != e && e == e.window
            }, n.type = function(e) {
                return null == e ? e + "" : "object" == typeof e || "function" == typeof e ? i[a.call(e)] || "object" : typeof e
            }, n.isArray = Array.isArray || function(e) {
                return "array" === n.type(e)
            }, n.isPlainObject = function(e) {
                var t;
                if (!e || "object" !== n.type(e) || e.nodeType || n.isWindow(e)) return !1;
                try {
                    if (e.constructor && !o.call(e, "constructor") && !o.call(e.constructor.prototype, "isPrototypeOf")) return !1
                } catch (e) {
                    return !1
                }
                for (t in e);
                return void 0 === t || o.call(e, t)
            }, n.each = function(e, n, r) {
                var i = 0,
                    o = e.length,
                    a = t(e);
                if (r) {
                    if (a)
                        for (; o > i && n.apply(e[i], r) !== !1; i++);
                    else
                        for (i in e)
                            if (n.apply(e[i], r) === !1) break
                } else if (a)
                    for (; o > i && n.call(e[i], i, e[i]) !== !1; i++);
                else
                    for (i in e)
                        if (n.call(e[i], i, e[i]) === !1) break; return e
            }, n.data = function(e, t, i) {
                if (void 0 === i) {
                    var o = e[n.expando],
                        a = o && r[o];
                    if (void 0 === t) return a;
                    if (a && t in a) return a[t]
                } else if (void 0 !== t) {
                    var o = e[n.expando] || (e[n.expando] = ++n.uuid);
                    return r[o] = r[o] || {}, r[o][t] = i, i
                }
            }, n.removeData = function(e, t) {
                var i = e[n.expando],
                    o = i && r[i];
                o && n.each(t, function(e, t) {
                    delete o[t]
                })
            }, n.extend = function() {
                var e, t, r, i, o, a, s = arguments[0] || {},
                    l = 1,
                    u = arguments.length,
                    c = !1;
                for ("boolean" == typeof s && (c = s, s = arguments[l] || {}, l++), "object" != typeof s && "function" !== n.type(s) && (s = {}), l === u && (s = this, l--); u > l; l++)
                    if (null != (o = arguments[l]))
                        for (i in o) e = s[i], r = o[i], s !== r && (c && r && (n.isPlainObject(r) || (t = n.isArray(r))) ? (t ? (t = !1, a = e && n.isArray(e) ? e : []) : a = e && n.isPlainObject(e) ? e : {}, s[i] = n.extend(c, a, r)) : void 0 !== r && (s[i] = r));
                return s
            }, n.queue = function(e, r, i) {
                if (e) {
                    r = (r || "fx") + "queue";
                    var o = n.data(e, r);
                    return i ? (!o || n.isArray(i) ? o = n.data(e, r, function(e, n) {
                        var r = n || [];
                        return null != e && (t(Object(e)) ? function(e, t) {
                            for (var n = +t.length, r = 0, i = e.length; n > r;) e[i++] = t[r++];
                            if (n !== n)
                                for (; void 0 !== t[r];) e[i++] = t[r++];
                            e.length = i, e
                        }(r, "string" == typeof e ? [e] : e) : [].push.call(r, e)), r
                    }(i)) : o.push(i), o) : o || []
                }
            }, n.dequeue = function(e, t) {
                n.each(e.nodeType ? [e] : e, function(e, r) {
                    t = t || "fx";
                    var i = n.queue(r, t),
                        o = i.shift();
                    "inprogress" === o && (o = i.shift()), o && ("fx" === t && i.unshift("inprogress"), o.call(r, function() {
                        n.dequeue(r, t)
                    }))
                })
            }, n.fn = n.prototype = {
                init: function(e) {
                    if (e.nodeType) return this[0] = e, this;
                    throw new Error("Not a DOM node.")
                },
                offset: function() {
                    var t = this[0].getBoundingClientRect ? this[0].getBoundingClientRect() : {
                        top: 0,
                        left: 0
                    };
                    return {
                        top: t.top + (e.pageYOffset || document.scrollTop || 0) - (document.clientTop || 0),
                        left: t.left + (e.pageXOffset || document.scrollLeft || 0) - (document.clientLeft || 0)
                    }
                },
                position: function() {
                    function e() {
                        for (var e = this.offsetParent || document; e && "html" === !e.nodeType.toLowerCase && "static" === e.style.position;) e = e.offsetParent;
                        return e || document
                    }
                    var t = this[0],
                        e = e.apply(t),
                        r = this.offset(),
                        i = /^(?:body|html)$/i.test(e.nodeName) ? {
                            top: 0,
                            left: 0
                        } : n(e).offset();
                    return r.top -= parseFloat(t.style.marginTop) || 0, r.left -= parseFloat(t.style.marginLeft) || 0, e.style && (i.top += parseFloat(e.style.borderTopWidth) || 0, i.left += parseFloat(e.style.borderLeftWidth) || 0), {
                        top: r.top - i.top,
                        left: r.left - i.left
                    }
                }
            };
            var r = {};
            n.expando = "velocity" + (new Date).getTime(), n.uuid = 0;
            for (var i = {}, o = i.hasOwnProperty, a = i.toString, s = "Boolean Number String Function Array Date RegExp Object Error".split(" "), l = 0; l < s.length; l++) i["[object " + s[l] + "]"] = s[l].toLowerCase();
            n.fn.init.prototype = n.fn, e.Velocity = {
                Utilities: n
            }
        }
    }(window),
    function(e) {
        "object" == typeof module && "object" == typeof module.exports ? module.exports = e() : "function" == typeof define && define.amd ? define(e) : e()
    }(function() {
        return function(e, t, n, r) {
            function i(e) {
                for (var t = -1, n = e ? e.length : 0, r = []; ++t < n;) {
                    var i = e[t];
                    i && r.push(i)
                }
                return r
            }

            function o(e) {
                return g.isWrapped(e) ? e = [].slice.call(e) : g.isNode(e) && (e = [e]), e
            }

            function a(e) {
                var t = f.data(e, "velocity");
                return null === t ? r : t
            }

            function s(e) {
                return function(t) {
                    return Math.round(t * e) * (1 / e)
                }
            }

            function l(e, n, r, i) {
                function o(e, t) {
                    return 1 - 3 * t + 3 * e
                }

                function a(e, t) {
                    return 3 * t - 6 * e
                }

                function s(e) {
                    return 3 * e
                }

                function l(e, t, n) {
                    return ((o(t, n) * e + a(t, n)) * e + s(t)) * e
                }

                function u(e, t, n) {
                    return 3 * o(t, n) * e * e + 2 * a(t, n) * e + s(t)
                }

                function c(t, n) {
                    for (var i = 0; g > i; ++i) {
                        var o = u(n, e, r);
                        if (0 === o) return n;
                        n -= (l(n, e, r) - t) / o
                    }
                    return n
                }

                function d() {
                    for (var t = 0; b > t; ++t) T[t] = l(t * x, e, r)
                }

                function f(t, n, i) {
                    var o, a, s = 0;
                    do {
                        a = n + (i - n) / 2, o = l(a, e, r) - t, o > 0 ? i = a : n = a
                    } while (Math.abs(o) > v && ++s < y);
                    return a
                }

                function p(t) {
                    for (var n = 0, i = 1, o = b - 1; i != o && T[i] <= t; ++i) n += x;
                    --i;
                    var a = (t - T[i]) / (T[i + 1] - T[i]),
                        s = n + a * x,
                        l = u(s, e, r);
                    return l >= m ? c(t, s) : 0 == l ? s : f(t, n, n + x)
                }

                function h() {
                    k = !0, (e != n || r != i) && d()
                }
                var g = 4,
                    m = .001,
                    v = 1e-7,
                    y = 10,
                    b = 11,
                    x = 1 / (b - 1),
                    w = "Float32Array" in t;
                if (4 !== arguments.length) return !1;
                for (var C = 0; 4 > C; ++C)
                    if ("number" != typeof arguments[C] || isNaN(arguments[C]) || !isFinite(arguments[C])) return !1;
                e = Math.min(e, 1), r = Math.min(r, 1), e = Math.max(e, 0), r = Math.max(r, 0);
                var T = w ? new Float32Array(b) : new Array(b),
                    k = !1,
                    S = function(t) {
                        return k || h(), e === n && r === i ? t : 0 === t ? 0 : 1 === t ? 1 : l(p(t), n, i)
                    };
                S.getControlPoints = function() {
                    return [{
                        x: e,
                        y: n
                    }, {
                        x: r,
                        y: i
                    }]
                };
                var j = "generateBezier(" + [e, n, r, i] + ")";
                return S.toString = function() {
                    return j
                }, S
            }

            function u(e, t) {
                var n = e;
                return g.isString(e) ? b.Easings[e] || (n = !1) : n = g.isArray(e) && 1 === e.length ? s.apply(null, e) : g.isArray(e) && 2 === e.length ? x.apply(null, e.concat([t])) : !(!g.isArray(e) || 4 !== e.length) && l.apply(null, e), n === !1 && (n = b.Easings[b.defaults.easing] ? b.defaults.easing : y), n
            }

            function c(e) {
                if (e) {
                    var t = (new Date).getTime(),
                        n = b.State.calls.length;
                    n > 1e4 && (b.State.calls = i(b.State.calls));
                    for (var o = 0; n > o; o++)
                        if (b.State.calls[o]) {
                            var s = b.State.calls[o],
                                l = s[0],
                                u = s[2],
                                p = s[3],
                                h = !!p,
                                m = null;
                            p || (p = b.State.calls[o][3] = t - 16);
                            for (var v = Math.min((t - p) / u.duration, 1), y = 0, x = l.length; x > y; y++) {
                                var C = l[y],
                                    k = C.element;
                                if (a(k)) {
                                    var S = !1;
                                    if (u.display !== r && null !== u.display && "none" !== u.display) {
                                        if ("flex" === u.display) {
                                            var j = ["-webkit-box", "-moz-box", "-ms-flexbox", "-webkit-flex"];
                                            f.each(j, function(e, t) {
                                                w.setPropertyValue(k, "display", t)
                                            })
                                        }
                                        w.setPropertyValue(k, "display", u.display)
                                    }
                                    u.visibility !== r && "hidden" !== u.visibility && w.setPropertyValue(k, "visibility", u.visibility);
                                    for (var E in C)
                                        if ("element" !== E) {
                                            var N, A = C[E],
                                                P = g.isString(A.easing) ? b.Easings[A.easing] : A.easing;
                                            if (1 === v) N = A.endValue;
                                            else {
                                                var H = A.endValue - A.startValue;
                                                if (N = A.startValue + H * P(v, u, H), !h && N === A.currentValue) continue
                                            }
                                            if (A.currentValue = N, "tween" === E) m = N;
                                            else {
                                                if (w.Hooks.registered[E]) {
                                                    var L = w.Hooks.getRoot(E),
                                                        D = a(k).rootPropertyValueCache[L];
                                                    D && (A.rootPropertyValue = D)
                                                }
                                                var M = w.setPropertyValue(k, E, A.currentValue + (0 === parseFloat(N) ? "" : A.unitType), A.rootPropertyValue, A.scrollData);
                                                w.Hooks.registered[E] && (a(k).rootPropertyValueCache[L] = w.Normalizations.registered[L] ? w.Normalizations.registered[L]("extract", null, M[1]) : M[1]), "transform" === M[0] && (S = !0)
                                            }
                                        }
                                    u.mobileHA && a(k).transformCache.translate3d === r && (a(k).transformCache.translate3d = "(0px, 0px, 0px)", S = !0), S && w.flushTransformCache(k)
                                }
                            }
                            u.display !== r && "none" !== u.display && (b.State.calls[o][2].display = !1), u.visibility !== r && "hidden" !== u.visibility && (b.State.calls[o][2].visibility = !1), u.progress && u.progress.call(s[1], s[1], v, Math.max(0, p + u.duration - t), p, m), 1 === v && d(o)
                        }
                }
                b.State.isTicking && T(c)
            }

            function d(e, t) {
                if (!b.State.calls[e]) return !1;
                for (var n = b.State.calls[e][0], i = b.State.calls[e][1], o = b.State.calls[e][2], s = b.State.calls[e][4], l = !1, u = 0, c = n.length; c > u; u++) {
                    var d = n[u].element;
                    if (t || o.loop || ("none" === o.display && w.setPropertyValue(d, "display", o.display), "hidden" === o.visibility && w.setPropertyValue(d, "visibility", o.visibility)), o.loop !== !0 && (f.queue(d)[1] === r || !/\.velocityQueueEntryFlag/i.test(f.queue(d)[1])) && a(d)) {
                        a(d).isAnimating = !1, a(d).rootPropertyValueCache = {};
                        var p = !1;
                        f.each(w.Lists.transforms3D, function(e, t) {
                            var n = /^scale/.test(t) ? 1 : 0,
                                i = a(d).transformCache[t];
                            a(d).transformCache[t] !== r && new RegExp("^\\(" + n + "[^.]").test(i) && (p = !0, delete a(d).transformCache[t])
                        }), o.mobileHA && (p = !0, delete a(d).transformCache.translate3d), p && w.flushTransformCache(d), w.Values.removeClass(d, "velocity-animating")
                    }
                    if (!t && o.complete && !o.loop && u === c - 1) try {
                        o.complete.call(i, i)
                    } catch (e) {
                        setTimeout(function() {
                            throw e
                        }, 1)
                    }
                    s && o.loop !== !0 && s(i), a(d) && o.loop === !0 && !t && (f.each(a(d).tweensContainer, function(e, t) {
                        /^rotate/.test(e) && 360 === parseFloat(t.endValue) && (t.endValue = 0, t.startValue = 360), /^backgroundPosition/.test(e) && 100 === parseFloat(t.endValue) && "%" === t.unitType && (t.endValue = 0, t.startValue = 100)
                    }), b(d, "reverse", {
                        loop: !0,
                        delay: o.delay
                    })), o.queue !== !1 && f.dequeue(d, o.queue)
                }
                b.State.calls[e] = !1;
                for (var h = 0, g = b.State.calls.length; g > h; h++)
                    if (b.State.calls[h] !== !1) {
                        l = !0;
                        break
                    }
                l === !1 && (b.State.isTicking = !1, delete b.State.calls, b.State.calls = [])
            }
            var f, p = function() {
                    if (n.documentMode) return n.documentMode;
                    for (var e = 7; e > 4; e--) {
                        var t = n.createElement("div");
                        if (t.innerHTML = "<!--[if IE " + e + "]><span></span><![endif]-->", t.getElementsByTagName("span").length) return t = null, e
                    }
                    return r
                }(),
                h = function() {
                    var e = 0;
                    return t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || function(t) {
                        var n, r = (new Date).getTime();
                        return n = Math.max(0, 16 - (r - e)), e = r + n, setTimeout(function() {
                            t(r + n)
                        }, n)
                    }
                }(),
                g = {
                    isString: function(e) {
                        return "string" == typeof e
                    },
                    isArray: Array.isArray || function(e) {
                        return "[object Array]" === Object.prototype.toString.call(e)
                    },
                    isFunction: function(e) {
                        return "[object Function]" === Object.prototype.toString.call(e)
                    },
                    isNode: function(e) {
                        return e && e.nodeType
                    },
                    isNodeList: function(e) {
                        return "object" == typeof e && /^\[object (HTMLCollection|NodeList|Object)\]$/.test(Object.prototype.toString.call(e)) && e.length !== r && (0 === e.length || "object" == typeof e[0] && e[0].nodeType > 0)
                    },
                    isWrapped: function(e) {
                        return e && (e.jquery || t.Zepto && t.Zepto.zepto.isZ(e))
                    },
                    isSVG: function(e) {
                        return t.SVGElement && e instanceof t.SVGElement
                    },
                    isEmptyObject: function(e) {
                        for (var t in e) return !1;
                        return !0
                    }
                },
                m = !1;
            if (e.fn && e.fn.jquery ? (f = e, m = !0) : f = t.Velocity.Utilities, 8 >= p && !m) throw new Error("Velocity: IE8 and below require jQuery to be loaded before Velocity.");
            if (7 >= p) return void(jQuery.fn.velocity = jQuery.fn.animate);
            var v = 400,
                y = "swing",
                b = {
                    State: {
                        isMobile: /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent),
                        isAndroid: /Android/i.test(navigator.userAgent),
                        isGingerbread: /Android 2\.3\.[3-7]/i.test(navigator.userAgent),
                        isChrome: t.chrome,
                        isFirefox: /Firefox/i.test(navigator.userAgent),
                        prefixElement: n.createElement("div"),
                        prefixMatches: {},
                        scrollAnchor: null,
                        scrollPropertyLeft: null,
                        scrollPropertyTop: null,
                        isTicking: !1,
                        calls: []
                    },
                    CSS: {},
                    Utilities: f,
                    Redirects: {},
                    Easings: {},
                    Promise: t.Promise,
                    defaults: {
                        queue: "",
                        duration: v,
                        easing: y,
                        begin: r,
                        complete: r,
                        progress: r,
                        display: r,
                        visibility: r,
                        loop: !1,
                        delay: !1,
                        mobileHA: !0,
                        _cacheValues: !0
                    },
                    init: function(e) {
                        f.data(e, "velocity", {
                            isSVG: g.isSVG(e),
                            isAnimating: !1,
                            computedStyle: null,
                            tweensContainer: null,
                            rootPropertyValueCache: {},
                            transformCache: {}
                        })
                    },
                    hook: null,
                    mock: !1,
                    version: {
                        major: 1,
                        minor: 2,
                        patch: 2
                    },
                    debug: !1
                };
            t.pageYOffset !== r ? (b.State.scrollAnchor = t, b.State.scrollPropertyLeft = "pageXOffset", b.State.scrollPropertyTop = "pageYOffset") : (b.State.scrollAnchor = n.documentElement || n.body.parentNode || n.body, b.State.scrollPropertyLeft = "scrollLeft", b.State.scrollPropertyTop = "scrollTop");
            var x = function() {
                function e(e) {
                    return -e.tension * e.x - e.friction * e.v
                }

                function t(t, n, r) {
                    var i = {
                        x: t.x + r.dx * n,
                        v: t.v + r.dv * n,
                        tension: t.tension,
                        friction: t.friction
                    };
                    return {
                        dx: i.v,
                        dv: e(i)
                    }
                }

                function n(n, r) {
                    var i = {
                            dx: n.v,
                            dv: e(n)
                        },
                        o = t(n, .5 * r, i),
                        a = t(n, .5 * r, o),
                        s = t(n, r, a),
                        l = 1 / 6 * (i.dx + 2 * (o.dx + a.dx) + s.dx),
                        u = 1 / 6 * (i.dv + 2 * (o.dv + a.dv) + s.dv);
                    return n.x = n.x + l * r, n.v = n.v + u * r, n
                }
                return function e(t, r, i) {
                    var o, a, s, l = {
                            x: -1,
                            v: 0,
                            tension: null,
                            friction: null
                        },
                        u = [0],
                        c = 0;
                    for (t = parseFloat(t) || 500, r = parseFloat(r) || 20, i = i || null, l.tension = t, l.friction = r, o = null !== i, o ? (c = e(t, r), a = c / i * .016) : a = .016; s = n(s || l, a), u.push(1 + s.x), c += 16, Math.abs(s.x) > 1e-4 && Math.abs(s.v) > 1e-4;);
                    return o ? function(e) {
                        return u[e * (u.length - 1) | 0]
                    } : c
                }
            }();
            b.Easings = {
                linear: function(e) {
                    return e
                },
                swing: function(e) {
                    return .5 - Math.cos(e * Math.PI) / 2
                },
                spring: function(e) {
                    return 1 - Math.cos(4.5 * e * Math.PI) * Math.exp(6 * -e)
                }
            }, f.each([
                ["ease", [.25, .1, .25, 1]],
                ["ease-in", [.42, 0, 1, 1]],
                ["ease-out", [0, 0, .58, 1]],
                ["ease-in-out", [.42, 0, .58, 1]],
                ["easeInSine", [.47, 0, .745, .715]],
                ["easeOutSine", [.39, .575, .565, 1]],
                ["easeInOutSine", [.445, .05, .55, .95]],
                ["easeInQuad", [.55, .085, .68, .53]],
                ["easeOutQuad", [.25, .46, .45, .94]],
                ["easeInOutQuad", [.455, .03, .515, .955]],
                ["easeInCubic", [.55, .055, .675, .19]],
                ["easeOutCubic", [.215, .61, .355, 1]],
                ["easeInOutCubic", [.645, .045, .355, 1]],
                ["easeInQuart", [.895, .03, .685, .22]],
                ["easeOutQuart", [.165, .84, .44, 1]],
                ["easeInOutQuart", [.77, 0, .175, 1]],
                ["easeInQuint", [.755, .05, .855, .06]],
                ["easeOutQuint", [.23, 1, .32, 1]],
                ["easeInOutQuint", [.86, 0, .07, 1]],
                ["easeInExpo", [.95, .05, .795, .035]],
                ["easeOutExpo", [.19, 1, .22, 1]],
                ["easeInOutExpo", [1, 0, 0, 1]],
                ["easeInCirc", [.6, .04, .98, .335]],
                ["easeOutCirc", [.075, .82, .165, 1]],
                ["easeInOutCirc", [.785, .135, .15, .86]]
            ], function(e, t) {
                b.Easings[t[0]] = l.apply(null, t[1])
            });
            var w = b.CSS = {
                RegEx: {
                    isHex: /^#([A-f\d]{3}){1,2}$/i,
                    valueUnwrap: /^[A-z]+\((.*)\)$/i,
                    wrappedValueAlreadyExtracted: /[0-9.]+ [0-9.]+ [0-9.]+( [0-9.]+)?/,
                    valueSplit: /([A-z]+\(.+\))|(([A-z0-9#-.]+?)(?=\s|$))/gi
                },
                Lists: {
                    colors: ["fill", "stroke", "stopColor", "color", "backgroundColor", "borderColor", "borderTopColor", "borderRightColor", "borderBottomColor", "borderLeftColor", "outlineColor"],
                    transformsBase: ["translateX", "translateY", "scale", "scaleX", "scaleY", "skewX", "skewY", "rotateZ"],
                    transforms3D: ["transformPerspective", "translateZ", "scaleZ", "rotateX", "rotateY"]
                },
                Hooks: {
                    templates: {
                        textShadow: ["Color X Y Blur", "black 0px 0px 0px"],
                        boxShadow: ["Color X Y Blur Spread", "black 0px 0px 0px 0px"],
                        clip: ["Top Right Bottom Left", "0px 0px 0px 0px"],
                        backgroundPosition: ["X Y", "0% 0%"],
                        transformOrigin: ["X Y Z", "50% 50% 0px"],
                        perspectiveOrigin: ["X Y", "50% 50%"]
                    },
                    registered: {},
                    register: function() {
                        for (var e = 0; e < w.Lists.colors.length; e++) {
                            var t = "color" === w.Lists.colors[e] ? "0 0 0 1" : "255 255 255 1";
                            w.Hooks.templates[w.Lists.colors[e]] = ["Red Green Blue Alpha", t]
                        }
                        var n, r, i;
                        if (p)
                            for (n in w.Hooks.templates) {
                                r = w.Hooks.templates[n], i = r[0].split(" ");
                                var o = r[1].match(w.RegEx.valueSplit);
                                "Color" === i[0] && (i.push(i.shift()), o.push(o.shift()), w.Hooks.templates[n] = [i.join(" "), o.join(" ")])
                            }
                        for (n in w.Hooks.templates) {
                            r = w.Hooks.templates[n], i = r[0].split(" ");
                            for (var e in i) {
                                var a = n + i[e],
                                    s = e;
                                w.Hooks.registered[a] = [n, s]
                            }
                        }
                    },
                    getRoot: function(e) {
                        var t = w.Hooks.registered[e];
                        return t ? t[0] : e
                    },
                    cleanRootPropertyValue: function(e, t) {
                        return w.RegEx.valueUnwrap.test(t) && (t = t.match(w.RegEx.valueUnwrap)[1]), w.Values.isCSSNullValue(t) && (t = w.Hooks.templates[e][1]), t
                    },
                    extractValue: function(e, t) {
                        var n = w.Hooks.registered[e];
                        if (n) {
                            var r = n[0],
                                i = n[1];
                            return t = w.Hooks.cleanRootPropertyValue(r, t), t.toString().match(w.RegEx.valueSplit)[i]
                        }
                        return t
                    },
                    injectValue: function(e, t, n) {
                        var r = w.Hooks.registered[e];
                        if (r) {
                            var i, o = r[0],
                                a = r[1];
                            return n = w.Hooks.cleanRootPropertyValue(o, n), i = n.toString().match(w.RegEx.valueSplit), i[a] = t, i.join(" ")
                        }
                        return n
                    }
                },
                Normalizations: {
                    registered: {
                        clip: function(e, t, n) {
                            switch (e) {
                                case "name":
                                    return "clip";
                                case "extract":
                                    var r;
                                    return w.RegEx.wrappedValueAlreadyExtracted.test(n) ? r = n : (r = n.toString().match(w.RegEx.valueUnwrap), r = r ? r[1].replace(/,(\s+)?/g, " ") : n), r;
                                case "inject":
                                    return "rect(" + n + ")"
                            }
                        },
                        blur: function(e, t, n) {
                            switch (e) {
                                case "name":
                                    return b.State.isFirefox ? "filter" : "-webkit-filter";
                                case "extract":
                                    var r = parseFloat(n);
                                    if (!r && 0 !== r) {
                                        var i = n.toString().match(/blur\(([0-9]+[A-z]+)\)/i);
                                        r = i ? i[1] : 0
                                    }
                                    return r;
                                case "inject":
                                    return parseFloat(n) ? "blur(" + n + ")" : "none"
                            }
                        },
                        opacity: function(e, t, n) {
                            if (8 >= p) switch (e) {
                                case "name":
                                    return "filter";
                                case "extract":
                                    var r = n.toString().match(/alpha\(opacity=(.*)\)/i);
                                    return n = r ? r[1] / 100 : 1;
                                case "inject":
                                    return t.style.zoom = 1, parseFloat(n) >= 1 ? "" : "alpha(opacity=" + parseInt(100 * parseFloat(n), 10) + ")"
                            } else switch (e) {
                                case "name":
                                    return "opacity";
                                case "extract":
                                    return n;
                                case "inject":
                                    return n
                            }
                        }
                    },
                    register: function() {
                        9 >= p || b.State.isGingerbread || (w.Lists.transformsBase = w.Lists.transformsBase.concat(w.Lists.transforms3D));
                        for (var e = 0; e < w.Lists.transformsBase.length; e++) ! function() {
                            var t = w.Lists.transformsBase[e];
                            w.Normalizations.registered[t] = function(e, n, i) {
                                switch (e) {
                                    case "name":
                                        return "transform";
                                    case "extract":
                                        return a(n) === r || a(n).transformCache[t] === r ? /^scale/i.test(t) ? 1 : 0 : a(n).transformCache[t].replace(/[()]/g, "");
                                    case "inject":
                                        var o = !1;
                                        switch (t.substr(0, t.length - 1)) {
                                            case "translate":
                                                o = !/(%|px|em|rem|vw|vh|\d)$/i.test(i);
                                                break;
                                            case "scal":
                                            case "scale":
                                                b.State.isAndroid && a(n).transformCache[t] === r && 1 > i && (i = 1), o = !/(\d)$/i.test(i);
                                                break;
                                            case "skew":
                                                o = !/(deg|\d)$/i.test(i);
                                                break;
                                            case "rotate":
                                                o = !/(deg|\d)$/i.test(i)
                                        }
                                        return o || (a(n).transformCache[t] = "(" + i + ")"), a(n).transformCache[t]
                                }
                            }
                        }();
                        for (var e = 0; e < w.Lists.colors.length; e++) ! function() {
                            var t = w.Lists.colors[e];
                            w.Normalizations.registered[t] = function(e, n, i) {
                                switch (e) {
                                    case "name":
                                        return t;
                                    case "extract":
                                        var o;
                                        if (w.RegEx.wrappedValueAlreadyExtracted.test(i)) o = i;
                                        else {
                                            var a, s = {
                                                black: "rgb(0, 0, 0)",
                                                blue: "rgb(0, 0, 255)",
                                                gray: "rgb(128, 128, 128)",
                                                green: "rgb(0, 128, 0)",
                                                red: "rgb(255, 0, 0)",
                                                white: "rgb(255, 255, 255)"
                                            };
                                            /^[A-z]+$/i.test(i) ? a = s[i] !== r ? s[i] : s.black : w.RegEx.isHex.test(i) ? a = "rgb(" + w.Values.hexToRgb(i).join(" ") + ")" : /^rgba?\(/i.test(i) || (a = s.black), o = (a || i).toString().match(w.RegEx.valueUnwrap)[1].replace(/,(\s+)?/g, " ")
                                        }
                                        return 8 >= p || 3 !== o.split(" ").length || (o += " 1"), o;
                                    case "inject":
                                        return 8 >= p ? 4 === i.split(" ").length && (i = i.split(/\s+/).slice(0, 3).join(" ")) : 3 === i.split(" ").length && (i += " 1"), (8 >= p ? "rgb" : "rgba") + "(" + i.replace(/\s+/g, ",").replace(/\.(\d)+(?=,)/g, "") + ")"
                                }
                            }
                        }()
                    }
                },
                Names: {
                    camelCase: function(e) {
                        return e.replace(/-(\w)/g, function(e, t) {
                            return t.toUpperCase()
                        })
                    },
                    SVGAttribute: function(e) {
                        var t = "width|height|x|y|cx|cy|r|rx|ry|x1|x2|y1|y2";
                        return (p || b.State.isAndroid && !b.State.isChrome) && (t += "|transform"), new RegExp("^(" + t + ")$", "i").test(e)
                    },
                    prefixCheck: function(e) {
                        if (b.State.prefixMatches[e]) return [b.State.prefixMatches[e], !0];
                        for (var t = ["", "Webkit", "Moz", "ms", "O"], n = 0, r = t.length; r > n; n++) {
                            var i;
                            if (i = 0 === n ? e : t[n] + e.replace(/^\w/, function(e) {
                                    return e.toUpperCase()
                                }), g.isString(b.State.prefixElement.style[i])) return b.State.prefixMatches[e] = i, [i, !0]
                        }
                        return [e, !1]
                    }
                },
                Values: {
                    hexToRgb: function(e) {
                        var t, n = /^#?([a-f\d]{2})([a-f\d]{2})([a-f\d]{2})$/i;
                        return e = e.replace(/^#?([a-f\d])([a-f\d])([a-f\d])$/i, function(e, t, n, r) {
                            return t + t + n + n + r + r
                        }), t = n.exec(e), t ? [parseInt(t[1], 16), parseInt(t[2], 16), parseInt(t[3], 16)] : [0, 0, 0]
                    },
                    isCSSNullValue: function(e) {
                        return 0 == e || /^(none|auto|transparent|(rgba\(0, ?0, ?0, ?0\)))$/i.test(e)
                    },
                    getUnitType: function(e) {
                        return /^(rotate|skew)/i.test(e) ? "deg" : /(^(scale|scaleX|scaleY|scaleZ|alpha|flexGrow|flexHeight|zIndex|fontWeight)$)|((opacity|red|green|blue|alpha)$)/i.test(e) ? "" : "px"
                    },
                    getDisplayType: function(e) {
                        var t = e && e.tagName.toString().toLowerCase();
                        return /^(b|big|i|small|tt|abbr|acronym|cite|code|dfn|em|kbd|strong|samp|var|a|bdo|br|img|map|object|q|script|span|sub|sup|button|input|label|select|textarea)$/i.test(t) ? "inline" : /^(li)$/i.test(t) ? "list-item" : /^(tr)$/i.test(t) ? "table-row" : /^(table)$/i.test(t) ? "table" : /^(tbody)$/i.test(t) ? "table-row-group" : "block"
                    },
                    addClass: function(e, t) {
                        e.classList ? e.classList.add(t) : e.className += (e.className.length ? " " : "") + t
                    },
                    removeClass: function(e, t) {
                        e.classList ? e.classList.remove(t) : e.className = e.className.toString().replace(new RegExp("(^|\\s)" + t.split(" ").join("|") + "(\\s|$)", "gi"), " ")
                    }
                },
                getPropertyValue: function(e, n, i, o) {
                    function s(e, n) {
                        function i() {
                            u && w.setPropertyValue(e, "display", "none")
                        }
                        var l = 0;
                        if (8 >= p) l = f.css(e, n);
                        else {
                            var u = !1;
                            if (/^(width|height)$/.test(n) && 0 === w.getPropertyValue(e, "display") && (u = !0, w.setPropertyValue(e, "display", w.Values.getDisplayType(e))), !o) {
                                if ("height" === n && "border-box" !== w.getPropertyValue(e, "boxSizing").toString().toLowerCase()) {
                                    var c = e.offsetHeight - (parseFloat(w.getPropertyValue(e, "borderTopWidth")) || 0) - (parseFloat(w.getPropertyValue(e, "borderBottomWidth")) || 0) - (parseFloat(w.getPropertyValue(e, "paddingTop")) || 0) - (parseFloat(w.getPropertyValue(e, "paddingBottom")) || 0);
                                    return i(), c
                                }
                                if ("width" === n && "border-box" !== w.getPropertyValue(e, "boxSizing").toString().toLowerCase()) {
                                    var d = e.offsetWidth - (parseFloat(w.getPropertyValue(e, "borderLeftWidth")) || 0) - (parseFloat(w.getPropertyValue(e, "borderRightWidth")) || 0) - (parseFloat(w.getPropertyValue(e, "paddingLeft")) || 0) - (parseFloat(w.getPropertyValue(e, "paddingRight")) || 0);
                                    return i(), d
                                }
                            }
                            var h;
                            h = a(e) === r ? t.getComputedStyle(e, null) : a(e).computedStyle ? a(e).computedStyle : a(e).computedStyle = t.getComputedStyle(e, null), "borderColor" === n && (n = "borderTopColor"), l = 9 === p && "filter" === n ? h.getPropertyValue(n) : h[n], ("" === l || null === l) && (l = e.style[n]), i()
                        }
                        if ("auto" === l && /^(top|right|bottom|left)$/i.test(n)) {
                            var g = s(e, "position");
                            ("fixed" === g || "absolute" === g && /top|left/i.test(n)) && (l = f(e).position()[n] + "px")
                        }
                        return l
                    }
                    var l;
                    if (w.Hooks.registered[n]) {
                        var u = n,
                            c = w.Hooks.getRoot(u);
                        i === r && (i = w.getPropertyValue(e, w.Names.prefixCheck(c)[0])), w.Normalizations.registered[c] && (i = w.Normalizations.registered[c]("extract", e, i)), l = w.Hooks.extractValue(u, i)
                    } else if (w.Normalizations.registered[n]) {
                        var d, h;
                        d = w.Normalizations.registered[n]("name", e), "transform" !== d && (h = s(e, w.Names.prefixCheck(d)[0]), w.Values.isCSSNullValue(h) && w.Hooks.templates[n] && (h = w.Hooks.templates[n][1])), l = w.Normalizations.registered[n]("extract", e, h)
                    }
                    if (!/^[\d-]/.test(l))
                        if (a(e) && a(e).isSVG && w.Names.SVGAttribute(n))
                            if (/^(height|width)$/i.test(n)) try {
                                l = e.getBBox()[n]
                            } catch (e) {
                                l = 0
                            } else l = e.getAttribute(n);
                            else l = s(e, w.Names.prefixCheck(n)[0]);
                    return w.Values.isCSSNullValue(l) && (l = 0), b.debug >= 2 && console.log("Get " + n + ": " + l), l
                },
                setPropertyValue: function(e, n, r, i, o) {
                    var s = n;
                    if ("scroll" === n) o.container ? o.container["scroll" + o.direction] = r : "Left" === o.direction ? t.scrollTo(r, o.alternateValue) : t.scrollTo(o.alternateValue, r);
                    else if (w.Normalizations.registered[n] && "transform" === w.Normalizations.registered[n]("name", e)) w.Normalizations.registered[n]("inject", e, r), s = "transform", r = a(e).transformCache[n];
                    else {
                        if (w.Hooks.registered[n]) {
                            var l = n,
                                u = w.Hooks.getRoot(n);
                            i = i || w.getPropertyValue(e, u), r = w.Hooks.injectValue(l, r, i), n = u
                        }
                        if (w.Normalizations.registered[n] && (r = w.Normalizations.registered[n]("inject", e, r), n = w.Normalizations.registered[n]("name", e)), s = w.Names.prefixCheck(n)[0], 8 >= p) try {
                            e.style[s] = r
                        } catch (e) {
                            b.debug && console.log("Browser does not support [" + r + "] for [" + s + "]")
                        } else a(e) && a(e).isSVG && w.Names.SVGAttribute(n) ? e.setAttribute(n, r) : e.style[s] = r;
                        b.debug >= 2 && console.log("Set " + n + " (" + s + "): " + r)
                    }
                    return [s, r]
                },
                flushTransformCache: function(e) {
                    function t(t) {
                        return parseFloat(w.getPropertyValue(e, t))
                    }
                    var n = "";
                    if ((p || b.State.isAndroid && !b.State.isChrome) && a(e).isSVG) {
                        var r = {
                            translate: [t("translateX"), t("translateY")],
                            skewX: [t("skewX")],
                            skewY: [t("skewY")],
                            scale: 1 !== t("scale") ? [t("scale"), t("scale")] : [t("scaleX"), t("scaleY")],
                            rotate: [t("rotateZ"), 0, 0]
                        };
                        f.each(a(e).transformCache, function(e) {
                            /^translate/i.test(e) ? e = "translate" : /^scale/i.test(e) ? e = "scale" : /^rotate/i.test(e) && (e = "rotate"), r[e] && (n += e + "(" + r[e].join(" ") + ") ", delete r[e])
                        })
                    } else {
                        var i, o;
                        f.each(a(e).transformCache, function(t) {
                            return i = a(e).transformCache[t], "transformPerspective" === t ? (o = i, !0) : (9 === p && "rotateZ" === t && (t = "rotate"), void(n += t + i + " "))
                        }), o && (n = "perspective" + o + " " + n)
                    }
                    w.setPropertyValue(e, "transform", n)
                }
            };
            w.Hooks.register(), w.Normalizations.register(), b.hook = function(e, t, n) {
                var i = r;
                return e = o(e), f.each(e, function(e, o) {
                    if (a(o) === r && b.init(o), n === r) i === r && (i = b.CSS.getPropertyValue(o, t));
                    else {
                        var s = b.CSS.setPropertyValue(o, t, n);
                        "transform" === s[0] && b.CSS.flushTransformCache(o), i = s
                    }
                }), i
            };
            var C = function() {
                function e() {
                    return s ? E.promise || null : l
                }

                function i() {
                    function e(e) {
                        function d(e, t) {
                            var n = r,
                                i = r,
                                a = r;
                            return g.isArray(e) ? (n = e[0], !g.isArray(e[1]) && /^[\d-]/.test(e[1]) || g.isFunction(e[1]) || w.RegEx.isHex.test(e[1]) ? a = e[1] : (g.isString(e[1]) && !w.RegEx.isHex.test(e[1]) || g.isArray(e[1])) && (i = t ? e[1] : u(e[1], s.duration), e[2] !== r && (a = e[2]))) : n = e, t || (i = i || s.easing), g.isFunction(n) && (n = n.call(o, k, T)), g.isFunction(a) && (a = a.call(o, k, T)), [n || 0, i, a]
                        }

                        function p(e, t) {
                            var n, r;
                            return r = (t || "0").toString().toLowerCase().replace(/[%A-z]+$/, function(e) {
                                return n = e, ""
                            }), n || (n = w.Values.getUnitType(e)), [r, n]
                        }
                        if (s.begin && 0 === k) try {
                            s.begin.call(h, h)
                        } catch (e) {
                            setTimeout(function() {
                                throw e
                            }, 1)
                        }
                        if ("scroll" === N) {
                            var v, x, C, S = /^x$/i.test(s.axis) ? "Left" : "Top",
                                j = parseFloat(s.offset) || 0;
                            s.container ? g.isWrapped(s.container) || g.isNode(s.container) ? (s.container = s.container[0] || s.container, v = s.container["scroll" + S], C = v + f(o).position()[S.toLowerCase()] + j) : s.container = null : (v = b.State.scrollAnchor[b.State["scrollProperty" + S]], x = b.State.scrollAnchor[b.State["scrollProperty" + ("Left" === S ? "Top" : "Left")]], C = f(o).offset()[S.toLowerCase()] + j), l = {
                                scroll: {
                                    rootPropertyValue: !1,
                                    startValue: v,
                                    currentValue: v,
                                    endValue: C,
                                    unitType: "",
                                    easing: s.easing,
                                    scrollData: {
                                        container: s.container,
                                        direction: S,
                                        alternateValue: x
                                    }
                                },
                                element: o
                            }, b.debug && console.log("tweensContainer (scroll): ", l.scroll, o)
                        } else if ("reverse" === N) {
                            if (!a(o).tweensContainer) return void f.dequeue(o, s.queue);
                            "none" === a(o).opts.display && (a(o).opts.display = "auto"), "hidden" === a(o).opts.visibility && (a(o).opts.visibility = "visible"), a(o).opts.loop = !1, a(o).opts.begin = null, a(o).opts.complete = null, y.easing || delete s.easing, y.duration || delete s.duration, s = f.extend({}, a(o).opts, s);
                            var A = f.extend(!0, {}, a(o).tweensContainer);
                            for (var P in A)
                                if ("element" !== P) {
                                    var H = A[P].startValue;
                                    A[P].startValue = A[P].currentValue = A[P].endValue, A[P].endValue = H, g.isEmptyObject(y) || (A[P].easing = s.easing), b.debug && console.log("reverse tweensContainer (" + P + "): " + JSON.stringify(A[P]), o)
                                }
                            l = A
                        } else if ("start" === N) {
                            var A;
                            a(o).tweensContainer && a(o).isAnimating === !0 && (A = a(o).tweensContainer), f.each(m, function(e, t) {
                                if (RegExp("^" + w.Lists.colors.join("$|^") + "$").test(e)) {
                                    var n = d(t, !0),
                                        i = n[0],
                                        o = n[1],
                                        a = n[2];
                                    if (w.RegEx.isHex.test(i)) {
                                        for (var s = ["Red", "Green", "Blue"], l = w.Values.hexToRgb(i), u = a ? w.Values.hexToRgb(a) : r, c = 0; c < s.length; c++) {
                                            var f = [l[c]];
                                            o && f.push(o), u !== r && f.push(u[c]), m[e + s[c]] = f
                                        }
                                        delete m[e]
                                    }
                                }
                            });
                            for (var L in m) {
                                var D = d(m[L]),
                                    F = D[0],
                                    B = D[1],
                                    $ = D[2];
                                L = w.Names.camelCase(L);
                                var V = w.Hooks.getRoot(L),
                                    q = !1;
                                if (a(o).isSVG || "tween" === V || w.Names.prefixCheck(V)[1] !== !1 || w.Normalizations.registered[V] !== r) {
                                    (s.display !== r && null !== s.display && "none" !== s.display || s.visibility !== r && "hidden" !== s.visibility) && /opacity|filter/.test(L) && !$ && 0 !== F && ($ = 0), s._cacheValues && A && A[L] ? ($ === r && ($ = A[L].endValue + A[L].unitType), q = a(o).rootPropertyValueCache[V]) : w.Hooks.registered[L] ? $ === r ? (q = w.getPropertyValue(o, V), $ = w.getPropertyValue(o, L, q)) : q = w.Hooks.templates[V][1] : $ === r && ($ = w.getPropertyValue(o, L));
                                    var _, R, z, I = !1;
                                    if (_ = p(L, $), $ = _[0], z = _[1], _ = p(L, F), F = _[0].replace(/^([+-\/*])=/, function(e, t) {
                                            return I = t, ""
                                        }), R = _[1], $ = parseFloat($) || 0, F = parseFloat(F) || 0, "%" === R && (/^(fontSize|lineHeight)$/.test(L) ? (F /= 100, R = "em") : /^scale/.test(L) ? (F /= 100, R = "") : /(Red|Green|Blue)$/i.test(L) && (F = F / 100 * 255, R = "")), /[\/*]/.test(I)) R = z;
                                    else if (z !== R && 0 !== $)
                                        if (0 === F) R = z;
                                        else {
                                            i = i || function() {
                                                var e = {
                                                        myParent: o.parentNode || n.body,
                                                        position: w.getPropertyValue(o, "position"),
                                                        fontSize: w.getPropertyValue(o, "fontSize")
                                                    },
                                                    r = e.position === M.lastPosition && e.myParent === M.lastParent,
                                                    i = e.fontSize === M.lastFontSize;
                                                M.lastParent = e.myParent, M.lastPosition = e.position, M.lastFontSize = e.fontSize;
                                                var s = 100,
                                                    l = {};
                                                if (i && r) l.emToPx = M.lastEmToPx, l.percentToPxWidth = M.lastPercentToPxWidth, l.percentToPxHeight = M.lastPercentToPxHeight;
                                                else {
                                                    var u = a(o).isSVG ? n.createElementNS("http://www.w3.org/2000/svg", "rect") : n.createElement("div");
                                                    b.init(u), e.myParent.appendChild(u), f.each(["overflow", "overflowX", "overflowY"], function(e, t) {
                                                        b.CSS.setPropertyValue(u, t, "hidden")
                                                    }), b.CSS.setPropertyValue(u, "position", e.position), b.CSS.setPropertyValue(u, "fontSize", e.fontSize), b.CSS.setPropertyValue(u, "boxSizing", "content-box"), f.each(["minWidth", "maxWidth", "width", "minHeight", "maxHeight", "height"], function(e, t) {
                                                        b.CSS.setPropertyValue(u, t, s + "%")
                                                    }), b.CSS.setPropertyValue(u, "paddingLeft", s + "em"), l.percentToPxWidth = M.lastPercentToPxWidth = (parseFloat(w.getPropertyValue(u, "width", null, !0)) || 1) / s, l.percentToPxHeight = M.lastPercentToPxHeight = (parseFloat(w.getPropertyValue(u, "height", null, !0)) || 1) / s, l.emToPx = M.lastEmToPx = (parseFloat(w.getPropertyValue(u, "paddingLeft")) || 1) / s, e.myParent.removeChild(u)
                                                }
                                                return null === M.remToPx && (M.remToPx = parseFloat(w.getPropertyValue(n.body, "fontSize")) || 16), null === M.vwToPx && (M.vwToPx = parseFloat(t.innerWidth) / 100, M.vhToPx = parseFloat(t.innerHeight) / 100), l.remToPx = M.remToPx, l.vwToPx = M.vwToPx, l.vhToPx = M.vhToPx, b.debug >= 1 && console.log("Unit ratios: " + JSON.stringify(l), o), l
                                            }();
                                            var W = /margin|padding|left|right|width|text|word|letter/i.test(L) || /X$/.test(L) || "x" === L ? "x" : "y";
                                            switch (z) {
                                                case "%":
                                                    $ *= "x" === W ? i.percentToPxWidth : i.percentToPxHeight;
                                                    break;
                                                case "px":
                                                    break;
                                                default:
                                                    $ *= i[z + "ToPx"]
                                            }
                                            switch (R) {
                                                case "%":
                                                    $ *= 1 / ("x" === W ? i.percentToPxWidth : i.percentToPxHeight);
                                                    break;
                                                case "px":
                                                    break;
                                                default:
                                                    $ *= 1 / i[R + "ToPx"]
                                            }
                                        }
                                    switch (I) {
                                        case "+":
                                            F = $ + F;
                                            break;
                                        case "-":
                                            F = $ - F;
                                            break;
                                        case "*":
                                            F *= $;
                                            break;
                                        case "/":
                                            F = $ / F
                                    }
                                    l[L] = {
                                        rootPropertyValue: q,
                                        startValue: $,
                                        currentValue: $,
                                        endValue: F,
                                        unitType: R,
                                        easing: B
                                    }, b.debug && console.log("tweensContainer (" + L + "): " + JSON.stringify(l[L]), o)
                                } else b.debug && console.log("Skipping [" + V + "] due to a lack of browser support.")
                            }
                            l.element = o
                        }
                        l.element && (w.Values.addClass(o, "velocity-animating"), O.push(l), "" === s.queue && (a(o).tweensContainer = l, a(o).opts = s), a(o).isAnimating = !0, k === T - 1 ? (b.State.calls.push([O, h, s, null, E.resolver]), b.State.isTicking === !1 && (b.State.isTicking = !0, c())) : k++)
                    }
                    var i, o = this,
                        s = f.extend({}, b.defaults, y),
                        l = {};
                    switch (a(o) === r && b.init(o), parseFloat(s.delay) && s.queue !== !1 && f.queue(o, s.queue, function(e) {
                        b.velocityQueueEntryFlag = !0, a(o).delayTimer = {
                            setTimeout: setTimeout(e, parseFloat(s.delay)),
                            next: e
                        }
                    }), s.duration.toString().toLowerCase()) {
                        case "fast":
                            s.duration = 200;
                            break;
                        case "normal":
                            s.duration = v;
                            break;
                        case "slow":
                            s.duration = 600;
                            break;
                        default:
                            s.duration = parseFloat(s.duration) || 1
                    }
                    b.mock !== !1 && (b.mock === !0 ? s.duration = s.delay = 1 : (s.duration *= parseFloat(b.mock) || 1, s.delay *= parseFloat(b.mock) || 1)), s.easing = u(s.easing, s.duration), s.begin && !g.isFunction(s.begin) && (s.begin = null), s.progress && !g.isFunction(s.progress) && (s.progress = null), s.complete && !g.isFunction(s.complete) && (s.complete = null), s.display !== r && null !== s.display && (s.display = s.display.toString().toLowerCase(), "auto" === s.display && (s.display = b.CSS.Values.getDisplayType(o))), s.visibility !== r && null !== s.visibility && (s.visibility = s.visibility.toString().toLowerCase()), s.mobileHA = s.mobileHA && b.State.isMobile && !b.State.isGingerbread, s.queue === !1 ? s.delay ? setTimeout(e, s.delay) : e() : f.queue(o, s.queue, function(t, n) {
                        return n === !0 ? (E.promise && E.resolver(h), !0) : (b.velocityQueueEntryFlag = !0, void e(t))
                    }), "" !== s.queue && "fx" !== s.queue || "inprogress" === f.queue(o)[0] || f.dequeue(o)
                }
                var s, l, p, h, m, y, x = arguments[0] && (arguments[0].p || f.isPlainObject(arguments[0].properties) && !arguments[0].properties.names || g.isString(arguments[0].properties));
                if (g.isWrapped(this) ? (s = !1, p = 0, h = this, l = this) : (s = !0, p = 1, h = x ? arguments[0].elements || arguments[0].e : arguments[0]), h = o(h)) {
                    x ? (m = arguments[0].properties || arguments[0].p, y = arguments[0].options || arguments[0].o) : (m = arguments[p], y = arguments[p + 1]);
                    var T = h.length,
                        k = 0;
                    if (!/^(stop|finish)$/i.test(m) && !f.isPlainObject(y)) {
                        var S = p + 1;
                        y = {};
                        for (var j = S; j < arguments.length; j++) g.isArray(arguments[j]) || !/^(fast|normal|slow)$/i.test(arguments[j]) && !/^\d/.test(arguments[j]) ? g.isString(arguments[j]) || g.isArray(arguments[j]) ? y.easing = arguments[j] : g.isFunction(arguments[j]) && (y.complete = arguments[j]) : y.duration = arguments[j]
                    }
                    var E = {
                        promise: null,
                        resolver: null,
                        rejecter: null
                    };
                    s && b.Promise && (E.promise = new b.Promise(function(e, t) {
                        E.resolver = e, E.rejecter = t
                    }));
                    var N;
                    switch (m) {
                        case "scroll":
                            N = "scroll";
                            break;
                        case "reverse":
                            N = "reverse";
                            break;
                        case "finish":
                        case "stop":
                            f.each(h, function(e, t) {
                                a(t) && a(t).delayTimer && (clearTimeout(a(t).delayTimer.setTimeout), a(t).delayTimer.next && a(t).delayTimer.next(), delete a(t).delayTimer)
                            });
                            var A = [];
                            return f.each(b.State.calls, function(e, t) {
                                t && f.each(t[1], function(n, i) {
                                    var o = y === r ? "" : y;
                                    return o !== !0 && t[2].queue !== o && (y !== r || t[2].queue !== !1) || void f.each(h, function(n, r) {
                                        r === i && ((y === !0 || g.isString(y)) && (f.each(f.queue(r, g.isString(y) ? y : ""), function(e, t) {
                                            g.isFunction(t) && t(null, !0)
                                        }), f.queue(r, g.isString(y) ? y : "", [])), "stop" === m ? (a(r) && a(r).tweensContainer && o !== !1 && f.each(a(r).tweensContainer, function(e, t) {
                                            t.endValue = t.currentValue
                                        }), A.push(e)) : "finish" === m && (t[2].duration = 1))
                                    })
                                })
                            }), "stop" === m && (f.each(A, function(e, t) {
                                d(t, !0)
                            }), E.promise && E.resolver(h)), e();
                        default:
                            if (!f.isPlainObject(m) || g.isEmptyObject(m)) {
                                if (g.isString(m) && b.Redirects[m]) {
                                    var P = f.extend({}, y),
                                        H = P.duration,
                                        L = P.delay || 0;
                                    return P.backwards === !0 && (h = f.extend(!0, [], h).reverse()), f.each(h, function(e, t) {
                                        parseFloat(P.stagger) ? P.delay = L + parseFloat(P.stagger) * e : g.isFunction(P.stagger) && (P.delay = L + P.stagger.call(t, e, T)), P.drag && (P.duration = parseFloat(H) || (/^(callout|transition)/.test(m) ? 1e3 : v), P.duration = Math.max(P.duration * (P.backwards ? 1 - e / T : (e + 1) / T), .75 * P.duration, 200)), b.Redirects[m].call(t, t, P || {}, e, T, h, E.promise ? E : r)
                                    }), e()
                                }
                                var D = "Velocity: First argument (" + m + ") was not a property map, a known action, or a registered redirect. Aborting.";
                                return E.promise ? E.rejecter(new Error(D)) : console.log(D), e()
                            }
                            N = "start"
                    }
                    var M = {
                            lastParent: null,
                            lastPosition: null,
                            lastFontSize: null,
                            lastPercentToPxWidth: null,
                            lastPercentToPxHeight: null,
                            lastEmToPx: null,
                            remToPx: null,
                            vwToPx: null,
                            vhToPx: null
                        },
                        O = [];
                    f.each(h, function(e, t) {
                        g.isNode(t) && i.call(t)
                    });
                    var F, P = f.extend({}, b.defaults, y);
                    if (P.loop = parseInt(P.loop), F = 2 * P.loop - 1, P.loop)
                        for (var B = 0; F > B; B++) {
                            var $ = {
                                delay: P.delay,
                                progress: P.progress
                            };
                            B === F - 1 && ($.display = P.display, $.visibility = P.visibility, $.complete = P.complete), C(h, "reverse", $)
                        }
                    return e()
                }
            };
            b = f.extend(C, b), b.animate = C;
            var T = t.requestAnimationFrame || h;
            return b.State.isMobile || n.hidden === r || n.addEventListener("visibilitychange", function() {
                n.hidden ? (T = function(e) {
                    return setTimeout(function() {
                        e(!0)
                    }, 16)
                }, c()) : T = t.requestAnimationFrame || h
            }), e.Velocity = b, e !== t && (e.fn.velocity = C, e.fn.velocity.defaults = b.defaults), f.each(["Down", "Up"], function(e, t) {
                b.Redirects["slide" + t] = function(e, n, i, o, a, s) {
                    var l = f.extend({}, n),
                        u = l.begin,
                        c = l.complete,
                        d = {
                            height: "",
                            marginTop: "",
                            marginBottom: "",
                            paddingTop: "",
                            paddingBottom: ""
                        },
                        p = {};
                    l.display === r && (l.display = "Down" === t ? "inline" === b.CSS.Values.getDisplayType(e) ? "inline-block" : "block" : "none"), l.begin = function() {
                        u && u.call(a, a);
                        for (var n in d) {
                            p[n] = e.style[n];
                            var r = b.CSS.getPropertyValue(e, n);
                            d[n] = "Down" === t ? [r, 0] : [0, r]
                        }
                        p.overflow = e.style.overflow, e.style.overflow = "hidden"
                    }, l.complete = function() {
                        for (var t in p) e.style[t] = p[t];
                        c && c.call(a, a), s && s.resolver(a)
                    }, b(e, d, l)
                }
            }), f.each(["In", "Out"], function(e, t) {
                b.Redirects["fade" + t] = function(e, n, i, o, a, s) {
                    var l = f.extend({}, n),
                        u = {
                            opacity: "In" === t ? 1 : 0
                        },
                        c = l.complete;
                    l.complete = i !== o - 1 ? l.begin = null : function() {
                        c && c.call(a, a), s && s.resolver(a)
                    }, l.display === r && (l.display = "In" === t ? "auto" : "none"), b(this, u, l)
                }
            }), b
        }(window.jQuery || window.Zepto || window, window, document)
    }),
    function(e, t, n) {
        e.fn.backstretch = function(r, i) {
            return (r === n || 0 === r.length) && e.error("No images were supplied for Backstretch"), 0 === e(t).scrollTop() && t.scrollTo(0, 0), this.each(function() {
                var t = e(this),
                    n = t.data("backstretch");
                if (n) {
                    if ("string" == typeof r && "function" == typeof n[r]) return void n[r](i);
                    i = e.extend(n.options, i), n.destroy(!0)
                }
                n = new o(this, r, i), t.data("backstretch", n)
            })
        }, e.backstretch = function(t, n) {
            return e("body").backstretch(t, n).data("backstretch")
        }, e.expr[":"].backstretch = function(t) {
            return e(t).data("backstretch") !== n
        }, e.fn.backstretch.defaults = {
            centeredX: !0,
            centeredY: !0,
            duration: 5e3,
            fade: 0
        };
        var r = {
                left: 0,
                top: -20,
                overflow: "hidden",
                margin: 0,
                padding: 0,
                height: "100%",
                width: "100%",
                zIndex: -999999
            },
            i = {
                position: "absolute",
                display: "none",
                margin: 0,
                padding: 0,
                border: "none",
                width: "auto",
                height: "auto",
                maxHeight: "none",
                maxWidth: "none",
                zIndex: -999999
            },
            o = function(n, i, o) {
                this.options = e.extend({}, e.fn.backstretch.defaults, o || {}), this.images = e.isArray(i) ? i : [i], e.each(this.images, function() {
                    e("<img />")[0].src = this
                }), this.isBody = n === document.body, this.$container = e(n), this.$root = this.isBody ? e(a ? t : document) : this.$container, n = this.$container.children(".backstretch").first(), this.$wrap = n.length ? n : e('<div class="backstretch"></div>').css(r).appendTo(this.$container), this.isBody || (n = this.$container.css("position"), i = this.$container.css("zIndex"), this.$container.css({
                    position: "static" === n ? "relative" : n,
                    zIndex: "auto" === i ? 0 : i,
                    background: "none"
                }), this.$wrap.css({
                    zIndex: -999998
                })), this.$wrap.css({
                    position: this.isBody && a ? "fixed" : "absolute"
                }), this.index = 0, this.show(this.index), e(t).on("resize.backstretch", e.proxy(this.resize, this)).on("orientationchange.backstretch", e.proxy(function() {
                    this.isBody && 0 === t.pageYOffset && (t.scrollTo(0, 1), this.resize())
                }, this))
            };
        o.prototype = {
            resize: function() {
                try {
                    var e, n = {
                            left: 0,
                            top: 0
                        },
                        r = this.isBody ? this.$root.width() : this.$root.innerWidth(),
                        i = r,
                        o = this.isBody ? t.innerHeight ? t.innerHeight : this.$root.height() : this.$root.innerHeight(),
                        a = i / this.$img.data("ratio");
                    a >= o ? (e = (a - o) / 2, this.options.centeredY && (n.top = "-" + e + "px")) : (a = o, i = a * this.$img.data("ratio"), e = (i - r) / 2, this.options.centeredX && (n.left = "-" + e + "px")), this.$wrap.css({
                        width: r,
                        height: o
                    }).find("img:not(.deleteable)").css({
                        width: i,
                        height: a
                    }).css(n)
                } catch (e) {}
                return this
            },
            show: function(t) {
                if (!(Math.abs(t) > this.images.length - 1)) {
                    var n = this,
                        r = n.$wrap.find("img").addClass("deleteable"),
                        o = {
                            relatedTarget: n.$container[0]
                        };
                    return n.$container.trigger(e.Event("backstretch.before", o), [n, t]), this.index = t, clearInterval(n.interval), n.$img = e("<img />").css(i).bind("load", function(i) {
                        var a = this.width || e(i.target).width();
                        i = this.height || e(i.target).height(), e(this).data("ratio", a / i), e(this).fadeIn(n.options.speed || n.options.fade, function() {
                            r.remove(), n.paused || n.cycle(), e(["after", "show"]).each(function() {
                                n.$container.trigger(e.Event("backstretch." + this, o), [n, t])
                            })
                        }), n.resize()
                    }).appendTo(n.$wrap), n.$img.attr("src", n.images[t]), n
                }
            },
            next: function() {
                return this.show(this.index < this.images.length - 1 ? this.index + 1 : 0)
            },
            prev: function() {
                return this.show(0 === this.index ? this.images.length - 1 : this.index - 1)
            },
            pause: function() {
                return this.paused = !0, this
            },
            resume: function() {
                return this.paused = !1, this.next(), this
            },
            cycle: function() {
                return 1 < this.images.length && (clearInterval(this.interval), this.interval = setInterval(e.proxy(function() {
                    this.paused || this.next()
                }, this), this.options.duration)), this
            },
            destroy: function(n) {
                e(t).off("resize.backstretch orientationchange.backstretch"), clearInterval(this.interval), n || this.$wrap.remove(), this.$container.removeData("backstretch")
            }
        };
        var a, s = navigator.userAgent,
            l = navigator.platform,
            u = s.match(/AppleWebKit\/([0-9]+)/),
            u = !!u && u[1],
            c = s.match(/Fennec\/([0-9]+)/),
            c = !!c && c[1],
            d = s.match(/Opera Mobi\/([0-9]+)/),
            f = !!d && d[1],
            p = s.match(/MSIE ([0-9]+)/),
            p = !!p && p[1];
        a = !((-1 < l.indexOf("iPhone") || -1 < l.indexOf("iPad") || -1 < l.indexOf("iPod")) && u && 534 > u || t.operamini && "[object OperaMini]" === {}.toString.call(t.operamini) || d && 7458 > f || -1 < s.indexOf("Android") && u && 533 > u || c && 6 > c || "palmGetResource" in t && u && 534 > u || -1 < s.indexOf("MeeGo") && -1 < s.indexOf("NokiaBrowser/8.5.0") || p && 6 >= p)
    }(jQuery, window),
    function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e(require("jquery")) : e(jQuery)
    }(function(e) {
        e.fn.jScrollPane = function(t) {
            function n(t, n) {
                function r(n) {
                    var o, s, u, c, d, h, g = !1,
                        m = !1;
                    if (V = n, void 0 === q) d = t.scrollTop(), h = t.scrollLeft(), t.css({
                        overflow: "hidden",
                        padding: 0
                    }), _ = t.innerWidth() + ve, R = t.innerHeight(), t.width(_), q = e('<div class="jspPane" />').css("padding", me).append(t.children()), z = e('<div class="jspContainer" />').css({
                        width: _ + "px",
                        height: R + "px"
                    }).append(q).appendTo(t);
                    else {
                        if (t.css("width", ""), g = V.stickToBottom && j(), m = V.stickToRight && E(), c = t.innerWidth() + ve != _ || t.outerHeight() != R, c && (_ = t.innerWidth() + ve, R = t.innerHeight(), z.css({
                                width: _ + "px",
                                height: R + "px"
                            })), !c && ye == I && q.outerHeight() == W) return void t.width(_);
                        ye = I, q.css("width", ""), t.width(_), z.find(">.jspVerticalBar,>.jspHorizontalBar").remove().end()
                    }
                    q.css("overflow", "auto"), I = n.contentWidth ? n.contentWidth : q[0].scrollWidth, W = q[0].scrollHeight, q.css("overflow", ""), X = I / _, Y = W / R, Q = Y > 1, G = X > 1, G || Q ? (t.addClass("jspScrollable"), o = V.maintainPosition && (Z || te), o && (s = k(), u = S()), i(), a(), l(), o && (C(m ? I - _ : s, !1), w(g ? W - R : u, !1)), H(), N(), B(), V.enableKeyboardNavigation && D(), V.clickOnTrack && f(), O(), V.hijackInternalLinks && F()) : (t.removeClass("jspScrollable"), q.css({
                        top: 0,
                        left: 0,
                        width: z.width() - ve
                    }), A(), L(), M(), p()), V.autoReinitialise && !ge ? ge = setInterval(function() {
                        r(V)
                    }, V.autoReinitialiseDelay) : !V.autoReinitialise && ge && clearInterval(ge), d && t.scrollTop(0) && w(d, !1), h && t.scrollLeft(0) && C(h, !1), t.trigger("jsp-initialised", [G || Q])
                }

                function i() {
                    Q && (z.append(e('<div class="jspVerticalBar" />').append(e('<div class="jspCap jspCapTop" />'), e('<div class="jspTrack" />').append(e('<div class="jspDrag" />').append(e('<div class="jspDragTop" />'), e('<div class="jspDragBottom" />'))), e('<div class="jspCap jspCapBottom" />'))), ne = z.find(">.jspVerticalBar"), re = ne.find(">.jspTrack"), U = re.find(">.jspDrag"), V.showArrows && (se = e('<a class="jspArrow jspArrowUp" />').bind("mousedown.jsp", c(0, -1)).bind("click.jsp", P), le = e('<a class="jspArrow jspArrowDown" />').bind("mousedown.jsp", c(0, 1)).bind("click.jsp", P), V.arrowScrollOnHover && (se.bind("mouseover.jsp", c(0, -1, se)), le.bind("mouseover.jsp", c(0, 1, le))), u(re, V.verticalArrowPositions, se, le)), oe = R, z.find(">.jspVerticalBar>.jspCap:visible,>.jspVerticalBar>.jspArrow").each(function() {
                        oe -= e(this).outerHeight()
                    }), U.hover(function() {
                        U.addClass("jspHover")
                    }, function() {
                        U.removeClass("jspHover")
                    }).bind("mousedown.jsp", function(t) {
                        e("html").bind("dragstart.jsp selectstart.jsp", P), U.addClass("jspActive");
                        var n = t.pageY - U.position().top;
                        return e("html").bind("mousemove.jsp", function(e) {
                            g(e.pageY - n, !1)
                        }).bind("mouseup.jsp mouseleave.jsp", h), !1
                    }), o())
                }

                function o() {
                    re.height(oe + "px"), Z = 0, ie = V.verticalGutter + re.outerWidth(), q.width(_ - ie - ve);
                    try {
                        0 === ne.position().left && q.css("margin-left", ie + "px")
                    } catch (e) {}
                }

                function a() {
                    G && (z.append(e('<div class="jspHorizontalBar" />').append(e('<div class="jspCap jspCapLeft" />'), e('<div class="jspTrack" />').append(e('<div class="jspDrag" />').append(e('<div class="jspDragLeft" />'), e('<div class="jspDragRight" />'))), e('<div class="jspCap jspCapRight" />'))), ue = z.find(">.jspHorizontalBar"), ce = ue.find(">.jspTrack"), K = ce.find(">.jspDrag"), V.showArrows && (pe = e('<a class="jspArrow jspArrowLeft" />').bind("mousedown.jsp", c(-1, 0)).bind("click.jsp", P), he = e('<a class="jspArrow jspArrowRight" />').bind("mousedown.jsp", c(1, 0)).bind("click.jsp", P), V.arrowScrollOnHover && (pe.bind("mouseover.jsp", c(-1, 0, pe)), he.bind("mouseover.jsp", c(1, 0, he))), u(ce, V.horizontalArrowPositions, pe, he)), K.hover(function() {
                        K.addClass("jspHover")
                    }, function() {
                        K.removeClass("jspHover")
                    }).bind("mousedown.jsp", function(t) {
                        e("html").bind("dragstart.jsp selectstart.jsp", P), K.addClass("jspActive");
                        var n = t.pageX - K.position().left;
                        return e("html").bind("mousemove.jsp", function(e) {
                            v(e.pageX - n, !1)
                        }).bind("mouseup.jsp mouseleave.jsp", h), !1
                    }), de = z.innerWidth(), s())
                }

                function s() {
                    z.find(">.jspHorizontalBar>.jspCap:visible,>.jspHorizontalBar>.jspArrow").each(function() {
                        de -= e(this).outerWidth()
                    }), ce.width(de + "px"), te = 0
                }

                function l() {
                    if (G && Q) {
                        var t = ce.outerHeight(),
                            n = re.outerWidth();
                        oe -= t, e(ue).find(">.jspCap:visible,>.jspArrow").each(function() {
                            de += e(this).outerWidth()
                        }), de -= n, R -= n, _ -= t, ce.parent().append(e('<div class="jspCorner" />').css("width", t + "px")), o(), s()
                    }
                    G && q.width(z.outerWidth() - ve + "px"), W = q.outerHeight(), Y = W / R, G && (fe = Math.ceil(1 / X * de), fe > V.horizontalDragMaxWidth ? fe = V.horizontalDragMaxWidth : fe < V.horizontalDragMinWidth && (fe = V.horizontalDragMinWidth), K.width(fe + "px"), ee = de - fe, y(te)), Q && (ae = Math.ceil(1 / Y * oe), ae > V.verticalDragMaxHeight ? ae = V.verticalDragMaxHeight : ae < V.verticalDragMinHeight && (ae = V.verticalDragMinHeight), U.height(ae - 12 + "px"), J = oe - ae, m(Z))
                }

                function u(e, t, n, r) {
                    var i, o = "before",
                        a = "after";
                    "os" == t && (t = /Mac/.test(navigator.platform) ? "after" : "split"), t == o ? a = t : t == a && (o = t, i = n, n = r, r = i), e[o](n)[a](r)
                }

                function c(e, t, n) {
                    return function() {
                        return d(e, t, this, n), this.blur(), !1
                    }
                }

                function d(t, n, r, i) {
                    r = e(r).addClass("jspActive");
                    var o, a, s = !0,
                        l = function() {
                            0 !== t && be.scrollByX(t * V.arrowButtonSpeed), 0 !== n && be.scrollByY(n * V.arrowButtonSpeed), a = setTimeout(l, s ? V.initialDelay : V.arrowRepeatFreq), s = !1
                        };
                    l(), o = i ? "mouseout.jsp" : "mouseup.jsp", i = i || e("html"), i.bind(o, function() {
                        r.removeClass("jspActive"), a && clearTimeout(a), a = null, i.unbind(o)
                    })
                }

                function f() {
                    p(), Q && re.bind("mousedown.jsp", function(t) {
                        if (void 0 === t.originalTarget || t.originalTarget == t.currentTarget) {
                            var n, r = e(this),
                                i = r.offset(),
                                o = t.pageY - i.top - Z,
                                a = !0,
                                s = function() {
                                    var e = r.offset(),
                                        i = t.pageY - e.top - ae / 2,
                                        u = R * V.scrollPagePercent,
                                        c = J * u / (W - R);
                                    if (0 > o) Z - c > i ? be.scrollByY(-u) : g(i);
                                    else {
                                        if (!(o > 0)) return void l();
                                        i > Z + c ? be.scrollByY(u) : g(i)
                                    }
                                    n = setTimeout(s, a ? V.initialDelay : V.trackClickRepeatFreq), a = !1
                                },
                                l = function() {
                                    n && clearTimeout(n), n = null, e(document).unbind("mouseup.jsp", l)
                                };
                            return s(), e(document).bind("mouseup.jsp", l), !1
                        }
                    }), G && ce.bind("mousedown.jsp", function(t) {
                        if (void 0 === t.originalTarget || t.originalTarget == t.currentTarget) {
                            var n, r = e(this),
                                i = r.offset(),
                                o = t.pageX - i.left - te,
                                a = !0,
                                s = function() {
                                    var e = r.offset(),
                                        i = t.pageX - e.left - fe / 2,
                                        u = _ * V.scrollPagePercent,
                                        c = ee * u / (I - _);
                                    if (0 > o) te - c > i ? be.scrollByX(-u) : v(i);
                                    else {
                                        if (!(o > 0)) return void l();
                                        i > te + c ? be.scrollByX(u) : v(i)
                                    }
                                    n = setTimeout(s, a ? V.initialDelay : V.trackClickRepeatFreq), a = !1
                                },
                                l = function() {
                                    n && clearTimeout(n), n = null, e(document).unbind("mouseup.jsp", l)
                                };
                            return s(), e(document).bind("mouseup.jsp", l), !1
                        }
                    })
                }

                function p() {
                    ce && ce.unbind("mousedown.jsp"), re && re.unbind("mousedown.jsp")
                }

                function h() {
                    e("html").unbind("dragstart.jsp selectstart.jsp mousemove.jsp mouseup.jsp mouseleave.jsp"), U && U.removeClass("jspActive"), K && K.removeClass("jspActive")
                }

                function g(e, t) {
                    Q && (0 > e ? e = 0 : e > J && (e = J), void 0 === t && (t = V.animateScroll), t ? be.animate(U, "top", e, m) : (U.css("top", e), m(e)))
                }

                function m(e) {
                    void 0 === e && (e = U.position().top), z.scrollTop(0), Z = e || 0;
                    var n = 0 === Z,
                        r = Z == J,
                        i = e / J,
                        o = -i * (W - R);
                    (xe != n || Ce != r) && (xe = n, Ce = r, t.trigger("jsp-arrow-change", [xe, Ce, we, Te])), b(n, r), q.css("top", o), t.trigger("jsp-scroll-y", [-o, n, r]).trigger("scroll")
                }

                function v(e, t) {
                    G && (0 > e ? e = 0 : e > ee && (e = ee), void 0 === t && (t = V.animateScroll), t ? be.animate(K, "left", e, y) : (K.css("left", e), y(e)))
                }

                function y(e) {
                    void 0 === e && (e = K.position().left), z.scrollTop(0), te = e || 0;
                    var n = 0 === te,
                        r = te == ee,
                        i = e / ee,
                        o = -i * (I - _);
                    (we != n || Te != r) && (we = n, Te = r, t.trigger("jsp-arrow-change", [xe, Ce, we, Te])), x(n, r), q.css("left", o), t.trigger("jsp-scroll-x", [-o, n, r]).trigger("scroll")
                }

                function b(e, t) {
                    V.showArrows && (se[e ? "addClass" : "removeClass"]("jspDisabled"), le[t ? "addClass" : "removeClass"]("jspDisabled"))
                }

                function x(e, t) {
                    V.showArrows && (pe[e ? "addClass" : "removeClass"]("jspDisabled"), he[t ? "addClass" : "removeClass"]("jspDisabled"))
                }

                function w(e, t) {
                    g(e / (W - R) * J, t)
                }

                function C(e, t) {
                    v(e / (I - _) * ee, t)
                }

                function T(t, n, r) {
                    var i, o, a, s, l, u, c, d, f, p = 0,
                        h = 0;
                    try {
                        i = e(t)
                    } catch (e) {
                        return
                    }
                    for (o = i.outerHeight(), a = i.outerWidth(), z.scrollTop(0), z.scrollLeft(0); !i.is(".jspPane");)
                        if (p += i.position().top, h += i.position().left, i = i.offsetParent(), /^body|html$/i.test(i[0].nodeName)) return;
                    s = S(), u = s + R, s > p || n ? d = p - V.horizontalGutter : p + o > u && (d = p - R + o + V.horizontalGutter), isNaN(d) || w(d, r), l = k(), c = l + _, l > h || n ? f = h - V.horizontalGutter : h + a > c && (f = h - _ + a + V.horizontalGutter), isNaN(f) || C(f, r)
                }

                function k() {
                    return -q.position().left
                }

                function S() {
                    return -q.position().top
                }

                function j() {
                    var e = W - R;
                    return e > 20 && e - S() < 10
                }

                function E() {
                    var e = I - _;
                    return e > 20 && e - k() < 10
                }

                function N() {
                    z.unbind(Se).bind(Se, function(e, t, n, r) {
                        te || (te = 0), Z || (Z = 0);
                        var i = te,
                            o = Z,
                            a = e.deltaFactor || V.mouseWheelSpeed;
                        return be.scrollBy(n * a, -r * a, !1), i == te && o == Z
                    })
                }

                function A() {
                    z.unbind(Se)
                }

                function P() {
                    return !1
                }

                function H() {
                    q.find(":input,a").unbind("focus.jsp").bind("focus.jsp", function(e) {
                        T(e.target, !1)
                    })
                }

                function L() {
                    q.find(":input,a").unbind("focus.jsp")
                }

                function D() {
                    function n() {
                        var e = te,
                            t = Z;
                        switch (r) {
                            case 40:
                                be.scrollByY(V.keyboardSpeed, !1);
                                break;
                            case 38:
                                be.scrollByY(-V.keyboardSpeed, !1);
                                break;
                            case 34:
                            case 32:
                                be.scrollByY(R * V.scrollPagePercent, !1);
                                break;
                            case 33:
                                be.scrollByY(-R * V.scrollPagePercent, !1);
                                break;
                            case 39:
                                be.scrollByX(V.keyboardSpeed, !1);
                                break;
                            case 37:
                                be.scrollByX(-V.keyboardSpeed, !1)
                        }
                        return i = e != te || t != Z
                    }
                    var r, i, o = [];
                    G && o.push(ue[0]), Q && o.push(ne[0]), q.bind("focus.jsp", function() {
                        t.focus()
                    }), t.attr("tabindex", 0).unbind("keydown.jsp keypress.jsp").bind("keydown.jsp", function(t) {
                        if (t.target === this || o.length && e(t.target).closest(o).length) {
                            var a = te,
                                s = Z;
                            switch (t.keyCode) {
                                case 40:
                                case 38:
                                case 34:
                                case 32:
                                case 33:
                                case 39:
                                case 37:
                                    r = t.keyCode, n();
                                    break;
                                case 35:
                                    w(W - R), r = null;
                                    break;
                                case 36:
                                    w(0), r = null
                            }
                            return !(i = t.keyCode == r && a != te || s != Z)
                        }
                    }).bind("keypress.jsp", function(e) {
                        return e.keyCode == r && n(), !i
                    }), V.hideFocus ? (t.css("outline", "none"), "hideFocus" in z[0] && t.attr("hideFocus", !0)) : (t.css("outline", ""), "hideFocus" in z[0] && t.attr("hideFocus", !1))
                }

                function M() {
                    t.attr("tabindex", "-1").removeAttr("tabindex").unbind("keydown.jsp keypress.jsp"), q.unbind(".jsp")
                }

                function O() {
                    if (location.hash && location.hash.length > 1) {
                        var t, n, r = escape(location.hash.substr(1));
                        try {
                            t = e("#" + r + ', a[name="' + r + '"]')
                        } catch (e) {
                            return
                        }
                        t.length && q.find(r) && (0 === z.scrollTop() ? n = setInterval(function() {
                            z.scrollTop() > 0 && (T(t, !0), e(document).scrollTop(z.position().top), clearInterval(n))
                        }, 50) : (T(t, !0), e(document).scrollTop(z.position().top)))
                    }
                }

                function F() {
                    e(document.body).data("jspHijack") || (e(document.body).data("jspHijack", !0), e(document.body).delegate("a[href*=#]", "click", function(t) {
                        var n, r, i, o, a, s, l = this.href.substr(0, this.href.indexOf("#")),
                            u = location.href;
                        if (-1 !== location.href.indexOf("#") && (u = location.href.substr(0, location.href.indexOf("#"))), l === u) {
                            n = escape(this.href.substr(this.href.indexOf("#") + 1));
                            try {
                                r = e("#" + n + ', a[name="' + n + '"]')
                            } catch (e) {
                                return
                            }
                            r.length && (i = r.closest(".jspScrollable"), o = i.data("jsp"), o.scrollToElement(r, !0), i[0].scrollIntoView && (a = e(window).scrollTop(), s = r.offset().top, (a > s || s > a + e(window).height()) && i[0].scrollIntoView()), t.preventDefault())
                        }
                    }))
                }

                function B() {
                    var e, t, n, r, i, o = !1;
                    z.unbind("touchstart.jsp touchmove.jsp touchend.jsp click.jsp-touchclick").bind("touchstart.jsp", function(a) {
                        var s = a.originalEvent.touches[0];
                        e = k(), t = S(), n = s.pageX, r = s.pageY, i = !1, o = !0
                    }).bind("touchmove.jsp", function(a) {
                        if (o) {
                            var s = a.originalEvent.touches[0],
                                l = te,
                                u = Z;
                            return be.scrollTo(e + n - s.pageX, t + r - s.pageY), i = i || Math.abs(n - s.pageX) > 5 || Math.abs(r - s.pageY) > 5, l == te && u == Z
                        }
                    }).bind("touchend.jsp", function(e) {
                        o = !1
                    }).bind("click.jsp-touchclick", function(e) {
                        return i ? (i = !1, !1) : void 0
                    })
                }

                function $() {
                    var e = S(),
                        n = k();
                    t.removeClass("jspScrollable").unbind(".jsp"), q.unbind(".jsp"), t.replaceWith(ke.append(q.children())), ke.scrollTop(e), ke.scrollLeft(n), ge && clearInterval(ge)
                }
                var V, q, _, R, z, I, W, X, Y, Q, G, U, J, Z, K, ee, te, ne, re, ie, oe, ae, se, le, ue, ce, de, fe, pe, he, ge, me, ve, ye, be = this,
                    xe = !0,
                    we = !0,
                    Ce = !1,
                    Te = !1,
                    ke = t.clone(!1, !1).empty(),
                    Se = e.fn.mwheelIntent ? "mwheelIntent.jsp" : "mousewheel.jsp";
                "border-box" === t.css("box-sizing") ? (me = 0, ve = 0) : (me = t.css("paddingTop") + " " + t.css("paddingRight") + " " + t.css("paddingBottom") + " " + t.css("paddingLeft"), ve = (parseInt(t.css("paddingLeft"), 10) || 0) + (parseInt(t.css("paddingRight"), 10) || 0)), e.extend(be, {
                    reinitialise: function(t) {
                        t = e.extend({}, V, t), r(t)
                    },
                    scrollToElement: function(e, t, n) {
                        T(e, t, n)
                    },
                    scrollTo: function(e, t, n) {
                        C(e, n), w(t, n)
                    },
                    scrollToX: function(e, t) {
                        C(e, t)
                    },
                    scrollToY: function(e, t) {
                        w(e, t)
                    },
                    scrollToPercentX: function(e, t) {
                        C(e * (I - _), t)
                    },
                    scrollToPercentY: function(e, t) {
                        w(e * (W - R), t)
                    },
                    scrollBy: function(e, t, n) {
                        be.scrollByX(e, n), be.scrollByY(t, n)
                    },
                    scrollByX: function(e, t) {
                        v((k() + Math[0 > e ? "floor" : "ceil"](e)) / (I - _) * ee, t)
                    },
                    scrollByY: function(e, t) {
                        g((S() + Math[0 > e ? "floor" : "ceil"](e)) / (W - R) * J, t)
                    },
                    positionDragX: function(e, t) {
                        v(e, t)
                    },
                    positionDragY: function(e, t) {
                        g(e, t)
                    },
                    animate: function(e, t, n, r) {
                        var i = {};
                        i[t] = n, e.animate(i, {
                            duration: V.animateDuration,
                            easing: V.animateEase,
                            queue: !1,
                            step: r
                        })
                    },
                    getContentPositionX: function() {
                        return k()
                    },
                    getContentPositionY: function() {
                        return S()
                    },
                    getContentWidth: function() {
                        return I
                    },
                    getContentHeight: function() {
                        return W
                    },
                    getPercentScrolledX: function() {
                        return k() / (I - _)
                    },
                    getPercentScrolledY: function() {
                        return S() / (W - R)
                    },
                    getIsScrollableH: function() {
                        return G
                    },
                    getIsScrollableV: function() {
                        return Q
                    },
                    getContentPane: function() {
                        return q
                    },
                    scrollToBottom: function(e) {
                        g(J, e)
                    },
                    hijackInternalLinks: e.noop,
                    destroy: function() {
                        $()
                    }
                }), r(n)
            }
            return t = e.extend({}, e.fn.jScrollPane.defaults, t), e.each(["arrowButtonSpeed", "trackClickSpeed", "keyboardSpeed"], function() {
                t[this] = t[this] || t.speed
            }), this.each(function() {
                var r = e(this),
                    i = r.data("jsp");
                i ? i.reinitialise(t) : (e("script", r).filter('[type="text/javascript"],:not([type])').remove(), i = new n(r, t), r.data("jsp", i))
            })
        }, e.fn.jScrollPane.defaults = {
            showArrows: !1,
            maintainPosition: !0,
            stickToBottom: !1,
            stickToRight: !1,
            clickOnTrack: !0,
            autoReinitialise: !1,
            autoReinitialiseDelay: 500,
            verticalDragMinHeight: 0,
            verticalDragMaxHeight: 99999,
            horizontalDragMinWidth: 0,
            horizontalDragMaxWidth: 99999,
            contentWidth: void 0,
            animateScroll: !1,
            animateDuration: 300,
            animateEase: "linear",
            hijackInternalLinks: !1,
            verticalGutter: 4,
            horizontalGutter: 4,
            mouseWheelSpeed: 3,
            arrowButtonSpeed: 0,
            arrowRepeatFreq: 50,
            arrowScrollOnHover: !1,
            trackClickSpeed: 0,
            trackClickRepeatFreq: 70,
            verticalArrowPositions: "split",
            horizontalArrowPositions: "split",
            enableKeyboardNavigation: !0,
            hideFocus: !1,
            keyboardSpeed: 0,
            initialDelay: 300,
            speed: 30,
            scrollPagePercent: .8
        }
    }),
    function(e) {
        "function" == typeof define && define.amd ? define(["jquery"], e) : "object" == typeof exports ? module.exports = e : e(jQuery)
    }(function(e) {
        function t(t) {
            var a = t || window.event,
                s = l.call(arguments, 1),
                u = 0,
                c = 0,
                d = 0,
                f = 0;
            if (t = e.event.fix(a), t.type = "mousewheel", "detail" in a && (d = a.detail * -1), "wheelDelta" in a && (d = a.wheelDelta), "wheelDeltaY" in a && (d = a.wheelDeltaY), "wheelDeltaX" in a && (c = a.wheelDeltaX * -1), "axis" in a && a.axis === a.HORIZONTAL_AXIS && (c = d * -1, d = 0), u = 0 === d ? c : d, "deltaY" in a && (d = a.deltaY * -1, u = d), "deltaX" in a && (c = a.deltaX, 0 === d && (u = c * -1)), 0 !== d || 0 !== c) {
                if (1 === a.deltaMode) {
                    var p = e.data(this, "mousewheel-line-height");
                    u *= p, d *= p, c *= p
                } else if (2 === a.deltaMode) {
                    var h = e.data(this, "mousewheel-page-height");
                    u *= h, d *= h, c *= h
                }
                return f = Math.max(Math.abs(d), Math.abs(c)), (!o || f < o) && (o = f, r(a, f) && (o /= 40)), r(a, f) && (u /= 40, c /= 40, d /= 40), u = Math[u >= 1 ? "floor" : "ceil"](u / o), c = Math[c >= 1 ? "floor" : "ceil"](c / o), d = Math[d >= 1 ? "floor" : "ceil"](d / o), t.deltaX = c, t.deltaY = d, t.deltaFactor = o, t.deltaMode = 0, s.unshift(t, u, c, d), i && clearTimeout(i), i = setTimeout(n, 200), (e.event.dispatch || e.event.handle).apply(this, s)
            }
        }

        function n() {
            o = null
        }

        function r(e, t) {
            return c.settings.adjustOldDeltas && "mousewheel" === e.type && t % 120 == 0
        }
        var i, o, a = ["wheel", "mousewheel", "DOMMouseScroll", "MozMousePixelScroll"],
            s = "onwheel" in document || document.documentMode >= 9 ? ["wheel"] : ["mousewheel", "DomMouseScroll", "MozMousePixelScroll"],
            l = Array.prototype.slice;
        if (e.event.fixHooks)
            for (var u = a.length; u;) e.event.fixHooks[a[--u]] = e.event.mouseHooks;
        var c = e.event.special.mousewheel = {
            version: "3.1.9",
            setup: function() {
                if (this.addEventListener)
                    for (var n = s.length; n;) this.addEventListener(s[--n], t, !1);
                else this.onmousewheel = t;
                e.data(this, "mousewheel-line-height", c.getLineHeight(this)), e.data(this, "mousewheel-page-height", c.getPageHeight(this))
            },
            teardown: function() {
                if (this.removeEventListener)
                    for (var e = s.length; e;) this.removeEventListener(s[--e], t, !1);
                else this.onmousewheel = null
            },
            getLineHeight: function(t) {
                return parseInt(e(t)["offsetParent" in e.fn ? "offsetParent" : "parent"]().css("fontSize"), 10)
            },
            getPageHeight: function(t) {
                return e(t).height()
            },
            settings: {
                adjustOldDeltas: !0
            }
        };
        e.fn.extend({
            mousewheel: function(e) {
                return e ? this.bind("mousewheel", e) : this.trigger("mousewheel")
            },
            unmousewheel: function(e) {
                return this.unbind("mousewheel", e)
            }
        })
    }),
    function(e) {
        e.fn.easyRollover = function(t) {
            for (var n = {
                    suffix: "_out.",
                    suffix_replace: "_over.",
                    transition: "moment",
                    opacity: .7,
                    duration: 250,
                    easing: "linear",
                    parent_style_overlap: !0
                }, r = e.extend(n, t), i = 0; i < this.length; i++) ! function(t) {
                var n, o, a, s;
                if (n = t[i].getAttribute("src"), n.indexOf(r.suffix) > -1) switch (o = n.replace(r.suffix, r.suffix_replace), s = new Image, s.src = o, r.transition) {
                    case "moment":
                        e(t[i]).hover(function() {
                            this.setAttribute("src", o)
                        }, function() {
                            this.setAttribute("src", n)
                        });
                        break;
                    case "fade":
                        var a = e(t[i]),
                            l = a.parent();
                        a.css({
                            position: "relative",
                            zIndex: 1
                        }).hover(function() {
                            e(this).stop().fadeTo(r.duration, 0, r.easing)
                        }, function() {
                            e(this).stop().fadeTo(r.duration, 1, r.easing)
                        }), r.parent_style_overlap && l.css({
                            position: "relative",
                            display: "inline-block"
                        }), e(s).addClass("over").css({
                            position: "absolute",
                            top: 0,
                            left: 0
                        }), l.append(s)
                } else switch (r.transition) {
                    case "fade":
                        var a = e(t[i]);
                        a.hover(function() {
                            a.stop().fadeTo(r.duration, r.opacity, r.easing)
                        }, function() {
                            a.stop().fadeTo(r.duration, 1, r.easing)
                        })
                }
            }(this);
            return this
        }
    }(jQuery);
var ua = {};
ua.name = window.navigator.userAgent.toLowerCase(), ua.isIE = ua.name.indexOf("msie") >= 0 || ua.name.indexOf("trident") >= 0, ua.isiPhone = ua.name.indexOf("iphone") >= 0, ua.isiPod = ua.name.indexOf("ipod") >= 0, ua.isiPad = ua.name.indexOf("ipad") >= 0, ua.isiOS = ua.isiPhone || ua.isiPod || ua.isiPad, ua.isAndroid = ua.name.indexOf("android") >= 0, ua.isTablet = ua.isiPad || ua.isAndroid && ua.name.indexOf("mobile") < 0, ua.isIE && (ua.verArray = /(msie|rv:?)\s?([0-9]{1,})([\.0-9]{1,})/.exec(ua.name), ua.verArray && (ua.ver = parseInt(ua.verArray[2], 10))), ua.isiOS && (ua.verArray = /(os)\s([0-9]{1,})([\_0-9]{1,})/.exec(ua.name), ua.verArray && (ua.ver = parseInt(ua.verArray[2], 10))), ua.isAndroid && (ua.verArray = /(android)\s([0-9]{1,})([\.0-9]{1,})/.exec(ua.name), ua.verArray && (ua.ver = parseInt(ua.verArray[2], 10))), ua.isAndroid && ua.ver <= 2 && $("html").addClass("android_2"), ua.isiOS && $("html").addClass("iOS"), ua.isTablet && $("html").addClass("tab");
var isIE8 = ua.isIE && ua.ver <= 8;
$(function() {
    var e = $(window),
        t = $("body"),
        n = $("#container"),
        r = $("#footer"),
        i = $("#landScape").children(".ls"),
        o = $("#comMenu"),
        a = $("#menu"),
        s = $("#menuOver"),
        l = $("#menu-btn"),
        u = $("#cover"),
        c = $(".localMenu");
    ua.isiOS || ua.isAndroid || $("a.telLink").each(function() {
        $(this).replaceWith('<span class="telLink">' + $(this).html() + "</span>")
    }), l.click(function() {
        return n.hasClass("menu-open") ? (n.removeClass("menu-open"), l.find(".txt").text("MENU"), "second" == breakCheck() ? (n.attr("style", ""), setTimeout(function() {
            $("body,html").animate({
                scrollTop: 0
            }, 500)
        }, 500)) : ($("#info").removeClass("info-btn"), $(".backstretch").stop().velocity({
            translateX: 0
        }, {
            duration: 500,
            easing: "ease"
        }))) : (n.addClass("menu-open"), l.find(".txt").text("CLOSE"), "second" == breakCheck() ? (n.css({
            height: o.outerHeight(!0),
            overflow: "hidden"
        }), setTimeout(function() {
            $("body,html").stop().animate({
                scrollTop: 0
            }, 500)
        }, 500)) : ($(".backstretch").stop().velocity({
            translateX: 120
        }, {
            duration: 500,
            easing: "ease"
        }), $("#info").addClass("info-btn"), $(".info-btn").on("click", function() {
            n.removeClass("menu-open"), l.find(".txt").text("MENU"), $(".backstretch").stop().velocity({
                translateX: 0
            }, {
                duration: 500,
                easing: "ease"
            })
        }))), !1
    });
    var d = t.attr("id");
    t.hasClass("sightseeing") ? (o.find(".sight a").addClass("act"), c.find(".sight a").addClass("act")) : t.hasClass("modelcourse") ? (o.find(".course a").addClass("act"), c.find(".course a").addClass("act")) : "event" == d ? (o.find(".special a").addClass("act"), c.find(".special a").addClass("act")) : "about" == d && (o.find(".about a").addClass("act"), c.find(".about a").addClass("act"));
    var f, p = function() {
            $(".fadeOv").easyRollover({
                transition: "fade",
                duration: 500,
                easing: "swing"
            });
            var e;
            t.hasClass("top") ? (a.mouseenter(function() {
                e = 0, s.velocity({
                    translateX: -220
                }, {
                    duration: 500,
                    easing: "easeOutExpo"
                })
            }).mouseleave(function() {
                s.velocity({
                    translateX: 0
                }, {
                    duration: 500,
                    easing: "easeOutExpo"
                }), e = 1
            }), a.find("a").on("mouseenter", function() {
                1 == e ? (s.velocity({
                    translateY: $(this).position().top + a.scrollTop()
                }, {
                    duration: 1,
                    easing: "linier"
                }), e = 0) : s.velocity({
                    translateY: $(this).position().top + a.scrollTop()
                }, {
                    duration: 500,
                    queue: !1,
                    easing: "easeOutExpo"
                })
            })) : (e = 1, s.velocity({
                translateX: -220,
                translateY: o.find("#menu .act").position().top + a.scrollTop()
            }, {
                duration: 500,
                easing: "easeOutExpo"
            }), a.find("a").on("mouseenter", function() {
                s.stop().velocity({
                    translateY: $(this).position().top + a.scrollTop()
                }, {
                    duration: 500,
                    easing: "easeOutExpo"
                })
            }), a.on("mouseleave", function() {
                s.stop().velocity({
                    translateY: o.find("#menu .act").position().top + a.scrollTop()
                }, {
                    duration: 500,
                    easing: "easeOutExpo"
                })
            }))
        },
        h = function() {
            var e = function(e) {
                "touchstart" === e.type ? $(this).addClass("tapOn") : (e.type, $(this).removeClass("tapOn"))
            };
            ! function() {
                $(".tapBtn").on("touchstart touchmove touchend", e)
            }()
        },
        g = !1;
    e.resize(function() {
        g !== !1 && clearTimeout(g), g = setTimeout(function() {
            "second" == breakCheck() ? ("sp" != f ? (t.attr("style", ""), i.attr("style", ""), o.attr("style", ""), n.attr("style", ""), $(".jspContainer").attr("style", ""), $(".jspPane").attr("style", ""), h(), f = "sp") : n.hasClass("menu-open") && n.css({
                height: o.outerHeight(!0),
                overflow: "hidden"
            }), u.fadeOut(500)) : (o.attr("style", ""), n.attr("style", ""), "pc" != f && (p(), f = "pc"), t.css({
                width: e.outerWidth(),
                height: e.outerHeight()
            }), i.css({
                width: e.outerWidth(),
                height: e.outerHeight()
            }), o.css({
                height: e.outerHeight() - r.outerHeight()
            }), setTimeout(function() {
                o.jScrollPane({
                    showArrows: !0,
                    horizontalGutter: 10
                }), o.find(".jspContainer").css({
                    height: o.outerHeight()
                })
            }, 1e3), "first" == breakCheck() ? (n.addClass("menu-hide"), setTimeout(function() {
                $(".backstretch").stop().velocity({
                    translateX: 0
                }, {
                    duration: 500,
                    easing: "ease"
                })
            }, 300)) : (n.removeClass("menu-hide"), setTimeout(function() {
                $(".backstretch").stop().velocity({
                    translateX: 120
                }, {
                    duration: 500,
                    easing: "ease"
                })
            }, 300)), u.attr("style", ""))
        }, 300)
    }).trigger("resize"), e.scroll(function() {
        "second" == breakCheck() && (e.scrollTop() > 60 ? l.hasClass("scrl") || l.addClass("scrl") : l.hasClass("scrl") && l.removeClass("scrl"))
    }).trigger("scroll")
});