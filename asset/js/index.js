/* ニュースティッカー */
var newsInterval;
$.fn.newsTick = function(config){
	var defaults = {
		speed: 1000,
		change: 5000,
		easing: 'swing'
	},
	options = $.extend(defaults, config),
	target = this,
	parent = target.parent(),
	speed = options.speed,
	change = options.change,
	easing = options.easing;

	target.eq(0).show().addClass('show');
	if(target.length > 1){
		newsInterval = setInterval(function(){
			var active = parent.find('.show');
			active.velocity({
				top: -90
			},speed,easing).next().show().css({
				top: 90
			}).velocity({
				top: 0
			},speed,easing).addClass('show').end().appendTo(parent).removeClass('show');
		},change);
	}
}

/* 新着テキスト 省略 */
$.fn.cutTxt = function(config){
	var defaults = {
		max: 38,
		afterTxt: '…もっと読む≫'
	},
	options = $.extend(defaults, config),
	target = this,
	max = options.max,
	txt = options.afterTxt;

	for(var i = 0; i < target.length; i++) {
		var self = target.eq(i);
		// 最大文字数を超えた際にafterTxtを代入する
		if(self.text().length > max){
			var len = self.text().length,
				trim = self.text().substr(0,(max));

			if(max < len) {
				self.html(trim + txt).css({visibility: 'visible'});
			}else{
				self.css({visibility: 'visible'});
			}
		}
	};
}

$(function(){
/* PC */
	/* 町並みパララックス */
	var landScape = new Array(3);
	landScape[0] = 20;
	landScape[1] = 50;
	landScape[2] = 90;

	/* サークル */
	var	time = 3, // 通過の秒数
		total = ((time*16)+time)*1000,
		span = 10,
		divide = time*1000,
		interval,
		timenow = 0,
		circle = $('#circle');

	var numList = $('#circle .set').find('li');

	// アクティブの設定と背景の切り替え
	function set(num){
		for(var i=0; i<numList.length; i++) {
			if(numList.eq(i).hasClass('set'+num)){
				numList.removeClass('act');
				numList.eq(i).addClass('act');

				$.backstretch('/img/main'+num+'.jpg',{speed: 1000});
			}
		}
	}

	if(isIE8) {
		// 円弧の描画処理（IE)
		function drawIE(){
			timenow += 1;
			if(timenow >= total/divide){
				timenow = 0;
				set(0);
			}
			set(timenow);
		}
	}else{
		var route = document.getElementById('route');
		route.width = 480;
		route.height = 480;
		var ctx = route.getContext('2d');

		// 円弧の描画処理
		function draw(){
			var str = timenow / total;
			clear();
			ctx.beginPath();
			ctx.lineWidth = 2;
			ctx.strokeStyle = "#FFF";
			ctx.arc(240,240,200,Math.PI*1.5,Math.PI*2*str+Math.PI*1.5,false); // 0時方向から始めるために1.5をかけて計算
			ctx.stroke();
			if(timenow > total){
				timenow = 0;
				set(0);
			}
			timenow += span;
			if((timenow%divide) == 0){
				set(timenow/divide);
			}
		}
		function clear(){
			ctx.clearRect(0,0,480,480);
		}
	}

	var indexPC = function(){
		/* 町並みパララックス */
		$(window).mousemove(function(e){
			var landScapePos = new Array(3),
				move = e.clientX,
				windowWidth = $(window).width(),
				movePer = move / windowWidth;
			// マウスの動きに合わせて対象のX軸を移動させる
			for(var i = 0; i < 3; i++){
				landScapePos[i] = landScape[i] - (landScape[i] * movePer);
				$('#ls0' + (i + 1)).stop().velocity({
					translateX: landScapePos[i]
				});
			}
		});

		// ニュースティッカー
		$('#info').find('.news li').newsTick();

		// 新着テキスト 省略
		// $('#info').find('a').cutTxt();

		// サークル
		circle.fadeIn();
		// クリックでの切り替え
		numList.click(function(){
			var self = $(this),
				index = self.index();

			clearInterval(interval);
			if(isIE8) {
				timenow = index;
				interval = setInterval(drawIE,span*300);
			}else{
				timenow = index*divide;
				interval = setInterval(draw,span);
			}
			set(index);
		});

		set(0);

		if(isIE8) {
			// 円弧の描画処理（IE)
			interval = setInterval(drawIE,span*300);

		}else{
			// 円弧の描画処理
			interval = setInterval(draw,span);
		}
	}

/* SP */
	var main = $('#main');

	var indexSP = function(){
		// ニュースティッカー
		main.find('.news li').newsTick();

		// 新着テキスト 省略
		// main.find('a span').cutTxt({
		// 	max: 20,
		// 	afterTxt: '…'
		// });
	}

	/* リサイズイベント */
	var timer = false;
	var devFlg;
	$(window).resize(function(){
		// 操作が終わった時にだけ処理を実行
		if (timer !== false) {
			clearTimeout(timer);
		}
		timer = setTimeout(function(){
			if(breakCheck() == 'second'){
				if(devFlg != 'sp'){
					// PC リセット
					clearInterval(interval);
					timenow = 0;
					$('#info').find('.news li').attr('style', '');
					$.backstretch('destroy');
					circle.fadeOut();

					clearInterval(newsInterval);

					indexSP();
					devFlg = 'sp';
				}
			}else{
				if($(window).height() < 760){
					$('#circle').addClass('virtical');
				}else{
					$('#circle').removeClass('virtical');
				}
				if(devFlg != 'pc'){
					// SP リセット
					clearInterval(newsInterval);

					indexPC();
					devFlg = 'pc';
				}
			}
		}, 300);
	}).trigger('resize');
});
